import 'dart:async';

import 'package:comp1/Seller/Fortend/SellerScreen/SelVerify.dart';
import 'package:flutter/material.dart';

import '../Repository/setting_repository.dart' as repo;
import '../Repository/user_repository.dart';
import '../Screens/dashBoard.dart';
import '../Screens/onboarding.dart';
import '../Screens/seller_dashboard.dart';
import '../util/common.dart';
import '../util/send_request.dart';
import '../util/shared_helper.dart';
import '../util/style.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  SharedHelper sharedHelper = SharedHelper();

  @override
  void initState() {
    // SharedHelper().remove('current_user');
    checkLogin();
    super.initState();
  }

  checkLogin() async {
    bool isLogin = await sharedHelper.containsKey('current_user');
    if (isLogin) {
      currentUser.value = await getCurrentUser();
      print(currentUser.value.token);
      repo.getScreen(context).then((value) => {
            if (value != null)
              {
                if (value['screen'] == 1)
                  navigationRemoveUntil(context, DashBoard(currentTab: 1))
                else
                  navigationRemoveUntil(context, SellerVerifyFromProfile()
                      //  SellerVerfiyPage()
                      // SellerDashboard(
                      //   currentTab: 0,
                      // )
                      )
              }
            else
              {
                sharedHelper.remove('current_user'),
                navigationRemoveUntil(context, OnBoarding()),
              }
          });
    } else
      Timer(Duration(seconds: 2),
          () => navigationRemoveUntil(context, OnBoarding()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: redColor,
      body: Hero(
        tag: 'logo',
        child: Center(
          child: Image.asset('assets/app_logo/logo.png', height: 200),
        ),
      ),
    );
  }
}
