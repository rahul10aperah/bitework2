import 'package:comp1/Controller/user_controller.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/input_widgets.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class InputForOtp extends StatefulWidget {
  final bool isEmail;

  InputForOtp({this.isEmail = false});

  @override
  _InputForOtpState createState() => _InputForOtpState();
}

class _InputForOtpState extends StateMVC<InputForOtp> {
  late UserController _con;

  _InputForOtpState() : super(UserController()) {
    _con = controller as UserController;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: offWhiteColor,
      appBar: AppBar(
        backgroundColor: offWhiteColor,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0.0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: Form(
          key: _con.formKey,
          child: ListView(
            children: [
              heightSizedBox(getHeight(context) / 4),
              Text(
                widget.isEmail
                    ? "Enter email to reset password"
                    : "Enter Mobile number to reset password",
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: montserratMedium,
                ),
              ),
              heightSizedBox(30.0),
              widget.isEmail
                  ? EditTextField(
                      validator: validateEmail,
                      onChanged: (input) {
                        _con.user.email = input.toString();
                      },
                      keyBoard: TextInputType.emailAddress,
                      labelText: 'Email',
                      hint: 'Enter your email.',
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Mobile', style: labelTextStyle),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Text(
                                '+91',
                                style: TextStyle(
                                    fontSize: 17.0, color: Colors.black87),
                              ),
                            ),
                            widthSizedBox(10.0),
                            Expanded(
                              child: EditTextField(
                                validator: validateMobile,
                                // initValue: '91  ',
                                onChanged: (input) {
                                  _con.user.mobile = input.toString();
                                },
                                keyBoard: TextInputType.number,
                                // labelText: '',
                                hint: 'Enter your mobile Number',
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
              heightSizedBox(40.0),
              BigButton(
                  title: 'Next',
                  onTap: () => _con.sendOTP(context, widget.isEmail)),
            ],
          ),
        ),
      ),
    );
  }
}
