import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Screens/inputForOtp.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key? key}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: offWhiteColor,
      appBar: BaseAppBar(
        title: "Forgot Password",
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: Container(
              decoration: boxDecoration,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Forget your password ?",
                    style: smallTextStyle,
                  ),
                  heightSizedBox(20.0),
                  Text(
                    "We got you. Just enter the email or phone below and we will help you reset the password",
                    style: smallTextStyle,
                  ),
                  heightSizedBox(20.0),
                  BigButton(
                    title: "Reset by Email",
                    onTap: () =>
                        navigationPush(context, InputForOtp(isEmail: true)),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    alignment: Alignment.center,
                    child: Text(
                      "Or",
                      textAlign: TextAlign.center,
                      style: labelTextStyle,
                    ),
                  ),
                  BigButton(
                    color: darkBlueColor,
                    title: "Reset by Phone",
                    onTap: () => navigationPush(context, InputForOtp()),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
