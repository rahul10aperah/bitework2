import 'package:comp1/Elements/deal_item.dart';
import 'package:comp1/Elements/loader.dart';
import 'package:comp1/Model/category.dart';
// import 'package:comp1/Model/category.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '/Controller/home_controller.dart';
import '/Elements/base_appbar.dart';
import '/util/common.dart';
import '/util/style.dart';

class DealsOffers extends StatefulWidget {
  @override
  _DealsOffersState createState() => _DealsOffersState();
}

class _DealsOffersState extends StateMVC<DealsOffers> {
  late HomeController _con;

  _DealsOffersState() : super(HomeController()) {
    _con = controller as HomeController;
  }

  @override
  void initState() {
    super.initState();
    _con.getHomeData(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: redColor,
      appBar: BaseAppBar(
          title: 'Deals and Offers', elevation: 0.0, centerTitle: true),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            alignment: Alignment.bottomCenter,
            color: primaryColor,
            height: getHeight(context) / 1.7,
          ),
          CustomScrollView(
            primary: true,
            // shrinkWrap: false,
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: redColor,
                expandedHeight: 75,
                elevation: 0,
                flexibleSpace: FlexibleSpaceBar(
                  collapseMode: CollapseMode.parallax,
                  background: Container(
                    color: redColor,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 45,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 8),
                              decoration: fieldDecoration,
                              child: TextField(
                                cursorColor: Colors.black,
                                maxLines: 1,
                                maxLength: 30,
                                decoration: InputDecoration(
                                  hintText: "Search food or chef",
                                  border: InputBorder.none,
                                  counterText: "",
                                  hintStyle: TextStyle(
                                      fontSize: 14.0, color: Colors.grey),
                                ),
                                onChanged: (input) {},
                              ),
                            ),
                          ),
                          widthSizedBox(10.0),
                          Container(
                            height: 45,
                            width: 45,
                            decoration: fieldDecoration,
                            child: Icon(
                              Icons.location_on_outlined,
                              color: redColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: _con.deals.isEmpty ? getHeight(context) : null,
                  decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25))),
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: Wrap(
                    runSpacing: 0,
                    children: [
                      Column(
                        children: [
                          _con.deals.isEmpty
                              ? Loader(height: getHeight(context) / 2)
                              : ListView.builder(
                                  shrinkWrap: true,
                                  primary: false,
                                  padding: EdgeInsets.only(
                                      left: 20, right: 20, bottom: 10),
                                  itemCount: _con.deals.length,
                                  itemBuilder: (context, index) {
                                    Deal deal = _con.deals[index];
                                    return DealItem(deal: deal);
                                  }),
                          heightSizedBox(15.0),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
