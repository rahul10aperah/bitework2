import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/foot_item.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

class PickUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GridView.builder(
          shrinkWrap: true,
          primary: false,
          padding: EdgeInsets.symmetric(horizontal: 10),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            crossAxisSpacing: 10,
            mainAxisSpacing: 25,
            childAspectRatio: 0.65,
          ),
          itemCount: 9,
          itemBuilder: (context, index) {
            return FootItem();
          },
        ),
        heightSizedBox(15.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: BigButton(
            color: darkBlueColor,
            title: "Load More Food",
            onTap: () {},
          ),
        ),
        heightSizedBox(15.0),
      ],
    );
  }
}
