import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/foot_item.dart';
import 'package:comp1/Elements/loader.dart';
import 'package:comp1/Model/category.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '/Controller/home_controller.dart';
import '/Elements/base_appbar.dart';
import '/util/common.dart';
import '/util/style.dart';

class FoodAvailable extends StatefulWidget {
  final String? whichTab;
  final List<Category>? categories;

  FoodAvailable({this.categories, this.whichTab});

  @override
  _FoodAvailableState createState() => _FoodAvailableState();
}

class _FoodAvailableState extends StateMVC<FoodAvailable> {
  late HomeController _con;

  _FoodAvailableState() : super(HomeController()) {
    _con = controller as HomeController;
  }

  @override
  void initState() {
    super.initState();
    _con.whichTab = widget.whichTab ?? '';
    _con.getHomeData(context);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: redColor,
        appBar: BaseAppBar(
            title: 'Foods Available', elevation: 0.0, centerTitle: true),
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              alignment: Alignment.bottomCenter,
              color: primaryColor,
              height: getHeight(context) / 1.7,
            ),
            CustomScrollView(
              primary: true,
              // shrinkWrap: false,
              slivers: <Widget>[
                SliverAppBar(
                  backgroundColor: redColor,
                  expandedHeight: 75,
                  elevation: 0,
                  leading: heightSizedBox(0.0),
                  flexibleSpace: FlexibleSpaceBar(
                    collapseMode: CollapseMode.parallax,
                    background: Container(
                      color: redColor,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 45,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 8),
                                decoration: fieldDecoration,
                                child: TextField(
                                  cursorColor: Colors.black,
                                  maxLines: 1,
                                  maxLength: 30,
                                  decoration: InputDecoration(
                                    hintText: "Search food or chef",
                                    border: InputBorder.none,
                                    counterText: "",
                                    hintStyle: TextStyle(
                                        fontSize: 14.0, color: Colors.grey),
                                  ),
                                  onChanged: (input) {},
                                ),
                              ),
                            ),
                            widthSizedBox(10.0),
                            Container(
                              height: 45,
                              width: 45,
                              decoration: fieldDecoration,
                              child: Icon(
                                Icons.location_on_outlined,
                                color: redColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height:
                        _con.foodsAvailable.isEmpty ? getHeight(context) : null,
                    decoration: BoxDecoration(
                        color: primaryColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25))),
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: Wrap(
                      runSpacing: 0,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 25),
                          margin: EdgeInsets.only(bottom: 25),
                          height: 40,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            // indicatorColor: Colors.transparent,
                            children: [
                              MyTabButton(
                                onTap: () => _con.tabHandler('Delivery'),
                                color: _con.whichTab == 'Delivery'
                                    ? redColor
                                    : primaryColor,
                                label: 'Delivery',
                                color2: _con.whichTab == 'Delivery'
                                    ? primaryColor
                                    : Colors.grey.shade800,
                              ),
                              MyTabButton(
                                onTap: () => _con.tabHandler('PickUp'),
                                color: _con.whichTab == 'PickUp'
                                    ? redColor
                                    : primaryColor,
                                label: 'Pickup',
                                color2: _con.whichTab == 'PickUp'
                                    ? primaryColor
                                    : Colors.grey.shade800,
                              ),
                            ],
                          ),
                        ),
                        _con.foodsAvailable.isEmpty
                            ? Loader(height: getHeight(context) / 2)
                            : GridView.builder(
                                shrinkWrap: true,
                                primary: false,
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 25,
                                  childAspectRatio: 0.65,
                                ),
                                itemCount: _con.foodsAvailable.length,
                                itemBuilder: (context, index) {
                                  Food food = _con.foodsAvailable[index];
                                  return FootItem(food: food);
                                },
                              ),
                        heightSizedBox(150.0),
                        if (_con.foodsAvailable.isNotEmpty)
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 25),
                            child: BigButton(
                              color: darkBlueColor,
                              title: "Load More Food",
                              onTap: () {},
                            ),
                          ),
                        heightSizedBox(15.0),
                        // Center(
                        //   child: [Delivery(), PickUp()][_con.isTabFirst ? 0 : 1],
                        // ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// class TabButton extends StatelessWidget {
//   final VoidCallback? onTap;
//   final Color? color;
//   final Color? color2;
//   final String? label;
//
//   TabButton({this.onTap, this.color, this.label, this.color2});
//
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: onTap,
//       child: Container(
//         decoration: BoxDecoration(
//             border: Border.all(color: redColor, width: 1),
//             borderRadius: BorderRadius.circular(8),
//             color: color),
//         alignment: Alignment.center,
//         child: Text(
//           label!,
//           style: TextStyle(
//             color: color2,
//           ),
//         ),
//       ),
//     );
//   }
// }
