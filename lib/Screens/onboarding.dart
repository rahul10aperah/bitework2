import 'package:comp1/Controller/user_controller.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/helper.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends StateMVC<OnBoarding> {
  late UserController _con;

  _OnBoardingState() : super(UserController()) {
    _con = controller as UserController;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        backgroundColor: redColor,
        body: Column(
          children: [
            Expanded(
              child: Hero(
                tag: "logo",
                child: Center(
                  child: Image.asset(
                    'assets/app_logo/logo.png',
                    height: 120,
                  ),
                ),
              ),
            ),
            Container(
              decoration: cardDecoration,
              child: Column(
                children: [
                  Container(
                    width: getWidth(context),
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: getWidth(context) / 10),
                        child: Column(
                          children: [
                            heightSizedBox(10.0),
                            SizedBox(
                              width: double.infinity,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  onPrimary: redColor.withOpacity(0.5),
                                  primary: primaryColor,
                                  elevation: 2,
                                  shadowColor: blackColor,
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                ),
                                onPressed: () => _con.signInGoogle(context),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/icons/google.png',
                                      width: 15,
                                    ),
                                    widthSizedBox(10.0),
                                    Text(
                                      "Sign in with Google",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: blackColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          fontFamily: montserratMedium),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            BigButton(
                              title: 'Sign In to comp1',
                              onTap: () => _con.signInDialog(context),
                            ),
                            BigButton(
                                title: "Create an account",
                                onTap: () => _con.registerDialog(context)),
                            heightSizedBox(10.0),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.black,
                            height: 1.4,
                            fontFamily: montserratMedium),
                        children: <TextSpan>[
                          TextSpan(text: 'By proceeding you agree to our  '),
                          TextSpan(
                            text: 'Terms and Conditions',
                            style: blueTextStyle,
                          ),
                          TextSpan(text: '  and  '),
                          TextSpan(
                            text: 'Privacy Statement',
                            style: blueTextStyle,
                          ),
                        ],
                      ),
                    ),
                  ),
                  heightSizedBox(35.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
