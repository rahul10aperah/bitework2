import 'package:comp1/Controller/user_controller.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/input_widgets.dart';
import 'package:comp1/Model/user.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ResetPassword extends StatefulWidget {
  final bool isEmail;
  final User? user;

  ResetPassword({this.isEmail = false, this.user});

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends StateMVC<ResetPassword> {
  late UserController _con;

  _ResetPasswordState() : super(UserController()) {
    _con = controller as UserController;
  }

  TextEditingController emailCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    _con.user = widget.user!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: offWhiteColor,
      appBar: AppBar(
        backgroundColor: offWhiteColor,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0.0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: Form(
          key: _con.formKey,
          child: ListView(
            children: [
              heightSizedBox(getHeight(context) / 4),
              Text(
                "Enter New password",
                style: TextStyle(fontSize: 18, fontFamily: montserratMedium),
              ),
              heightSizedBox(30.0),
              EditTextField(
                validator: validatePassword,
                onChanged: (input) {
                  _con.user.password = input.toString();
                },
                keyBoard: TextInputType.emailAddress,
                labelText: 'Password',
                hint: '6 or more characters',
              ),
              heightSizedBox(40.0),
              BigButton(
                title: 'Submit',
                onTap: () => _con.resetPassword(context, widget.isEmail),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
