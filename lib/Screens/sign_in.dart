// import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:comp1/Controller/user_controller.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/input_widgets.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '/util/style.dart';
import 'forget_password.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends StateMVC<SignIn> {
  late UserController _con;

  _SignInState() : super(UserController()) {
    _con = controller as UserController;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      backgroundColor: transparent,
      body: Stack(
        children: [
          InkWell(
            highlightColor: transparent,
            splashColor: transparent,
            hoverColor: transparent,
            onTap: () => navigationPop(context),
            child: Container(
              height: double.maxFinite,
              width: double.maxFinite,
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: [
                GestureDetector(
                  onTap: () => closeKeyboard(context),
                  child: Container(
                    decoration: cardDecoration,
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 20),
                        child: Form(
                          key: _con.formKey,
                          child: ListView(
                            shrinkWrap: true,
                            children: [
                              Column(
                                children: [
                                  EditTextField(
                                    validator: validateEmail,
                                    onChanged: (input) {
                                      _con.user.email = input.toString();
                                    },
                                    keyBoard: TextInputType.emailAddress,
                                    labelText: 'Email',
                                    hint: 'johndoe@gmail.com',
                                  ),
                                  heightSizedBox(10.0),
                                  EditTextField(
                                    validator: validatePassword,
                                    onChanged: (input) {
                                      _con.user.password = input.toString();
                                    },
                                    keyBoard: TextInputType.visiblePassword,
                                    obcureText: true,
                                    labelText: 'Password',
                                    hint: '6 or more characters',
                                  ),
                                ],
                              ),
                              heightSizedBox(10.0),
                              Align(
                                alignment: Alignment.centerRight,
                                child: InkWell(
                                  child: Text(
                                    'Forget Password',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        height: 2,
                                        fontSize: 13,
                                        fontFamily: montserratSemiBold),
                                  ),
                                  onTap: () =>
                                      navigationPush(context, ForgetPassword()),
                                ),
                              ),
                              BigButton(
                                title: 'Sign In',
                                onTap: () => _con.login(context),
                              ),
                              heightSizedBox(10.0),
                              TextButton(
                                onPressed: () {
                                  navigationPop(context);
                                  _con.registerDialog(context);
                                },
                                child: Text(
                                  "New User? Create Account",
                                  style: TextStyle(
                                    color: darkBlueColor,
                                    fontFamily: montserratSemiBold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
