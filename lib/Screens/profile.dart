import 'package:comp1/Elements/dialogs.dart';

import 'package:comp1/Seller/Fortend/Extra/Dras.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SelVerify.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerReg.dart';

import 'package:flutter/material.dart';

import '/Elements/base_appbar.dart';
import '/util/common.dart';
import '/util/style.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(title: 'Profile'),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: ListView(
          children: [
            ProfileCard(
                cardText: 'Recent Orders',
                imagePath: ('assets/icons/icon1.jpg')),
            ProfileCard(
                cardText: 'your Favourites',
                imagePath: 'assets/icons/icon2.jpg'),
            heightSizedBox(10.0),
            Text('Account Settings', style: liteHeadingTS),
            ProfileCard(
                cardText: 'Personal Information',
                imagePath: 'assets/icons/icon3.jpg'),
            ProfileCard(
                cardText: 'Address & Homes',
                imagePath: ('assets/icons/icon4.jpg')),
            ProfileCard(
                cardText: 'Payment Cards',
                imagePath: ('assets/icons/icon5.jpg')),
            ProfileCard(
                cardText: 'Notifications',
                imagePath: ('assets/icons/icon6.jpg')),
            ProfileCard(
                cardText: 'Swith to selling',
                imagePath: ('assets/icons/icon7.jpg'),
                onTap: () =>
                    // ! GO TO SIGNP PAGE
                    navigationPush(context, SellerVerifyFromProfile())
                // navigationPush(context, SellerDrawerList()),
                ),
            heightSizedBox(10.0),
            Text('Referrals & Credits ', style: liteHeadingTS),
            ProfileCard(
                cardText: 'Invite Friends ',
                imagePath: ('assets/icons/icon8.jpg')),
            ProfileCard(
                cardText: 'Credit & Gift Cards',
                imagePath: ('assets/icons/icon9.jpg')),
            ProfileCard(
                cardText: 'Promo codes',
                imagePath: ('assets/icons/icon10.jpg')),
            heightSizedBox(10.0),
            Text('Support ', style: liteHeadingTS),
            ProfileCard(
                cardText: 'Get Help ', imagePath: ('assets/icons/icon11.jpg')),
            ProfileCard(
                cardText: 'Feedback', imagePath: ('assets/icons/icon12.jpg')),
            heightSizedBox(10.0),
            Text('Legal ', style: liteHeadingTS),
            ProfileCard(
                cardText: 'Term & Condition ',
                imagePath: 'assets/icons/icon13.jpg'),
            ProfileCard(
                onTap: () => logOutDialog(context),
                cardText: 'Logout',
                imagePath: 'assets/icons/icon14.jpg')
          ],
        ),
      ),
    );
  }
}

class ProfileCard extends StatelessWidget {
  final String cardText;
  final String imagePath;
  final VoidCallback? onTap;

  ProfileCard({required this.cardText, required this.imagePath, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    cardText,
                    style:
                        TextStyle(fontSize: 14.0, fontFamily: montserratMedium),
                  ),
                ),
                Image.asset(imagePath, height: 20)
                // Image.asset(profileIcon)
              ],
            ),
          ),
        ),
        divider(),
      ],
    );
  }
}
