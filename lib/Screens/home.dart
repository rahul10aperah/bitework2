import 'package:comp1/Elements/address_bottom_sheet.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/category_item.dart';
import 'package:comp1/Elements/deal_item.dart';
import 'package:comp1/Elements/foot_item.dart';
import 'package:comp1/Elements/loader.dart';
import 'package:comp1/Elements/seller_item.dart';
import 'package:comp1/Model/category.dart';
import 'package:comp1/Screens/deals_offers.dart';
import 'package:comp1/Screens/food_available.dart';
import 'package:comp1/Screens/select_location.dart';
import 'package:comp1/Screens/sellers_view.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart' as mvc;

import '/Controller/home_controller.dart';
import '/util/common.dart';
import '/util/helper.dart';
import '/util/style.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends mvc.StateMVC<Home> {
  late HomeController _con;

  _HomeState() : super(HomeController()) {
    _con = controller as HomeController;
  }

  @override
  void initState() {
    super.initState();
    _con.getHomeData(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        backgroundColor: redColor,
        body: CustomScrollView(
          primary: true,
          shrinkWrap: false,
          slivers: <Widget>[
            SliverAppBar(
              backgroundColor: redColor,
              expandedHeight: 90,
              elevation: 0,
              flexibleSpace: FlexibleSpaceBar(
                collapseMode: CollapseMode.parallax,
                background: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 25, 20, 0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 45,
                          padding:
                              EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                          margin: EdgeInsets.symmetric(vertical: 3),
                          decoration: fieldDecoration,
                          child: InkWell(
                            // onTap: () =>
                            //     navigationPush(context, SelectLocation()),
                            onTap: () => addressBottomSheet(context),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Text(
                                    _con.selectedAdd.description == null
                                        ? 'Select address'
                                        : _con.selectedAdd.description!,
                                    style: _con.selectedAdd.description == null
                                        ? TextStyle(
                                            fontSize: 14.0, color: Colors.grey)
                                        : TextStyle(fontSize: 14),
                                    maxLines: 2,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // child: Container(
                        //   height: 45,
                        //   // padding: EdgeInsets.symmetric(
                        //   //     horizontal: 10, vertical: 8),
                        //   decoration: fieldDecoration,
                        //   child: TextField(
                        //     controller: _con.addressCon,
                        //     cursorColor: Colors.black,
                        //     maxLines: 2,
                        //     style: TextStyle(fontSize: 15),
                        //     // maxLength: 30,
                        //     readOnly: true,
                        //     decoration: InputDecoration(
                        //       hintText: "Select address",
                        //       border: InputBorder.none,
                        //       counterText: "",
                        //       hintStyle: TextStyle(
                        //           fontSize: 14.0, color: Colors.grey),
                        //     ),
                        //     // onChanged: (input) {},
                        //     onTap: () => addressBottomSheet(context),
                        //   ),
                        // ),
                      ),
                      widthSizedBox(10.0),
                      InkWell(
                        onTap: () => navigationPush(context, SelectLocation()),
                        child: Container(
                          height: 45,
                          width: 45,
                          decoration: fieldDecoration,
                          child: Icon(
                            Icons.location_on_outlined,
                            color: redColor,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))),
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Wrap(
                  runSpacing: 15,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      margin: EdgeInsets.only(bottom: 15),
                      height: 40,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        // indicatorColor: Colors.transparent,
                        children: [
                          MyTabButton(
                            onTap: () => navigationPush(
                                context, FoodAvailable(whichTab: 'Delivery')),
                            color: primaryColor,
                            label: 'Delivery',
                            color2: Colors.grey.shade800,
                          ),
                          MyTabButton(
                              onTap: () => navigationPush(
                                  context, FoodAvailable(whichTab: 'PickUp')),
                              color: primaryColor,
                              label: 'Pickup',
                              color2: Colors.grey.shade800),
                        ],
                      ),
                    ),
                    headingRow(label: 'Top Categories'),
                    SizedBox(
                      height: 120,
                      child: _con.categories.isEmpty
                          ? Loader(height: 120)
                          : ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              padding: EdgeInsets.only(left: 20, bottom: 20),
                              itemCount: _con.categories.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                Category category = _con.categories[index];
                                return Padding(
                                  padding: EdgeInsets.only(
                                      left: index == 0 ? 0.0 : 10.0,
                                      right: index == _con.categories.length - 1
                                          ? 10
                                          : 0),
                                  child: CategoryItem(category: category),
                                );
                              }),
                    ),
                    headingRow(
                        label: 'Deals and Offers',
                        onTap: () => navigationPush(context, DealsOffers())),
                    SizedBox(
                      height: 120,
                      child: _con.deals.isEmpty
                          ? Loader(height: 120)
                          : ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              padding: EdgeInsets.only(left: 20, bottom: 10),
                              itemCount: _con.deals.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                Deal deal = _con.deals[index];
                                return Padding(
                                  padding: EdgeInsets.only(
                                      left: index == 0 ? 0.0 : 10.0,
                                      right: index == _con.deals.length - 1
                                          ? 10
                                          : 0),
                                  child: DealItem(deal: deal),
                                );
                              }),
                    ),
                    headingRow(
                        label: 'Foods Available',
                        onTap: () => navigationPush(context, FoodAvailable())),
                    SizedBox(
                      height: 180,
                      child: _con.foods.isEmpty
                          ? Loader(height: 180)
                          : ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              padding: EdgeInsets.only(left: 20, bottom: 10),
                              itemCount: _con.foods.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                Food food = _con.foods[index];
                                return Padding(
                                  padding: EdgeInsets.only(
                                      left: index == 0 ? 0.0 : 10.0,
                                      right: index == _con.foods.length - 1
                                          ? 10
                                          : 0),
                                  child: FootItem(food: food),
                                );
                              }),
                    ),
                    headingRow(
                      label: 'Our Famous Bitevillars',
                      onTap: () => navigationPush(
                          context, SellersView(sellers: _con.sellers)),
                    ),
                    SizedBox(
                      height: 180,
                      child: _con.sellers.isEmpty
                          ? Loader(height: 180)
                          : ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              padding: EdgeInsets.only(left: 20, bottom: 10),
                              itemCount: _con.sellers.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                Seller seller = _con.sellers[index];
                                return Padding(
                                  padding: EdgeInsets.only(
                                      left: index == 0 ? 0.0 : 10.0,
                                      right: index == _con.sellers.length - 1
                                          ? 10
                                          : 0),
                                  child: SellerItem(seller: seller),
                                );
                              }),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void addressBottomSheet(context) async {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (builder) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return AddressBottomSheet();
          },
        );
      },
    ).then((value) {
      if (value != null) {
        setState(() => _con.selectedAdd = value);

        // _con.addressCon.text = _con.selectedAdd.description!;
      }
      // _con.getAddress(context);
      // you can do what you need here
      // setState etc.
    });
  }
}

Widget headingRow({String? label, VoidCallback? onTap}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label!,
          style: TextStyle(color: textBlueColor, fontSize: 18),
        ),
        if (onTap != null)
          InkWell(
            onTap: onTap,
            child: Row(
              children: [
                Text(
                  'View All',
                  style: TextStyle(color: redColor, fontSize: 14),
                ),
                widthSizedBox(5.0),
                Container(
                  padding: EdgeInsets.all(2),
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: redColor),
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: primaryColor,
                    size: 14,
                  ),
                ),
              ],
            ),
          ),
      ],
    ),
  );
}
