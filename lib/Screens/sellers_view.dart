import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/foot_item.dart';
import 'package:comp1/Elements/loader.dart';
import 'package:comp1/Elements/seller_view_item.dart';
import 'package:comp1/Model/category.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '/Controller/home_controller.dart';
import '/Elements/base_appbar.dart';
import '/util/common.dart';
import '/util/style.dart';

class SellersView extends StatefulWidget {
  final List<Seller>? sellers;

  SellersView({this.sellers});

  @override
  _SellersViewState createState() => _SellersViewState();
}

class _SellersViewState extends StateMVC<SellersView> {
  late HomeController _con;

  _SellersViewState() : super(HomeController()) {
    _con = controller as HomeController;
  }

  @override
  void initState() {
    super.initState();
    _con.getHomeData(context);
    // print(widget.sellers!.length);
    // _con.sellers.addAll(widget.sellers!);
    // _con.getSellersFood(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: redColor,
      appBar:
          BaseAppBar(title: 'Bitevillars', elevation: 0.0, centerTitle: true),
      body: CustomScrollView(
        primary: true,
        shrinkWrap: false,
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Container(
              height: _con.sellers.isEmpty ? getHeight(context) : null,
              decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25))),
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Wrap(
                runSpacing: 0,
                children: [
                  _con.sellers.isEmpty
                      ? Loader(height: getHeight(context) / 1.2)
                      : ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          padding: EdgeInsets.only(left: 10, bottom: 10),
                          itemCount: _con.sellers.length,
                          itemBuilder: (context, index) {
                            Seller seller = _con.sellers[index];
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: SellerViewItem(seller: seller),
                                ),
                                SizedBox(
                                  height: 180,
                                  child: _con.foods.isEmpty
                                      ? Loader(height: 180)
                                      : ListView.builder(
                                          shrinkWrap: true,
                                          primary: false,
                                          padding: EdgeInsets.only(
                                              left: 15, bottom: 10),
                                          itemCount: _con.foods.length,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, index) {
                                            Food food = _con.foods[index];
                                            return seller.sellerId ==
                                                    food.sellerId
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 10),
                                                    child: FootItem(food: food))
                                                : heightSizedBox(0.0);
                                          }),
                                ),
                              ],
                            );
                          }),
                  heightSizedBox(15.0),
                  if (_con.foods.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: BigButton(
                        color: darkBlueColor,
                        title: "Load More Bitevillars",
                        onTap: () {},
                      ),
                    ),
                  heightSizedBox(15.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class TabButton extends StatelessWidget {
  final VoidCallback? onTap;
  final Color? color;
  final Color? color2;
  final String? label;

  TabButton({this.onTap, this.color, this.label, this.color2});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: redColor, width: 1),
            borderRadius: BorderRadius.circular(8),
            color: color),
        alignment: Alignment.center,
        child: Text(
          label!,
          style: TextStyle(
            color: color2,
          ),
        ),
      ),
    );
  }
}
