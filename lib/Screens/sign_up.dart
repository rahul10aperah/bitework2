import 'package:comp1/Controller/user_controller.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/input_widgets.dart';
import 'package:comp1/Elements/loader.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:comp1/util/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SignUp extends StatefulWidget {
  final GoogleSignInAccount? account;

  const SignUp({Key? key, this.account}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends StateMVC<SignUp> {
  late UserController _con;

  _SignUpState() : super(UserController()) {
    _con = controller as UserController;
  }

  // late SignUpBloc comp1SignUpBloc;
  //
  // region Init
  @override
  void initState() {
    super.initState();
    if (widget.account != null) {
      List<String> baseStr = widget.account!.displayName!.split(" ");

      if (baseStr.length >= 2) {
        _con.user.lName = baseStr.last;
        baseStr.removeLast();
        for (String? s in baseStr) _con.user.fName += s!;
      } else
        _con.user.fName = baseStr[0];

      _con.fNameCon.text = _con.user.fName;
      _con.lNameCom.text = _con.user.lName!;
      _con.emailCon.text = widget.account!.email;
      _con.user.email = _con.emailCon.text;
    }
  }

  // endregion

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: transparent,
      body: Stack(
        children: [
          InkWell(
            highlightColor: transparent,
            splashColor: transparent,
            hoverColor: transparent,
            onTap: () => Navigator.pop(context),
            child: Container(
              height: double.maxFinite,
              width: double.maxFinite,
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () => closeKeyboard(context),
              child: Container(
                decoration: cardDecoration,
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                child: Card(
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 20),
                    child: Form(
                      key: _con.formKey,
                      child: ListView(
                        shrinkWrap: true,
                        children: [
                          if (widget.account != null &&
                              widget.account?.photoUrl != null)
                            Align(
                              alignment: Alignment.center,
                              child: Stack(
                                alignment: Alignment.bottomRight,
                                children: [
                                  ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50.0)),
                                    child: CachedNetworkImage(
                                      height: 90,
                                      width: 90,
                                      fit: BoxFit.cover,
                                      imageUrl: widget.account!.photoUrl!,
                                      placeholder: (context, url) =>
                                          Loader(height: 100),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    ),
                                  ),
                                  Image.asset(
                                    'assets/icons/gforgoogle.png',
                                    height: 20,
                                    width: 20,
                                    fit: BoxFit.cover,
                                  ),
                                ],
                              ),
                            ),
                          if (widget.account != null)
                            Column(
                              children: [
                                heightSizedBox(15.0),
                                Text(
                                  'Welcome, ${_con.user.fName}, Create your easy username and password',
                                  style:
                                      TextStyle(fontFamily: montserratMedium),
                                  textAlign: TextAlign.center,
                                ),
                                heightSizedBox(15.0),
                              ],
                            ),
                          Row(
                            children: [
                              Expanded(
                                child: EditTextField(
                                  validator: validateName,
                                  controller: _con.fNameCon,
                                  onChanged: (input) {
                                    _con.user.fName = input.toString();
                                  },
                                  keyBoard: TextInputType.text,
                                  labelText: 'First Name',
                                ),
                              ),
                              widthSizedBox(10.0),
                              Expanded(
                                child: EditTextField(
                                  validator: validateName,
                                  controller: _con.lNameCom,
                                  onChanged: (input) {
                                    _con.user.lName = input.toString();
                                  },
                                  keyBoard: TextInputType.text,
                                  labelText: 'Last Name',
                                ),
                              ),
                            ],
                          ),
                          heightSizedBox(10.0),
                          EditTextField(
                            validator: validateEmail,
                            controller: _con.emailCon,
                            onChanged: (input) {
                              _con.user.email = input.toString();
                            },
                            keyBoard: TextInputType.emailAddress,
                            labelText: 'Email',
                            hint: 'johndoe@gmail.com',
                          ),
                          heightSizedBox(10.0),
                          EditTextField(
                            validator: validatePassword,
                            onChanged: (input) {
                              _con.user.password = input.toString();
                            },
                            keyBoard: TextInputType.visiblePassword,
                            obcureText: true,
                            labelText: 'Password',
                            hint: '6 or more characters',
                          ),
                          heightSizedBox(10.0),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: !_con.isReferral
                                ? InkWell(
                                    onTap: () => setState(() =>
                                        _con.isReferral = !_con.isReferral),
                                    child: Text(
                                      'Referral Code',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          height: 2,
                                          fontSize: 13,
                                          fontFamily: montserratSemiBold),
                                    ),
                                  )
                                : EditTextField(
                                    validator: validateField,
                                    onChanged: (input) {
                                      _con.user.referrerCode = input.toString();
                                    },
                                    keyBoard: TextInputType.text,
                                    obcureText: true,
                                    labelText: 'Referral Code(Optional)',
                                    // hint: '',
                                  ),
                          ),
                          heightSizedBox(10.0),
                          BigButton(
                              title: 'Create Account',
                              onTap: () {
                                if (widget.account == null)
                                  _con.register(context);
                                else {
                                  _con.registerFromGoogle(
                                      context, widget.account);
                                }
                              }),
                          heightSizedBox(10.0),
                          TextButton(
                            onPressed: () {
                              navigationPop(context);
                              _con.signInDialog(context);
                            },
                            child: Text(
                              "Already User? Sign In",
                              style: TextStyle(
                                color: darkBlueColor,
                                fontFamily: montserratSemiBold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
