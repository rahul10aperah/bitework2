import 'dart:async';

import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Elements/input_widgets.dart';
import 'package:comp1/Elements/loader.dart';
import 'package:comp1/Model/address.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:google_maps_place_picker/google_maps_place_picker.dart';
// import 'package:location/location.dart' as LocationManager;
import 'package:mvc_pattern/mvc_pattern.dart';

class SelectLocation extends StatefulWidget {
  @override
  _SelectLocationState createState() => _SelectLocationState();
}

class _SelectLocationState extends StateMVC<SelectLocation> {
  // SellerMapController _con;
  //
  // _SellerMapState() : super(SellerMapController()) {
  //   _con = controller;
  // }

  // LocationData currentLocation;
  Position? position;
  Address address = Address();

  @override
  void initState() {
    // _con.getCurrentLocation();
    // getUserLocation();
    getPosition();
    super.initState();
  }

  getPosition() async {
    _determinePosition().then((value) {
      position = value;
      address.latitude = position!.latitude;
      address.longitude = position!.longitude;
      setState(() {});
    });
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  // Future<LatLng> getUserLocation() async {
  //   final location = LocationManager.Location();
  //
  //   try {
  //     currentLocation = await location.getLocation();
  //     setState(() {});
  //
  //     final lat = currentLocation.latitude;
  //
  //     final lng = currentLocation.longitude;
  //
  //     final center = LatLng(lat, lng);
  //
  //     return center;
  //   } on Exception {
  //     currentLocation = null;
  //     return LatLng(0.0, 0.0);
  //   }
  // }

  // BitmapDescriptor customIcon1;

  // createMarker(context) {
  //   if (customIcon1 == null) {
  //     ImageConfiguration configuration = createLocalImageConfiguration(context);
  //
  //     BitmapDescriptor.fromAssetImage(
  //             configuration, 'assets/Images/my_marker.png')
  //         .then((icon) {
  //       setState(() {
  //         customIcon1 = icon;
  //       });
  //     });
  //   }
  // }

  // Completer<GoogleMapController> _controller = Completer();

  // final CameraPosition _kGooglePlex = CameraPosition(
  //   target: LatLng(37.42796133580664, -122.085749655962),
  //   zoom: 14.4746,
  // );
  int markerId = 1;

  // final CameraPosition _kLake = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(37.43296265331129, -122.08832357078792),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    // return Scaffold(
    //   body: GoogleMap(
    //     mapType: MapType.normal,
    //     initialCameraPosition: _kGooglePlex,
    //     onMapCreated: (GoogleMapController controller) {
    //       _controller.complete(controller);
    //     },
    //   ),
    //   floatingActionButton: FloatingActionButton.extended(
    //     onPressed: _goToTheLake,
    //     label: Text('To the lake!'),
    //     icon: Icon(Icons.directions_boat),
    //   ),
    // );
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: BaseAppBar(title: 'Select Address'),
      body: position == null
          ? Loader(height: getHeight(context))
          : Column(
              children: [
                Expanded(
                  child: GoogleMap(
                    mapToolbarEnabled: false,
                    mapType: MapType.normal,
                    myLocationEnabled: true,
                    // myLocationButtonEnabled: true,
                    // initialCameraPosition: _con.cameraPosition,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(position!.latitude, position!.longitude),
                      zoom: 14.4746,
                    ),
                    // markers: Set.from(_con.allMarkers),
                    onTap: (pos) {
                      print(pos);
                      print('pos');
                      // print('pos');

                      // Marker f = Marker(
                      //     markerId: MarkerId(markerId.toString()),
                      //     icon: customIcon1,
                      //     position: pos,
                      //     // position: LatLng(23.025857, 72.543423),
                      //     onTap: () {});

                      // setState(() {
                      //   _con.allMarkers.add(f);
                      // });
                      markerId++;
                    },
                    // onMapCreated: (GoogleMapController controller) {
                    //   _con.mapController.complete(controller);
                    // },
                    onCameraMove: (CameraPosition cameraPosition) {
                      print(cameraPosition.target.latitude);
                      print(cameraPosition.target.longitude);
                      address.latitude = cameraPosition.target.latitude;
                      address.longitude = cameraPosition.target.longitude;
                      // _con.cameraPosition = cameraPosition;
                    },
                    onCameraIdle: () {
                      // _con.getMarketsOfArea();
                    },
                    // polylines: _con.polylines,
                  ),
                ),
                Container(
                  color: offWhiteColor,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 25.0, vertical: 15.0),
                    child: Column(
                      children: [
                        EditTextField(
                          onChanged: (value) {},
                          keyBoard: TextInputType.emailAddress,
                          labelText: 'Apt or Flat',
                          hint: 'Enter apartment or flat detail',
                        ),
                        heightSizedBox(8.0),
                        EditTextField(
                          onChanged: (value) {},
                          keyBoard: TextInputType.emailAddress,
                          labelText: 'Landmark',
                          hint: 'Enter landmark',
                        ),
                        heightSizedBox(10.0),
                        BigButton(
                          onTap: () {
                            // navigationPush(context, PlacePickerEx());
                          },
                          title: "Add this address",

                          // onTap: () => navigationPush(context, InputForOtp()),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
    );
  }
}

// class PlacePickerEx extends StatefulWidget {
//   const PlacePickerEx({Key? key}) : super(key: key);
//
//   static final kInitialPosition = LatLng(-33.8567844, 151.213108);
//
//   @override
//   _PlacePickerExState createState() => _PlacePickerExState();
// }
//
// class _PlacePickerExState extends State<PlacePickerEx> {
//   late PickResult selectedPlace;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text("Google Map Place Picer Demo"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: <Widget>[
//               ElevatedButton(
//                 child: Text("Load Google Map"),
//                 onPressed: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                       builder: (context) {
//                         return PlacePicker(
//                           apiKey: 'AIzaSyDvHPQaJcUZp6WbYS7YOj0c6ziF2mQ0MU0',
//                           initialPosition: PlacePickerEx.kInitialPosition,
//                           useCurrentLocation: true,
//                           selectInitialPosition: true,
//
//                           //usePlaceDetailSearch: true,
//                           onPlacePicked: (result) {
//                             selectedPlace = result;
//                             Navigator.of(context).pop();
//                             setState(() {});
//                           },
//                           //forceSearchOnZoomChanged: true,
//                           //automaticallyImplyAppBarLeading: false,
//                           //autocompleteLanguage: "ko",
//                           //region: 'au',
//                           //selectInitialPosition: true,
//                           // selectedPlaceWidgetBuilder: (_, selectedPlace, state, isSearchBarFocused) {
//                           //   print("state: $state, isSearchBarFocused: $isSearchBarFocused");
//                           //   return isSearchBarFocused
//                           //       ? Container()
//                           //       : FloatingCard(
//                           //           bottomPosition: 0.0, // MediaQuery.of(context) will cause rebuild. See MediaQuery document for the information.
//                           //           leftPosition: 0.0,
//                           //           rightPosition: 0.0,
//                           //           width: 500,
//                           //           borderRadius: BorderRadius.circular(12.0),
//                           //           child: state == SearchingState.Searching
//                           //               ? Center(child: CircularProgressIndicator())
//                           //               : RaisedButton(
//                           //                   child: Text("Pick Here"),
//                           //                   onPressed: () {
//                           //                     // IMPORTANT: You MUST manage selectedPlace data yourself as using this build will not invoke onPlacePicker as
//                           //                     //            this will override default 'Select here' Button.
//                           //                     print("do something with [selectedPlace] data");
//                           //                     Navigator.of(context).pop();
//                           //                   },
//                           //                 ),
//                           //         );
//                           // },
//                           // pinBuilder: (context, state) {
//                           //   if (state == PinState.Idle) {
//                           //     return Icon(Icons.favorite_border);
//                           //   } else {
//                           //     return Icon(Icons.favorite);
//                           //   }
//                           // },
//                         );
//                       },
//                     ),
//                   );
//                 },
//               ),
//               selectedPlace == null
//                   ? Container()
//                   : Text(selectedPlace.formattedAddress ?? ""),
//             ],
//           ),
//         ));
//   }
// }
