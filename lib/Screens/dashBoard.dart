import 'package:comp1/Screens/profile.dart';
import 'package:comp1/util/helper.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

import 'home.dart';

// ignore: must_be_immutable
class DashBoard extends StatefulWidget {
  int? currentTab;
  Widget? currentPage;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  DashBoard({Key? key, this.currentTab}) {
    if (currentTab == null) currentTab = 0;
    // if (currentTab != null) {
    //   if (currentTab is RouteArgument) {
    //     routeArgument = currentTab;
    //     currentTab = int.parse(currentTab.id);
    //   }
    // } else {
    //   currentTab = 0;
    // }
  }

  @override
  _DashBoardState createState() {
    return _DashBoardState();
  }
}

class _DashBoardState extends State<DashBoard> {
  @override
  initState() {
    super.initState();
    _selectTab(widget.currentTab!);
  }

  // @override
  // void didUpdateWidget(DashBoard oldWidget) {
  //   _selectTab(oldWidget.currentTab);
  //   super.didUpdateWidget(oldWidget);
  // }

  void _selectTab(int tabItem) {
    setState(() {
      widget.currentTab = tabItem;
      switch (tabItem) {
        case 0:
          widget.currentPage = Container();
          break;
        case 1:
          widget.currentPage = Home();
          break;
        case 2:
          widget.currentPage = Profile();
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        key: widget.scaffoldKey,
        body: widget.currentPage,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: redColor,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          unselectedItemColor: darkBlueColor,
          iconSize: 25,
          backgroundColor: Colors.white,
          currentIndex: widget.currentTab!,
          onTap: (int i) {
            this._selectTab(i);
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(widget.currentTab == 0
                  ? Icons.shopping_bag_rounded
                  : Icons.shopping_bag_outlined),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(widget.currentTab == 1
                  ? Icons.home_rounded
                  : Icons.home_outlined),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(widget.currentTab == 2
                  ? Icons.person_rounded
                  : Icons.person_outline),
              label: '',
            ),
          ],
        ),
      ),
    );
  }
}
