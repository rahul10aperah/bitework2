import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

class AddressItem extends StatelessWidget {
  final IconData? icon;
  final String? title;
  final String? subTitle;
  final Color? color;
  final VoidCallback? onTap;

  AddressItem({this.icon, this.title, this.subTitle, this.color, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: onTap,
          child: Row(
            children: [
              Icon(
                icon,
                color: color,
                size: 30.0,
              ),
              widthSizedBox(20.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (title != null)
                      Text(
                        title!,
                        style: TextStyle(
                            fontSize: 20.0,
                            color: color,
                            fontFamily: montserratSemiBold),
                      ),
                    Text(
                      subTitle!,
                      maxLines: title != null ? 1 : 2,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          fontSize: 15,
                          color: color!.withOpacity(0.6),
                          fontFamily: montserratSemiBold),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        heightSizedBox(7.0),
        if (color != redColor)
          Padding(
            padding: const EdgeInsets.only(left: 50.0),
            child: Divider(thickness: 1, color: Colors.grey),
          ),
      ],
    );
  }
}
