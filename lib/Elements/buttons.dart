import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

class BigButton extends StatelessWidget {
  final String? title;
  final VoidCallback? onTap;
  final Color? color;

  BigButton({@required this.title, @required this.onTap, this.color});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: color ?? redColor,
          // elevation: 2.0,
          shadowColor: color?.withOpacity(0.5) ?? redColor.withOpacity(0.5),
          padding: EdgeInsets.symmetric(vertical: 10),
          // shape:
          //     RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
        ),
        onPressed: onTap,
        child: Text(
          title!,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 16,
              fontFamily: montserratSemiBold),
        ),
      ),
    );
  }
}

class MyTabButton extends StatelessWidget {
  final VoidCallback? onTap;
  final Color? color;
  final Color? color2;
  final String? label;
  final Color? borderColor;

  MyTabButton(
      {this.onTap, this.color, this.borderColor, this.label, this.color2});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 110,
        decoration: BoxDecoration(
            border: Border.all(
                color: borderColor != null ? borderColor! : redColor,
                width: 1.5),
            borderRadius: BorderRadius.circular(8),
            color: color),
        alignment: Alignment.center,
        child: Text(
          label!,
          style: TextStyle(color: color2, fontFamily: montserratSemiBold),
        ),
      ),
    );
  }
}

// class BigButton2 extends StatelessWidget {
//   final String? title;
//   final Function? onTap;
//   final Color? color;
//
//   BigButton2({@required this.title, @required this.onTap, this.color});
//
//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButton(
//       style: ElevatedButton.styleFrom(
//         primary: color ?? redColor,
//         // elevation: 2.0,
//         shadowColor: color?.withOpacity(0.5) ?? redColor.withOpacity(0.5),
//         padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//         // shape:
//         //     RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
//       ),
//       onPressed: () => onTap!(),
//       child: Text(
//         title!,
//         textAlign: TextAlign.center,
//         // style: TextStyle(
//         //     color: Colors.white,
//         //     fontWeight: FontWeight.bold,
//         //     fontSize: 16,
//         //     fontFamily: "MontserratSemiBold"),
//       ),
//     );
//   }
// }
