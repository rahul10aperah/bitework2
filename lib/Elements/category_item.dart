import 'package:comp1/Model/category.dart';
import 'package:comp1/util/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'loader.dart';

class CategoryItem extends StatelessWidget {
  final Category? category;

  CategoryItem({this.category});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            // child: Image.asset(
            //   'assets/app_logo/logo-cropped.png',
            //   height: 100,
            //   width: 100,
            //   fit: BoxFit.cover,
            // ),
            child: CachedNetworkImage(
              height: 100,
              width: 100,
              fit: BoxFit.cover,
              imageUrl: category!.url!,
              placeholder: (context, url) => Loader(height: 100),
              // placeholder: (context, url) => Image.asset(
              //   'assets/img/loading.gif',
              //   fit: BoxFit.cover,
              // ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Container(
            height: 100,
            width: 100,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.black26,
              shape: BoxShape.circle,
            ),
            child: Text(
              category!.name!,
              overflow: TextOverflow.ellipsis,
              softWrap: false,
              maxLines: 2,
              style: TextStyle(fontSize: 15, color: primaryColor),
            ),
          ),
        ],
      ),
    );
  }
}
