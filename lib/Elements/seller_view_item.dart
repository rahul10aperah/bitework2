import 'package:comp1/Model/category.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'loader.dart';

class SellerViewItem extends StatelessWidget {
  final Seller? seller;

  SellerViewItem({this.seller});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Card(
            margin: EdgeInsets.zero,
            elevation: 3,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  width: 110,
                  height: 110,
                  imageUrl: seller!.url!,
                  placeholder: (context, url) => Loader(height: 100),
                  // placeholder: (context, url) => Image.asset(
                  //   'assets/img/loading.gif',
                  //   fit: BoxFit.cover,
                  // ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
          widthSizedBox(15.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                seller!.kitchenName!,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                maxLines: 2,
                style: TextStyle(
                  color: darkBlueColor,
                  fontWeight: FontWeight.bold,
                  fontFamily: montserratMedium,
                  fontSize: 15.0,
                ),
              ),
              heightSizedBox(7.0),
              Text(
                'Cuisine:North indians ',
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                maxLines: 2,
                style: greyTextStyle,
              ),
              heightSizedBox(7.0),
              Text(
                '3Kms',
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                maxLines: 2,
                style: greyTextStyle,
              ),
              heightSizedBox(7.0),
              Row(
                children: [
                  Text(
                    '5.0   ',
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    maxLines: 2,
                    style: greyTextStyle.copyWith(color: blackColor),
                  ),
                  Row(
                    children: List.generate(
                      5,
                      (index) => Icon(
                        Icons.star,
                        color: Colors.orange,
                        size: 17,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
