import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EditTextField extends StatefulWidget {
  final List<TextInputFormatter>? inputFormatterData;
  final TextEditingController? controller;
  final String? hint;
  final bool? readOnly;
  final TextInputType? keyBoard;
  final FocusNode? focusNode;
  final TextAlign? textAlign;
  final int? maxLength;
  final int? maxLines;
  final double? horizontal;
  final int? lengthLimiting;
  final Function? onChanged;
  final Function? validator;
  final String? labelText;
  final String? initValue;
  final bool? obcureText;
  final Widget? prefixIcon;

  EditTextField({
    this.inputFormatterData,
    this.controller,
    this.hint,
    this.readOnly,
    this.keyBoard,
    this.textAlign,
    this.focusNode,
    this.maxLength,
    this.maxLines,
    this.horizontal,
    this.lengthLimiting,
    this.onChanged,
    this.validator,
    this.labelText,
    this.initValue,
    this.obcureText,
    this.prefixIcon,
  });

  @override
  _EditTextFieldState createState() => _EditTextFieldState();
}

class _EditTextFieldState extends State<EditTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        widget.labelText != null
            ? Text(widget.labelText!, style: labelTextStyle)
            : heightSizedBox(0.0),
        // heightSizedBox(2.0),
        TextFormField(
          onTap: () {},
          readOnly: widget.readOnly ?? false,
          textAlign: widget.textAlign ?? TextAlign.start,
          cursorColor: Colors.black,
          expands: false,
          focusNode: widget.focusNode,
          validator: (value) => widget.validator!(value),

          inputFormatters: widget.inputFormatterData,
          scrollPadding: EdgeInsets.zero,
          // initialValue: widget.initValue,
          style: TextStyle(
              fontSize: 15.0,
              color: Colors.black87,
              fontFamily: montserratMedium),
          controller: widget.controller,
          keyboardType:
              widget.keyBoard != null ? widget.keyBoard : TextInputType.text,
          maxLines: 1,

          maxLength: widget.maxLength,
          obscureText: widget.obcureText ?? false,
          onChanged: (val) => widget.onChanged!(val),
          decoration: InputDecoration(
              focusColor: Colors.black,
              hoverColor: Colors.black,
              suffixIcon: widget.prefixIcon,
              prefixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
              prefixIcon: widget.initValue != null
                  ? Text(
                      widget.initValue.toString(),
                      style: TextStyle(fontSize: 17.0, color: Colors.black87),
                    )
                  : null,

              // prefixText: widget.initValue,

              // prefixStyle: TextStyle(fontSize: 17.0, color: Colors.black87),
              // prefixIcon: Text(''),
              contentPadding: EdgeInsets.symmetric(
                  vertical: 5.0, horizontal: widget.horizontal ?? 0),
              isDense: true,
              // labelStyle: labelTextStyle,
              // labelText: widget.labelText,
              hintText: widget.hint,
              // hintStyle: hintTextStyle,
              // filled: true,
              // fillColor: Colors.white,
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: redColor, width: 1.0),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0))),
        ),
      ],
    );
  }
}

class OtpTextField extends StatefulWidget {
  final List<TextInputFormatter>? inputFormatterData;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  // final String? hint;
  // final TextInputType? keyBoard;
  final int? maxLength;
  // final int? maxLines;
  final int? lengthLimiting;
  final Function? onChanged;
  // final Function? validator;
  String? Function(String?)? validator;
  // final String? labelText;

  OtpTextField({
    this.inputFormatterData,
    this.controller,
    this.focusNode,
    // this.hint,
    // this.keyBoard,
    this.maxLength,
    // this.maxLines,
    this.lengthLimiting,
    this.onChanged,
    this.validator,
    // this.labelText,
  });

  @override
  _OtpTextFieldState createState() => _OtpTextFieldState();
}

class _OtpTextFieldState extends State<OtpTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: widget.key,
      cursorColor: Colors.grey,
      expands: false,
      focusNode: widget.focusNode,
      inputFormatters: widget.inputFormatterData,
      // validator: (value) => widget.validator!(value),
      validator: widget.validator,
      scrollPadding: EdgeInsets.zero,
      style: TextStyle(
          fontSize: 17.0, color: Colors.black54, fontFamily: montserratMedium),
      controller: widget.controller,
      textAlign: TextAlign.center,
      keyboardType: TextInputType.number,
      // maxLines: widget.maxLines,
      maxLength: widget.maxLength,
      onChanged: (val) => widget.onChanged!(val),
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.symmetric(vertical: 5.0),
        labelStyle: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
            fontFamily: montserratMedium),
        // labelText: widget.labelText,
        // hintText: widget.hint,
        // hintStyle: TextStyle(
        //     color: Colors.grey.shade500,
        //     letterSpacing: 0.6,
        //     fontSize: 17.0,
        //     fontFamily: montserratMedium),
        filled: true,
        fillColor: Colors.transparent,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 2),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red, width: 2),
        ),
      ),
    );
  }
}
