import 'package:comp1/Model/category.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'loader.dart';

class DealItem extends StatelessWidget {
  final Deal? deal;

  DealItem({this.deal});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Card(
        margin: EdgeInsets.zero,
        elevation: 3,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              // height: 110,
              // width: 110,
              imageUrl: deal!.dealDetails!,
              placeholder: (context, url) => Loader(height: 100),
              // placeholder: (context, url) => Image.asset(
              //   'assets/img/loading.gif',
              //   fit: BoxFit.cover),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
      ),
    );
  }
}
