import 'package:flutter/material.dart';

import '/Repository/user_repository.dart';
import '/Screens/onboarding.dart';
import '/util/common.dart';
import '/util/input_validation.dart';
import '/util/style.dart';

Future<dynamic> logOutDialog(context) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(
          'Are you sure you want to \n LogOut',
          textAlign: TextAlign.center,
          style: TextStyle(fontFamily: montserratMedium, fontSize: 16),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('No', style: TextStyle(color: darkBlueColor)),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          TextButton(
            child: Text('Yes', style: TextStyle(color: Colors.red)),
            onPressed: () {
              logout().then(
                  (value) => navigationPushReplacement(context, OnBoarding()));
            },
          )
        ],
      );
    },
  );
}

Future<dynamic> errorDialog(context,
    {String? message, VoidCallback? onTap, int? show}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Column(
          children: [
            Icon(
              Icons.warning_amber_outlined,
              color: Colors.red,
              size: 50,
            ),
            heightSizedBox(20.0),
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  message ?? '',
                  maxLines: 5,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, color: Colors.black),
                )
              ],
            ),
          ],
        ),
        actions: <Widget>[
          show == 1
              ? TextButton(
                  child: Text('Cancel', style: TextStyle(color: redColor)),
                  onPressed: () => Navigator.pop(context))
              : widthSizedBox(0.0),
          TextButton(
              child: Text('OK', style: TextStyle(color: Colors.red)),
              onPressed: onTap),
        ],
      );
    },
  );
}

Future<dynamic> addReportDialog(context, con, index) {
  TextEditingController textCon = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 10),
            titlePadding: EdgeInsets.only(top: 10),
            content: Container(
              // padding: EdgeInsets.all(5),
              // decoration: BoxDecoration(
              //     borderRadius: BorderRadius.circular(5),
              //     color: Colors.white,
              //     border: Border.all(color: Colors.grey)),
              child: Form(
                key: formKey,
                child: TextFormField(
                  validator: (va) => validateField(va ?? ''),
                  controller: textCon,
                  cursorColor: Colors.black87,
                  maxLines: 4,
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.black87,
                      fontFamily: montserratMedium),
                  decoration: InputDecoration(
                      hintText: 'Type your message here',
                      // hintStyle:
                      //     TextStyle(fontSize: 13.0, color: Colors.black54),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.grey, width: 1.0))),
                  onChanged: (value) {},
                ),
              ),
            ),
            actions: [
              TextButton(
                  child: Text('Cancel', style: TextStyle(color: greyColor)),
                  onPressed: () => Navigator.pop(context)),
              TextButton(
                  child: Text(
                    'OK',
                    style: TextStyle(color: redColor),
                  ),
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      con.addRepost(context, index, textCon.text);
                    }
                  }),
            ],
          );
        },
      );
    },
  );
}

// ! generate promcode
Future<dynamic> generatePromoDialog(
  context, {
  VoidCallback? onTap,
}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        contentPadding: EdgeInsets.fromLTRB(15, 2, 2, 20),
        titlePadding: EdgeInsets.fromLTRB(0, 0, 0, 20),
        title: Container(
          decoration: BoxDecoration(
            color: greyColor,
            border: Border.all(
              // color: black,
              width: .1,
            ),
            borderRadius: BorderRadius.circular(3),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text('Generate Promo Code', style: labelTextStyle),
              ),
              Padding(
                padding: const EdgeInsets.all(3.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: offWhiteColor,
                    border: Border.all(
                      color: blackColor,
                      width: .5,
                    ),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: IconButton(
                    color: redColor,
                    icon: const Icon(Icons.cancel),
                    // tooltip: 'Increase volume by 10',
                    onPressed: () => Navigator.pop(context),
                  ),
                ),
              ),
            ],
          ),
        ),
        content: Container(
          child: Text(
              'Are You sure you wanted to generate Promo Code ? \n This will overwrite the existing Promo Code'),
        ),
        actions: <Widget>[
          TextButton(
              child: Text('Cancel', style: TextStyle(color: redColor)),
              onPressed: () => Navigator.pop(context)),
          TextButton(
              child: Text('Confirm', style: TextStyle(color: textBlueColor)),
              onPressed: onTap),
        ],
      );
    },
  );
}
