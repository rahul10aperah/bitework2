import 'package:comp1/Model/category.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'loader.dart';

class SellerItem extends StatelessWidget {
  final Seller? seller;
  SellerItem({this.seller});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        width: 120,
        decoration: BoxDecoration(
          color: cartColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.8),
                blurRadius: 2,
                offset: Offset(-0.1, -0.1)),
          ],
        ),
        child: Column(
          children: <Widget>[
            Card(
              margin: EdgeInsets.zero,
              elevation: 3,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                // child: Image.asset(
                //   'assets/app_logo/logo-cropped.png',
                //   fit: BoxFit.cover,
                // ),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: 110,
                    imageUrl: seller!.url!,
                    placeholder: (context, url) => Loader(height: 100),
                    // placeholder: (context, url) => Image.asset(
                    //   'assets/img/loading.gif',
                    //   fit: BoxFit.cover,
                    // ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            heightSizedBox(15.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                seller!.kitchenName!,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                maxLines: 2,
                style: TextStyle(fontSize: 15),
              ),
            ),
            // heightSizedBox(10.0)
          ],
        ),
      ),
    );
  }
}
