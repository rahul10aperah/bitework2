import 'package:comp1/Model/user.dart';
import 'package:comp1/Repository/user_repository.dart' as repo;
import 'package:comp1/Screens/dashBoard.dart';
import 'package:comp1/Screens/otp_varification.dart';
import 'package:comp1/Screens/reset_password.dart';
import 'package:comp1/Screens/seller_dashboard.dart';
import 'package:comp1/Screens/sign_in.dart';
import 'package:comp1/Screens/sign_up.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SelVerify.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/send_request.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class UserController extends ControllerMVC {
  User user = User();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final googleSignIn = GoogleSignIn();
  bool isReferral = false;
  OverlayEntry? loader;
  TextEditingController fNameCon = TextEditingController();
  TextEditingController lNameCom = TextEditingController();
  TextEditingController emailCon = TextEditingController();

  UserController() {
    // loader =
    // loginFormKey = GlobalKey<FormState>();
    // this.scaffoldKey = GlobalKey<ScaffoldState>();
  }

  void signInDialog(context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: transparent,
      builder: (context) {
        return SignIn();
      },
    );
  }

  void registerDialog(context, {GoogleSignInAccount? account}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: transparent,
      builder: (context) {
        return SignUp(account: account);
      },
    );
  }

  void signInGoogle(context) async {
    logoutGoogle();
    GoogleSignInAccount? account = await googleSignIn.signIn();
    if (account != null) {
      isEmailExists(context, account);
    }
  }

  void logoutGoogle() async {
    await googleSignIn.disconnect();
  }

  // void isSignInGoogle() async {
  //   bool isSignIn = await googleSignIn.isSignedIn();
  //   print(isSignIn);
  // }

  void isEmailExists(context, GoogleSignInAccount? account) async {
    FocusScope.of(context).unfocus();
    loader = overlayLoader(context);
    Overlay.of(context)!.insert(loader!);

    repo.isEmailExists(account, context).then((value) {
      if (value != null) {
        logoutGoogle();
        navigationRemoveUntil(
            context,
            currentUser.value.roles == '3'
                // ? SellerVerfiyPage()
                ? SellerVerifyFromProfile()
                : DashBoard(currentTab: 1));

        // navigationRemoveUntil(context, DashBoard(currentTab: 1));
        flutterToast(value['message']);
      } else {
        registerDialog(context, account: account);
      }
    }).whenComplete(() {
      removeLoader(loader!);
    });
  }

  void login(context) async {
    FocusScope.of(context).unfocus();
    if (formKey.currentState!.validate()) {
      loader = overlayLoader(context);
      formKey.currentState!.save();
      Overlay.of(context)!.insert(loader!);

      repo.login(user, context).then((value) {
        if (value != null) {
          navigationRemoveUntil(
              context,
              currentUser.value.roles == '3'
                  // ? SellerVerfiyPage()
                  ? SellerVerifyFromProfile()
                  : DashBoard(currentTab: 1));
          flutterToast(value['message']);
        }
      }).whenComplete(() {
        removeLoader(loader!);
      });
    }
  }

  void registerFromGoogle(context, GoogleSignInAccount? account) async {
    FocusScope.of(context).unfocus();
    if (formKey.currentState!.validate()) {
      loader = overlayLoader(context);
      formKey.currentState!.save();

      Overlay.of(context)!.insert(loader!);
      repo.registerFromGoogle(account, user, context).then((value) {
        if (value != null) {
          logoutGoogle();
          navigationRemoveUntil(context, DashBoard(currentTab: 1));
          flutterToast('Registration successfully done');
        }
      }).whenComplete(() {
        removeLoader(loader!);
      });
    }
  }

  void register(context) async {
    FocusScope.of(context).unfocus();
    if (formKey.currentState!.validate()) {
      loader = overlayLoader(context);
      formKey.currentState!.save();
      Overlay.of(context)!.insert(loader!);

      repo.register(user, context).then((value) {
        if (value != null) {
          navigationRemoveUntil(context, DashBoard(currentTab: 1));
          flutterToast('Registration successfully done');
        }
      }).whenComplete(() {
        removeLoader(loader!);
      });
    }
  }

  void sendOTP(context, isEmail) async {
    FocusScope.of(context).unfocus();
    if (formKey.currentState!.validate()) {
      loader = overlayLoader(context);
      formKey.currentState!.save();
      Overlay.of(context)!.insert(loader!);
      // if (!isEmail) user.mobile = '91' + user.mobile.toString();
      repo.sendOTP(user, context, isEmail).then((value) {
        if (value != null) {
          navigationPush(
              context,
              GetOtp(
                isEmail: isEmail,
                emailMobile: isEmail ? user.email : user.mobile,
              ));
          flutterToast(
            isEmail ? value['message'] : value['info'],
          );
        }
      }).whenComplete(() {
        removeLoader(loader!);
      });
    }
  }

  void reSendOTPMail(context, emailMobile) async {
    FocusScope.of(context).unfocus();
    loader = overlayLoader(context);
    Overlay.of(context)!.insert(loader!);

    repo.reSendOTPMail(context, emailMobile).then((value) {
      if (value != null) flutterToast(value['message']);
    }).whenComplete(() {
      removeLoader(loader!);
    });
  }

  void reSendOTPMbl(context, emailMobile) async {
    FocusScope.of(context).unfocus();
    loader = overlayLoader(context);
    Overlay.of(context)!.insert(loader!);

    repo.reSendOTPMbl(context, emailMobile).then((value) {
      if (value != null) flutterToast(value['message']);
    }).whenComplete(() {
      removeLoader(loader!);
    });
  }

  void verifyOTP(context, isEmail) async {
    FocusScope.of(context).unfocus();
    loader = overlayLoader(context);
    Overlay.of(context)!.insert(loader!);

    repo.verifyOTP(user, context, isEmail).then((value) {
      if (value != null) {
        user.otp = value['otpId'];
        navigationPush(context, ResetPassword(isEmail: isEmail, user: user));
        flutterToast(value['message']);
      }
    }).whenComplete(() {
      removeLoader(loader!);
    });
  }

  void resetPassword(context, isEmail) async {
    FocusScope.of(context).unfocus();
    if (formKey.currentState!.validate()) {
      loader = overlayLoader(context);
      Overlay.of(context)!.insert(loader!);

      repo.resetPassword(user, context, isEmail).then((value) {
        if (value != null) {
          navigationRemoveUntil(context, DashBoard(currentTab: 1));
          flutterToast(value['message']);
        }
      }).whenComplete(() {
        removeLoader(loader!);
      });
    }
  }
}
