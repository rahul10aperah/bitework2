import 'package:comp1/Model/category.dart';
import 'package:comp1/Repository/home_repository.dart' as repo;

import 'package:flutter/cupertino.dart';
import 'package:google_place/google_place.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class HomeController extends ControllerMVC {
  String whichTab = '';
  List<Category> categories = <Category>[];
  List<Deal> deals = <Deal>[];
  List<Food> foods = <Food>[];
  List<Food> foodsAvailable = <Food>[];
  List<Seller> sellers = <Seller>[];
  List<String> sellerIds = <String>[];

  late AutocompletePrediction selectedAdd = AutocompletePrediction();
  TextEditingController addressCon = TextEditingController();

  HomeController() {
    // loader =
    // loginFormKey = GlobalKey<FormState>();
    // this.scaffoldKey = GlobalKey<ScaffoldState>();
  }

  void tabHandler(String? value) {
    if (value != whichTab)
      whichTab = value!;
    else
      whichTab = 'All';
    foodsAvailable.clear();
    if (whichTab == 'Delivery') {
      foods.forEach((element) {
        if (element.modeOfDelivery == whichTab) foodsAvailable.add(element);
      });
    } else if (whichTab == 'PickUp') {
      foods.forEach((element) {
        if (element.modeOfDelivery == whichTab) foodsAvailable.add(element);
      });
    } else
      foodsAvailable.addAll(foods);
    notifyListeners();
    // flutterToast('$whichTab foods available');
  }

  void getHomeData(context) async {
    repo.getHomeData(context).then((value) {
      if (value != null) {
        if (value['categories']['success'] == 1) {
          categories.addAll(List<Category>.from(value['categories']['data']
              .map((item) => Category.fromJSON(item))));
        }
        if (value['deals']['success'] == 1) {
          deals.addAll(List<Deal>.from(
              value['deals']['data'].map((item) => Deal.fromJSON(item))));
        }
        if (value['availableNow']['success'] == 1) {
          foods.addAll(List<Food>.from(value['availableNow']['data']
              .map((item) => Food.fromJSON(item))));
        }
        if (value['chefsNearYou']['success'] == 1) {
          sellers.addAll(List<Seller>.from(value['chefsNearYou']
                  ['sellerDetails']
              .map((item) => Seller.fromJSON(item))));
        }
        if (whichTab == 'Delivery') {
          foods.forEach((element) {
            if (element.modeOfDelivery == whichTab) foodsAvailable.add(element);
          });
        } else if (whichTab == 'PickUp') {
          foods.forEach((element) {
            if (element.modeOfDelivery == whichTab) foodsAvailable.add(element);
          });
        } else
          foodsAvailable.addAll(foods);
        notifyListeners();
        print(categories.length);
        print(deals.length);
        print(foods.length);
        print(sellers.length);
      }
    });
  }

  void getSellersFood(context) async {
    sellers.forEach((element) {
      sellerIds.add(element.sellerId!);
    });
    repo.getSellersFood(context, sellerIds).then((value) {
      if (value != null) {
        foods.addAll(
            List<Food>.from(value['data'].map((item) => Food.fromJSON(item))));
        notifyListeners();
        print(foods.length);
      }
    });
  }
}
