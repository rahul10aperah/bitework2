import 'dart:convert';

import 'package:comp1/Model/user.dart';
import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/send_request.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:google_sign_in/google_sign_in.dart';

Future isEmailExists(GoogleSignInAccount? user, context) async {
  Map data = {
    'email': user!.email,
  };
  final GoogleSignInAuthentication googleSignInAuthentication =
      await user.authentication;

  print(googleSignInAuthentication.accessToken);
  final response = await postMethod(
    url: URLConstants.isEmailExists,
    data: data,
    header: googleSignInAuthentication.accessToken,
  );

  if (response != null && response['success'] == 1) {
    setCurrentUser(response);
    currentUser.value = User.fromJSON(response);

    // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
    currentUser.notifyListeners();
    return response;
  } else {
    if (response['message'] != null)
      snackBar(context, response['message'] ?? '');
    return null;
  }
}

Future login(User user, context) async {
  Map data = {
    'email': user.email ?? '',
    'password': user.password ?? '',
  };

  final response = await postMethod(
    url: URLConstants.loginApiUrl,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    setCurrentUser(response);
    currentUser.value = User.fromJSON(response);

    // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
    currentUser.notifyListeners();
    return response;
  } else {
    if (response['data'] != null) snackBar(context, response['data'] ?? '');
    return null;
  }
}

Future register(User user, context) async {
  Map data = {
    "firstName": user.fName,
    "lastName": user.lName ?? '',
    'email': user.email ?? '',
    'password': user.password ?? '',
    "referrerCode": user.referrerCode ?? ''
  };

  final response = await postMethod(
    url: URLConstants.signUpApiUrl,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    setCurrentUser(response['data']);
    currentUser.value = User.fromJSON(response['data']);

    return response;
  } else {
    if (response['data'] != null) snackBar(context, response['data'] ?? '');
    return null;
  }
}

Future registerFromGoogle(
    GoogleSignInAccount? account, User user, context) async {
  Map data = {
    "googleId": account!.id,
    "photoUrl": account.photoUrl ?? '',
    "firstName": user.fName,
    "lastName": user.lName ?? '',
    "email": user.email ?? '',
    "password": user.password ?? '',
    "referrerCode": user.referrerCode ?? ''
  };

  final response = await putMethod(
    url: URLConstants.updateGoogleUser,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    setCurrentUser(response['data']);

    currentUser.value = User.fromJSON(response['data']);
    return response;
  } else {
    if (response['data'] != null) snackBar(context, response['data'] ?? '');
    return null;
  }
}

Future sendOTP(User user, context, isEmail) async {
  if (!isEmail) user.mobile = '91' + user.mobile.toString();
  Map data =
      isEmail ? {'email': user.email ?? ''} : {'phone': user.mobile ?? ''};

  final response = await postMethod(
    url: isEmail ? URLConstants.sendOtpEmail : URLConstants.sendOtpMbl,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    return response;
  } else {
    if (response['message'] != null)
      snackBar(context, response['message'] ?? '');
    return null;
  }
}

Future reSendOTPMail(context, String? emailMob) async {
  Map data = {'email': emailMob};

  final response = await putMethod(
    url: URLConstants.resentOtpEmail,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    return response;
  } else {
    if (response['message'] != null)
      snackBar(context, response['message'] ?? '');
    return null;
  }
}

Future reSendOTPMbl(context, String? emailMob) async {
  Map data = {'phone': emailMob};

  final response = await postMethod(
    url: URLConstants.resentOtpMbl,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    return response;
  } else {
    if (response['data'] != null) snackBar(context, response['message'] ?? '');
    return null;
  }
}

Future verifyOTP(User user, context, isEmail) async {
  Map data = isEmail
      ? {'email': user.email ?? '', "otpCode": user.otp ?? ''}
      : {'phone': user.mobile ?? '', "otpCode": user.otp ?? ''};

  final response = isEmail
      ? await putMethod(url: URLConstants.verifyOtpEmail, data: data)
      : await postMethod(url: URLConstants.verifyOtpMbl, data: data);

  if (response != null && response['success'] == 1) {
    return response;
  } else {
    if (isEmail) {
      if (response['data'] != null) snackBar(context, response['data'] ?? '');
    } else {
      if (response['message'] != null)
        snackBar(context, response['message'] ?? '');
    }
    return null;
  }
}

Future resetPassword(User user, context, isEmail) async {
  Map data = isEmail
      ? {
          "email": user.email ?? '',
          "otpId": user.otp ?? '',
          "newPassword": user.password ?? ''
        }
      : {
          "phone": user.mobile ?? '',
          "otpId": user.otp ?? '',
          "newPassword": user.password ?? ''
        };

  final response = await putMethod(
    url: isEmail ? URLConstants.resetPassEmail : URLConstants.resetPassFromMbl,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    setCurrentUser(response);

    currentUser.value = User.fromJSON(response);
    return response;
  } else {
    if (response['data'] != null) snackBar(context, response['message'] ?? '');
    return null;
  }
}

void setCurrentUser(response) async {
  if (response != null) {
    SharedHelper().setString('current_user', json.encode(response));
  }
}

Future<User> getCurrentUser() async {
  SharedHelper shared = SharedHelper();
  currentUser.value =
      User.fromJSON(json.decode(await shared.getString('current_user')));

  // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
  currentUser.notifyListeners();
  return currentUser.value;
}

Future<void> logout() async {
  currentUser.value = User();
  await SharedHelper().remove('current_user');
}
