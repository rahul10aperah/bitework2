import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/send_request.dart';

Future getHomeData(context) async {
  Map data = {
    "custlat": "28.527582",
    "custlong": "77.0688951",
    "range": '35000'
  };

  final response = await postMethod(
    url: URLConstants.userHome,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    return response;
  } else {
    if (response['message'] != null)
      snackBar(context, response['message'] ?? '');
    return null;
  }
}

Future getSellersFood(context, List<String> sellerIds) async {
  // Map<String, dynamic> data = Map<String, dynamic>();
  // data["sellerId"] = sellerIds;
  // data["dishStatus"] = 2;
  Map data = {};
  data = {"sellerId": sellerIds.toString(), "dishStatus": '2'};

  final response = await postMethod(
    url: URLConstants.getSellersDish,
    data: data,
  );

  if (response != null && response['success'] == 1) {
    return response;
  } else {
    if (response['message'] != null)
      snackBar(context, response['message'] ?? '');
    return null;
  }
}
