import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/send_request.dart';

Future getScreen(context) async {
  final response = await getMethod(url: URLConstants.currentScreen);

  if (response != null && response['success'] == 1) {
    return response;
  } else {
    // if (response['data'] != null) snackBar(context, response['message'] ?? '');
    return null;
  }
}
