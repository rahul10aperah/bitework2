import 'package:comp1/Seller/Fortend/SellerScreen/SelVerify.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerAddItem.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerAddmoreDet.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SelKitechen.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerReg.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/GeneratePromoCode.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerProgress.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerRevRepoer.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerReview.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SelTrackOrder.dart';
import 'package:flutter/material.dart';

class SellerDrawerList extends StatefulWidget {
  const SellerDrawerList({Key? key}) : super(key: key);

  @override
  _SellerDrawerListState createState() => _SellerDrawerListState();
}

class _SellerDrawerListState extends State<SellerDrawerList> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.65,
      child: Drawer(
          child: ListView(padding: EdgeInsets.zero, children: <Widget>[
        //  orc Custom Header file
        CusDrawerHeader(),

        //  ! END  Custom Header file

        // ! CUSTOM DRAWER LIST ITEM

        CusDrawerList(
            title: 'Seller Verfiy',
            icon: Icons.price_check,
            submitMethod: () => SellerVerifyFromProfile()),

        CusDrawerList(
            title: 'Progress',
            icon: Icons.price_check,
            submitMethod: () => SellerProgressScr()),
        CusDrawerList(
            title: 'Add more Details',
            icon: Icons.price_check,
            submitMethod: () => SellerAddMoreDetails()),
        CusDrawerList(
            title: 'Review Page',
            icon: Icons.price_check,
            submitMethod: () => SellerReviewScr()),
        CusDrawerList(
            title: 'Revenue Page',
            icon: Icons.price_check,
            submitMethod: () => SellerRevenuReport()),
        CusDrawerList(
            title: 'Track Order',
            icon: Icons.price_check,
            submitMethod: () => SellerTrackOrderScr()),

        CusDrawerList(
            title: 'PromoCode',
            icon: Icons.price_check,
            submitMethod: () => SelGeneratePromoCodes()),
      ])),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                          // !P:- 1. CUSTOM DRAWER HEADER                         */
/* -------------------------------------------------------------------------- */
class CusDrawerHeader extends StatelessWidget {
  CusDrawerHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        child: Stack(children: <Widget>[
          Container(
            padding: EdgeInsets.all(20),
            child: Center(
              child: Container(
                  height: 100,
                  width: 130,
                  child: Icon(
                    Icons.account_circle,
                  )),
            ),
          ),
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text("Rahul",
                  style: TextStyle(
                      color: Color(0xFF545454),
                      fontSize: 10.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }
}

/* -------------------------------------------------------------------------- */
/*               // ! P:-2. CUSTOM DRAWER LIST VIEW DATA                     */
/* -------------------------------------------------------------------------- */

class CusDrawerList extends StatelessWidget {
  dynamic submitMethod;

  String title;
  IconData icon;

  CusDrawerList({
    Key? key,
    required this.title,
    required this.icon,
    this.submitMethod,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return submitMethod();
          }));
        },
        child: ListTile(
          title: Text(title,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
          leading: Icon(icon),
        ));
  }
}
