// ! SelProdAbleItem HEADER
import 'package:comp1/Seller/Backend/BlocPattern/HomeB/selhomep_bloc.dart';
import 'package:comp1/Seller/Backend/SellerRespsitory/SelHomeResp.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/SelHomeWidget.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SelProdAbleItem extends StatefulWidget {
  final dynamic prodNumber;
  final dynamic onoffStatus2;
  final dynamic fulldata;
  final int? sellerId;
  SelProdAbleItem(
      {Key? key,
      this.fulldata,
      this.prodNumber,
      this.onoffStatus2,
      this.sellerId})
      : super(key: key);

  @override
  _SelProdAbleItemState createState() => _SelProdAbleItemState();
}

class _SelProdAbleItemState extends State<SelProdAbleItem> {
  dynamic clBtn = false;

  dynamic onoffStatus;
  SellerHomeRespo addRespo = SellerHomeRespo();

  Widget? getData() {
    // ! SellerItem VALIDAT
    if (widget.prodNumber['DishStatus'] == 1) {
      return SelCardHTxt(
          prodNumber: widget.prodNumber, t1: 'Pending', col: Colors.yellow);
    }
    // ! SellerItem VALIDAT
    if (widget.prodNumber['DishStatus'] == 2) {
      return SelCardHTxt(
          prodNumber: widget.prodNumber, t1: 'Approved', col: Colors.green);
    }

    // ! SellerItem VALIDAT
    if (widget.prodNumber['DishStatus'] == 8) {
      return SelCardHTxt(
          prodNumber: widget.prodNumber, t1: 'Coming Soon', col: Colors.blue);
    }

    // ! SellerItem VALIDAT
    if (widget.prodNumber['DishStatus'] == 7) {
      return SelCardHTxt(
          prodNumber: widget.prodNumber, t1: 'Live', col: redColor);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (onoffStatus == null) {
      setState(() {
        onoffStatus = widget.prodNumber['OnOff'];
      });
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(3, 1, 6, 1),
      child: Card(
          elevation: 2,
          color: offWhiteColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
          child: Container(
            child: Column(
              // shrinkWrap: true,
              children: [
                InkWell(
                  onTap: () {
                    if (clBtn == false) {
                      setState(() {
                        clBtn = true;
                      });
                    } else {
                      setState(() {
                        clBtn = false;
                      });
                    }
                  },
                  child: Container(
                    width: double.infinity,
                    height: 160,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: offWhiteColor,
                      image: DecorationImage(
                        // image: AssetImage('assets/images/COUPON.jpg'),
                        image: NetworkImage(widget.prodNumber['DishImageUrl1']),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Stack(
                      fit: StackFit.passthrough,
                      children: [
                        Positioned(
                          top: 10,
                          right: 20,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              // ! TGL PAGE AND LOGIC                          */
                              widget.prodNumber['DishStatus'] != 1
                                  ? Container(
                                      height: 25,
                                      width: 96,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(9),
                                        color: offWhiteColor,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          tglBtnTxt(
                                            'ON',

                                            txtColor: onoffStatus == 1
                                                ? offWhiteColor
                                                : null,

                                            backColor: onoffStatus == 1
                                                ? darkBlueColor
                                                : offWhiteColor,
                                            // : null,
                                            onTap: () {
                                              setState(() {
                                                var mat = BlocProvider.of<
                                                    SelhomepBloc>(context)
                                                  ..add(SelHomeUpdateItemEvent(
                                                      pageNum: widget
                                                              .fulldata[
                                                          "pageNumber"],
                                                      dishStatus: [1, 2, 7, 8],
                                                      sellerId: widget.sellerId,
                                                      dishId: widget
                                                          .prodNumber['DishId']
                                                          .toString(),
                                                      onoff: '1',
                                                      context: context));

                                                onoffStatus = 1;
                                              });
                                            },
                                          ),
                                          tglBtnTxt('OFF',
                                              txtColor: onoffStatus == 0
                                                  ? offWhiteColor
                                                  : null,
                                              backColor: onoffStatus == 0
                                                  ? darkBlueColor
                                                  : null, onTap: () {
                                            setState(() {
                                              BlocProvider.of<SelhomepBloc>(
                                                  context)
                                                ..add(SelHomeUpdateItemEvent(
                                                    pageNum: 1,
                                                    sellerId: widget.sellerId,
                                                    dishId: widget
                                                        .prodNumber['DishId']
                                                        .toString(),
                                                    onoff: '0',
                                                    context: context));

                                              onoffStatus = 0;
                                            });
                                          })
                                        ],
                                      ),
                                    )
                                  : Container(
                                      child: null,
                                    ),
                            ],
                          ),
                        ),
                        // ! Card Item
                        Container(
                            width: double.infinity,
                            height: 50,
                            child: clBtn == true
                                ? SelClickBtn(
                                    prodNumber: widget.prodNumber,
                                    sellerId: widget.sellerId)
                                : null)
                      ],
                    ),
                  ),
                ),

                // ! Deatils of Item
                Container(child: getData())
              ],
            ),
          )),
    );
  }

  Widget tglBtnTxt(title,
      {dynamic onTap, Color? backColor, Color? txtColor, dynamic onoffStatus}) {
    return Container(
      width: 48,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: backColor != null ? backColor : offWhiteColor,
        // borderRadius: BorderRadius.circular(6),
        borderRadius: BorderRadius.only(
          topRight: title == 'OFF' ? Radius.circular(6) : Radius.circular(0),
          bottomRight: title == 'OFF' ? Radius.circular(6) : Radius.circular(0),
          topLeft: title == 'ON' ? Radius.circular(6) : Radius.circular(0),
          bottomLeft: title == 'ON' ? Radius.circular(6) : Radius.circular(0),
        ),
      ),
      child: InkWell(
        onTap: onTap,
        child: Text(title,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 15,
                color: onoffStatus != null ? offWhiteColor : blackColor,
                fontFamily: montserratMedium)),
      ),
    );
  }
}

// ! Expired Item
class SelProdExpiredItem extends StatefulWidget {
  final dynamic prodNumber;

  SelProdExpiredItem({Key? key, this.prodNumber}) : super(key: key);

  @override
  State<SelProdExpiredItem> createState() => _SelProdExpiredItemState();
}

class _SelProdExpiredItemState extends State<SelProdExpiredItem> {
  dynamic clBtn = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(3, 1, 6, 1),
      child: Card(
          elevation: 2,
          color: offWhiteColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
          child: Container(
            child: Column(
              // shrinkWrap: true,
              children: [
                InkWell(
                  onTap: () {
                    if (clBtn == false) {
                      setState(() {
                        clBtn = true;
                      });
                    } else {
                      setState(() {
                        clBtn = false;
                      });
                    }
                  },
                  child: Container(
                      width: double.infinity,
                      height: 160,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: offWhiteColor,
                        image: DecorationImage(
                          // image: AssetImage('assets/images/COUPON.jpg'),
                          image:
                              NetworkImage(widget.prodNumber['DishImageUrl1']),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child:
                          // ! Card Item
                          Container(
                              width: double.infinity,
                              height: 50,
                              child: clBtn == true ? SelClickBtn() : null)),
                ),

                // ! Deatils of Item
                SelCardHTxt(
                    prodNumber: widget.prodNumber,
                    t1: 'Expired',
                    col: darkBlueColor),
              ],
            ),
          )),
    );
  }
}

// ! Sold Out Item
class SelProdSoldItem extends StatefulWidget {
  final dynamic prodNumber;
  SelProdSoldItem({Key? key, this.prodNumber}) : super(key: key);

  @override
  State<SelProdSoldItem> createState() => _SelProdSoldItemState();
}

class _SelProdSoldItemState extends State<SelProdSoldItem> {
  dynamic clBtn = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(3, 1, 6, 1),
      child: Card(
          elevation: 2,
          color: offWhiteColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
          child: Container(
            child: Column(
              // shrinkWrap: true,
              children: [
                InkWell(
                  onTap: () {
                    if (clBtn == false) {
                      setState(() {
                        clBtn = true;
                      });
                    } else {
                      setState(() {
                        clBtn = false;
                      });
                    }
                  },
                  child: Container(
                      width: double.infinity,
                      height: 160,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: offWhiteColor,
                        image: DecorationImage(
                          // image: AssetImage('assets/images/COUPON.jpg'),
                          image:
                              NetworkImage(widget.prodNumber['DishImageUrl1']),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child:
                          // ! Card Item
                          Container(
                              width: double.infinity,
                              height: 50,
                              child: clBtn == true ? SelClickBtn() : null)),
                ),

                // ! Deatils of Item
                SelCardHTxt(
                    prodNumber: widget.prodNumber,
                    // prodNumber: prodNumber['DishName'].toString(),
                    t1: 'Sold Out',
                    col: redColor),
              ],
            ),
          )),
    );
  }
}

// ! REJECTED ITEM Item
class SelProdRejectedItem extends StatefulWidget {
  final dynamic prodNumber;
  SelProdRejectedItem({Key? key, this.prodNumber}) : super(key: key);

  @override
  State<SelProdRejectedItem> createState() => _SelProdRejectedItemState();
}

class _SelProdRejectedItemState extends State<SelProdRejectedItem> {
  dynamic clBtn = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(3, 1, 6, 1),
      child: Card(
          elevation: 2,
          color: offWhiteColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
          child: Container(
            child: Column(
              // shrinkWrap: true,
              children: [
                InkWell(
                  onTap: () {
                    if (clBtn == false) {
                      setState(() {
                        clBtn = true;
                      });
                    } else {
                      setState(() {
                        clBtn = false;
                      });
                    }
                  },
                  child: Container(
                      width: double.infinity,
                      height: 160,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: offWhiteColor,
                        image: DecorationImage(
                          // image: AssetImage('assets/images/COUPON.jpg'),
                          image:
                              NetworkImage(widget.prodNumber['DishImageUrl1']),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child:
                          // ! Card Item
                          Container(
                              width: double.infinity,
                              height: 50,
                              child: clBtn == true ? SelClickBtn() : null)),
                ),

                // ! Deatils of Item
                SelCardHTxt(
                    prodNumber: widget.prodNumber,
                    t1: 'Rejected',
                    col: redColor),
              ],
            ),
          )),
    );
  }
}

//  ! UPDATE ui page in flutter
class KitchenItemList extends StatefulWidget {
  dynamic prodNumber;
  final int? sellerId;
  KitchenItemList({Key? key, this.prodNumber, this.sellerId}) : super(key: key);

  @override
  State<KitchenItemList> createState() => _KitchenItemListState();
}

class _KitchenItemListState extends State<KitchenItemList> {
  dynamic clBtn = false;
  dynamic onoffStatus;

  Widget? getData() {
    // ! SellerItem VALIDAT
    if (widget.prodNumber['DishStatus'] == 1) {
      return SelCardHTxt(
          prodNumber: widget.prodNumber, t1: 'Pending', col: Colors.yellow);
    }
    // ! SellerItem VALIDAT
    if (widget.prodNumber['DishStatus'] == 2) {
      return SelCardHTxt(
          prodNumber: widget.prodNumber, t1: 'Approved', col: Colors.green);
    }

    // ! SellerItem VALIDAT
    if (widget.prodNumber['DishStatus'] == 7) {
      return SelCardHTxt(
          prodNumber: widget.prodNumber, t1: 'Live', col: redColor);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (onoffStatus == null) {
      setState(() {
        onoffStatus = widget.prodNumber['OnOff'];
      });
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(3, 1, 6, 1),
      child: Card(
        elevation: 2,
        color: offWhiteColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        child: Container(
            width: double.infinity,
            height: 160,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      // width: .0,
                      // height: 160.0,
                      child: Image.network(widget.prodNumber['DishImageUrl1'],
                          width: 100, height: 150, fit: BoxFit.cover)),
                  // ! Deatils of Item
                  Container(child: getData()),
                  widget.prodNumber['DishStatus'] == 2 ||
                          widget.prodNumber['DishStatus'] == 7
                      ? Container(
                          height: 25,
                          width: 96,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9),
                            color: offWhiteColor,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              tglBtnTxt(
                                'ON',

                                txtColor:
                                    onoffStatus == 1 ? offWhiteColor : null,

                                backColor: onoffStatus == 1
                                    ? darkBlueColor
                                    : offWhiteColor,
                                // : null,
                                onTap: () {
                                  setState(() {
                                    var mat =
                                        BlocProvider.of<SelhomepBloc>(context)
                                          ..add(SelHomeUpdateItemEvent(
                                              sellerId: widget.sellerId,
                                              dishId: widget
                                                  .prodNumber['DishId']
                                                  .toString(),
                                              onoff: '1',
                                              context: context));
                                    print('this is ====== ${mat.state}');
                                    onoffStatus = 1;
                                    // print(
                                    //     ' this is disname OnValue ${widget.prodNumber['OnOff']}');
                                  });
                                },
                              ),
                              tglBtnTxt('OFF',
                                  txtColor:
                                      onoffStatus == 0 ? offWhiteColor : null,
                                  backColor: onoffStatus == 0
                                      ? darkBlueColor
                                      : null, onTap: () {
                                setState(() {
                                  BlocProvider.of<SelhomepBloc>(context)
                                    ..add(SelHomeUpdateItemEvent(
                                        sellerId: widget.sellerId,
                                        dishId: widget.prodNumber['DishId']
                                            .toString(),
                                        onoff: '0',
                                        context: context));

                                  onoffStatus = 0;
                                });
                              })
                            ],
                          ),
                        )
                      : Container(
                          child: null,
                        ),
                ])),
      ),
    );
  }

  Widget tglBtnTxt(title,
      {dynamic onTap, Color? backColor, Color? txtColor, dynamic onoffStatus}) {
    return Container(
      width: 48,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: backColor != null ? backColor : offWhiteColor,
        // borderRadius: BorderRadius.circular(6),
        borderRadius: BorderRadius.only(
          topRight: title == 'OFF' ? Radius.circular(6) : Radius.circular(0),
          bottomRight: title == 'OFF' ? Radius.circular(6) : Radius.circular(0),
          topLeft: title == 'ON' ? Radius.circular(6) : Radius.circular(0),
          bottomLeft: title == 'ON' ? Radius.circular(6) : Radius.circular(0),
        ),
      ),
      child: InkWell(
        onTap: onTap,
        child: Text(title,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 15,
                color: onoffStatus != null ? offWhiteColor : blackColor,
                fontFamily: montserratMedium)),
      ),
    );
  }
}
