import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Screens/seller_dashboard.dart';
import 'package:comp1/Seller/Backend/BlocPattern/TrackOrder/seltrackorder_bloc.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SelTrackOrderWid.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/SelHomeWidget.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'SellerRevRepoer.dart';

class SellerTrackOrderScr extends StatefulWidget {
  final int? sellerId;
  static const routeName = '/SellerTrackOrderScr';
  SellerTrackOrderScr({Key? key, this.sellerId}) : super(key: key);

  @override
  _SellerTrackOrderScrState createState() => _SellerTrackOrderScrState();
}

class _SellerTrackOrderScrState extends State<SellerTrackOrderScr> {
  // ! box item
  dynamic selectedIndex = 'Pending';
  void onItemTapped(String? index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Track Order',
        boolLeading: false,
      ),
      body: Container(
        child: ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: [
            // ! Menu Header
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: double.infinity,
                child: Row(
                  children: [
                    SellerMenuTxt(
                      'Pending',
                      txtColor:
                          selectedIndex == 'Pending' ? selectedIndex : null,
                      selectedIndex: selectedIndex,
                      onItemTapped: onItemTapped,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(6),
                        bottomLeft: Radius.circular(6),
                      ),
                    ),
                    SellerMenuTxt('Accepted',
                        txtColor:
                            selectedIndex == 'Accepted' ? selectedIndex : null,
                        selectedIndex: selectedIndex,
                        onItemTapped: onItemTapped),
                    SellerMenuTxt('Completed',
                        txtColor:
                            selectedIndex == 'Completed' ? selectedIndex : null,
                        selectedIndex: selectedIndex,
                        onItemTapped: onItemTapped),
                    SellerMenuTxt('Rejected',
                        txtColor:
                            selectedIndex == 'Rejected' ? selectedIndex : null,
                        selectedIndex: selectedIndex,
                        onItemTapped: onItemTapped,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(6),
                          bottomRight: Radius.circular(6),
                        )),
                  ],
                ),
              ),
            ),
            // ! Card Item
            Container(
                child: SelTrackOrderOne(
              sellerId: widget.sellerId,
              selectedIndex: selectedIndex,
            ))
          ],
        ),
      ),
    );
  }
}

// ! Selected index number BTN
class SelTrackOrderOne extends StatefulWidget {
  dynamic selectedIndex;
  int? sellerId;
  SelTrackOrderOne({Key? key, this.selectedIndex, this.sellerId})
      : super(key: key);

  @override
  _SelTrackOrderOneState createState() => _SelTrackOrderOneState();
}

class _SelTrackOrderOneState extends State<SelTrackOrderOne> {
  Widget? getData() {
    if (widget.selectedIndex == 'Pending') {
      return SelTrackPendingData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
    if (widget.selectedIndex == 'Accepted') {
      return SelTrackAcceptedData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
    if (widget.selectedIndex == 'Completed') {
      return SelTrackCompletedData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
    if (widget.selectedIndex == 'Rejected') {
      return SelTrackRejectedData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // print(' selectIndex ${widget.selectedIndex}');
    return Container(child: getData());
  }
}

// ! SelTrack Order Pending
class SelTrackPendingData extends StatefulWidget {
  final int? sellerId;
  final dynamic selectedIndex;

  SelTrackPendingData({Key? key, this.sellerId, this.selectedIndex})
      : super(key: key);

  @override
  _SelTrackPendingDataState createState() => _SelTrackPendingDataState();
}

class _SelTrackPendingDataState extends State<SelTrackPendingData> {
  SeltrackorderBloc prodBloc = SeltrackorderBloc();
  @override
  void initState() {
    prodBloc = BlocProvider.of<SeltrackorderBloc>(context);
    prodBloc.add(FetchTrackOrderEvent(
      sellerId: widget.sellerId,
      orderStatus: 4,
      context: context,
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SeltrackorderBloc, SeltrackorderState>(
        listener: (context, state) {},
        child: BlocBuilder<SeltrackorderBloc, SeltrackorderState>(
            builder: (context, state) {
          if (state is SelTrackLoadedState) {
            if (state.selDishData.length > 0) {
              return SelTrackListItem(
                  btnName: true,
                  prodNumber: state.selDishData,
                  sellerId: widget.sellerId);
            } else {
              return Center(child: Text('No Data', style: labelTextStyle));
            }
          }
          return Center(child: CircularProgressIndicator());
        }));
  }
}

// ! SEL Accepted PAGE BTN
class SelTrackAcceptedData extends StatefulWidget {
  final int? sellerId;
  final dynamic selectedIndex;

  SelTrackAcceptedData({Key? key, this.sellerId, this.selectedIndex})
      : super(key: key);

  @override
  _SelTrackAcceptedDataState createState() => _SelTrackAcceptedDataState();
}

class _SelTrackAcceptedDataState extends State<SelTrackAcceptedData> {
  SeltrackorderBloc prodBloc = SeltrackorderBloc();
  @override
  void initState() {
    prodBloc = BlocProvider.of<SeltrackorderBloc>(context);
    prodBloc.add(FetchTrackOrderEvent(
      sellerId: widget.sellerId,
      orderStatus: 7,
      context: context,
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SeltrackorderBloc, SeltrackorderState>(
        listener: (context, state) {
      print('State $state');
      if (state is SelTrackSuccessState) {
        // navigationPush(
        //     context, SellerDashboard(sellerId: widget.sellerId, currentTab: 1));
        BlocProvider.of<SeltrackorderBloc>(context)
          ..add(FetchTrackOrderEvent(
            sellerId: widget.sellerId,
            orderStatus: 7,
            context: context,
          ));
      }
    }, child: BlocBuilder<SeltrackorderBloc, SeltrackorderState>(
            builder: (context, state) {
      print('State builder $state');
      if (state is SelTrackLoadedState) {
        if (state.selDishData.length > 0) {
          return SelTrackListItem(
              prodNumber: state.selDishData, sellerId: widget.sellerId);
        } else {
          return Center(child: Text('No Data', style: labelTextStyle));
        }
      }
      return Center(child: CircularProgressIndicator());
    }));
  }
}

// ! COMPLETED PAGE
class SelTrackCompletedData extends StatefulWidget {
  final int? sellerId;
  final dynamic selectedIndex;

  SelTrackCompletedData({Key? key, this.sellerId, this.selectedIndex})
      : super(key: key);

  @override
  _SelTrackCompletedDataState createState() => _SelTrackCompletedDataState();
}

class _SelTrackCompletedDataState extends State<SelTrackCompletedData> {
  SeltrackorderBloc prodBloc = SeltrackorderBloc();
  @override
  void initState() {
    prodBloc = BlocProvider.of<SeltrackorderBloc>(context);
    prodBloc.add(FetchTrackOrderEvent(
      sellerId: widget.sellerId,
      orderStatus: 18,
      context: context,
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SeltrackorderBloc, SeltrackorderState>(
        listener: (context, state) {
      print('State $state');
    }, child: BlocBuilder<SeltrackorderBloc, SeltrackorderState>(
            builder: (context, state) {
      print('State builder $state');
      if (state is SelTrackLoadedState) {
        if (state.selDishData.length > 0) {
          return SelTrackListItem(
              prodNumber: state.selDishData, sellerId: widget.sellerId);
        } else {
          return Center(child: Text('No Data', style: labelTextStyle));
        }
      }
      return Center(child: CircularProgressIndicator());
    }));
  }
}

// ! SEL Accepted PAGE BTN
class SelTrackRejectedData extends StatefulWidget {
  final int? sellerId;
  final dynamic selectedIndex;

  SelTrackRejectedData({Key? key, this.sellerId, this.selectedIndex})
      : super(key: key);

  @override
  _SelTrackRejectedDataState createState() => _SelTrackRejectedDataState();
}

class _SelTrackRejectedDataState extends State<SelTrackRejectedData> {
  SeltrackorderBloc prodBloc = SeltrackorderBloc();
  @override
  void initState() {
    prodBloc = BlocProvider.of<SeltrackorderBloc>(context);
    prodBloc.add(FetchTrackOrderEvent(
      sellerId: widget.sellerId,
      orderStatus: 8,
      context: context,
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SeltrackorderBloc, SeltrackorderState>(
        listener: (context, state) {
      if (state is SelTrackSuccessState) {
        // navigationPush(
        //     context, SellerDashboard(sellerId: widget.sellerId, currentTab: 1));
        BlocProvider.of<SeltrackorderBloc>(context)
          ..add(FetchTrackOrderEvent(
            sellerId: widget.sellerId,
            orderStatus: 8,
            context: context,
          ));
      }
    }, child: BlocBuilder<SeltrackorderBloc, SeltrackorderState>(
            builder: (context, state) {
      if (state is SelTrackLoadedState) {
        if (state.selDishData.length > 0) {
          return SelTrackListItem(
              prodNumber: state.selDishData, sellerId: widget.sellerId);
        } else {
          return Center(child: Text('No Data', style: labelTextStyle));
        }
      }
      return Center(child: CircularProgressIndicator());
    }));
  }
}
