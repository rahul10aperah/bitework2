import 'package:comp1/Seller/Backend/BlocPattern/TrackOrder/seltrackorder_bloc.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'SellerRevRepoer.dart';

class SelTrackListItem extends StatelessWidget {
  dynamic prodNumber;
  int? sellerId;
  bool? btnName;
  SelTrackListItem({Key? key, this.btnName, this.sellerId, this.prodNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      color: offWhiteColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
        Radius.circular(6),
      )),
      child: Column(
        children: [
          Container(
            child: prodNumber.length > 0
                ? SellerTableH5(
                    t1: 'SL.',
                    t2: 'Date & Time',
                    t3: 'Order #',
                    t4: 'Order By',
                    t5: 'Collection Type',
                    fSize: 13,
                  )
                : null,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 3.0, right: 3),
            child: Container(
                child:
                    // prodNumber.length > 0  ?
                    ListView.builder(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemCount: prodNumber.length,
                        itemBuilder: (context, index) {
                          return TrackOrderTableData(
                              prodNumber: prodNumber[index],
                              indexNumber: index,
                              btnName: btnName,
                              sellerId: sellerId);
                        })
                // : Center(child: Text('No Data'))
                ),
          )
        ],
      ),
    );
  }
}

// ! table Item
class TrackOrderTableData extends StatefulWidget {
  dynamic prodNumber;
  dynamic indexNumber;
  int? sellerId;
  bool? btnName;
  TrackOrderTableData(
      {Key? key,
      this.sellerId,
      this.btnName,
      this.prodNumber,
      this.indexNumber})
      : super(key: key);

  @override
  State<TrackOrderTableData> createState() => _TrackOrderTableDataState();
}

class _TrackOrderTableDataState extends State<TrackOrderTableData> {
  dynamic clBtn = false;
  dynamic res1;
  @override
  Widget build(BuildContext context) {
    // if (widget.prodNumber['dateCreated'] != null) {
    //   var d1 = DateTime.parse(widget.prodNumber['dateCreated']);

    //   // ! p2

    //   dynamic timeSlot2;

    //   switch (d1.month) {
    //     case 1:
    //       timeSlot2 = 'Jan';
    //       break;
    //     case 2:
    //       timeSlot2 = 'Feb';
    //       break;
    //     case 3:
    //       timeSlot2 = 'Mar';
    //       break;
    //     case 4:
    //       timeSlot2 = 'Apr';
    //       break;
    //     case 5:
    //       timeSlot2 = 'May';
    //       break;
    //     case 6:
    //       timeSlot2 = 'Jun';
    //       break;
    //     case 7:
    //       timeSlot2 = 'Jul';
    //       break;
    //     case 8:
    //       timeSlot2 = 'Aug';
    //       break;
    //     case 9:
    //       timeSlot2 = 'Sep';
    //       break;
    //     case 10:
    //       timeSlot2 = 'Oct';
    //       break;
    //     case 11:
    //       timeSlot2 = 'Nov';
    //       break;
    //     case 12:
    //       timeSlot2 = 'Dec';
    //       break;
    //   }

    //   setState(() {
    //     res1 = "${d1.day} ,$timeSlot2 , ${d1.year} ";
    //   });
    // }
    return InkWell(
      onTap: () {
        if (clBtn == false) {
          setState(() {
            clBtn = true;
          });
        } else {
          setState(() {
            clBtn = false;
          });
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 9.0, bottom: 9.0),
        child: Card(
          elevation: 2,
          color: offWhiteColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
            Radius.circular(6),
          )),
          child: Stack(
            children: [
              Column(
                children: [
                  Table(
                      border: TableBorder.all(
                          width: .8,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0))),
                      // border: TableBorder.all(width: .5, color: blackColor),
                      children: [
                        TableRow(children: [
                          tableH(
                            '${widget.indexNumber + 1}',
                          ),
                          tableH(DateFormat('dd,MMM,yyyy').format(
                              DateTime.parse(widget.prodNumber['dateCreated']
                                  .toString()))),
                          tableH(
                            widget.prodNumber['dishOrderId'].toString(),
                          ),
                          tableH(
                            widget.prodNumber['orderBy'].toString(),
                          ),
                          tableH(
                            widget.prodNumber['collectionType'].toString(),
                          ),
                        ]),
                      ]),
                  Table(
                    children: [
                      TableRow(children: [
                        tableH(
                          'Item',
                        ),
                        tableH(
                          'Qty',
                        ),
                        tableH(
                          'TotalAmmount',
                        ),
                      ]),
                    ],
                  ),

                  // ! Table List
                  Container(
                      child: tableListRow(
                          tableListData: widget.prodNumber['bags'])),
                ],
              ),
              // ! Butn Item
              Container(
                  alignment: Alignment.center,
                  child: clBtn == true
                      ? SelTrackClickBtn(
                          btnName: widget.btnName,
                          prodNumber: widget.prodNumber,
                          sellerId: widget.sellerId)
                      : null),
            ],
          ),
        ),
      ),
    );
  }

  Widget? tableListRow({dynamic tableListData}) {
    return ListView.builder(
        itemCount: tableListData.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (context, index) {
          return Table(children: [
            TableRow(children: [
              tableContent(
                  '${index + 1}. ${tableListData[index]['DishName'].toString()}'),
              tableContent(tableListData[index]['Qty'].toString()),
              tableContent(tableListData[index]['DishTotal'].toString()),
            ]),
          ]);
        });
  }

  Widget tableH(
    String t,
  ) {
    return Container(
      width: 30,
      height: 30,
      child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(t, textAlign: TextAlign.center, style: labelTextStyle)),
    );
  }

  Widget tableHeading(String t, {FontWeight? fWeig}) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text(t,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 13,
              fontFamily: montserratSemiBold,
              color: textBlueColor)),
    );
  }

  Widget tableContent(String t, {FontWeight? fWeig}) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text(t,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 12, fontFamily: montserratMedium, color: blackColor)),
    );
  }
}

class SelTrackClickBtn extends StatelessWidget {
  dynamic sellerId;
  final dynamic prodNumber;
  bool? btnName;
  SelTrackClickBtn({Key? key, this.btnName, this.prodNumber, this.sellerId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(top: 50.0, bottom: 50, left: 10, right: 10),
      child: btnName == false
          ? Container(
              width: double.infinity,
              height: 50,
              color: redColor,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  eadTxt('Rejected', onTap: () {
                    BlocProvider.of<SeltrackorderBloc>(context)
                      ..add(RejectedStatsEvent(
                        orderId: prodNumber['dishOrderId'],
                        orderStatus: 8,
                        context: context,
                      ));
                  }),
                  eadTxt('View', onTap: () {
                    print('sellerId  View $sellerId');
                  }),
                  eadTxt('Accepted', onTap: () {
                    print('sellerId  Accepted ${prodNumber['dishOrderId']}');
                    BlocProvider.of<SeltrackorderBloc>(context)
                      ..add(RejectedStatsEvent(
                        orderId: prodNumber['dishOrderId'],
                        orderStatus: 7,
                        context: context,
                      ));
                  }),
                ],
              ))
          : Container(
              width: 120,
              height: 50,
              color: redColor,
              child: Row(
                children: [
                  eadTxt('View', onTap: () {
                    // print('sellerId  View $sellerId');
                  }),
                ],
              ),
            ),
    );
  }

  // ! Text for EAD BTN
  Widget eadTxt(String t, {dynamic onTap}) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(color: offWhiteColor),
            ),
            child: Text(t,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 15,
                    color: offWhiteColor,
                    fontFamily: montserratMedium))),
      ),
    );
  }
}
