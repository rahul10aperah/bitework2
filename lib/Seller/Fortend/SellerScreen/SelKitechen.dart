import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Screens/seller_dashboard.dart';
import 'package:comp1/Seller/Backend/BlocPattern/HomeB/selhomep_bloc.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/SelHomeWidget.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'SelKitchenp2.dart';

class SellerHomeScr extends StatefulWidget {
  final int? sellerId;
  SellerHomeScr({Key? key, this.sellerId}) : super(key: key);

  @override
  State<SellerHomeScr> createState() => _SellerHomeScrState();
}

class _SellerHomeScrState extends State<SellerHomeScr> {
  dynamic selectedIndex = 'Available';
  // dynamic onoffStatus = false;

//  ! Select Menu Index Method
  void onItemTapped(String? index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: redColor,
        title: Text('Kitchen', style: appBarTS),
        actions: [SelAppbarBtn(sellerId: widget.sellerId)],
        automaticallyImplyLeading: false,
      ),
      body:
          // ListView(
          SingleChildScrollView(
        child: Column(
          children: [
            // SelHomeMenuText(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: double.infinity,
                child: Row(
                  children: [
                    SellerMenuTxt('Available',
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(6),
                          bottomLeft: Radius.circular(6),
                        ),
                        txtColor:
                            selectedIndex == 'Available' ? selectedIndex : null,
                        selectedIndex: selectedIndex,
                        onItemTapped: onItemTapped),
                    SellerMenuTxt('Expired',
                        txtColor:
                            selectedIndex == 'Expired' ? selectedIndex : null,
                        selectedIndex: selectedIndex,
                        onItemTapped: onItemTapped),
                    SellerMenuTxt('Sold Out',
                        txtColor:
                            selectedIndex == 'Sold Out' ? selectedIndex : null,
                        selectedIndex: selectedIndex,
                        onItemTapped: onItemTapped),
                    SellerMenuTxt(
                      'Rejected',
                      txtColor:
                          selectedIndex == 'Rejected' ? selectedIndex : null,
                      selectedIndex: selectedIndex,
                      onItemTapped: onItemTapped,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(6),
                        bottomRight: Radius.circular(6),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            SellerHomeOne(
                selectedIndex: selectedIndex, sellerId: widget.sellerId)
          ],
        ),
      ),
    );
  }
}

class SellerHomeOne extends StatefulWidget {
  dynamic selectedIndex;
  int? sellerId;
  SellerHomeOne({Key? key, this.selectedIndex, this.sellerId})
      : super(key: key);

  @override
  _SellerHomeOneState createState() => _SellerHomeOneState();
}

class _SellerHomeOneState extends State<SellerHomeOne> {
  Widget? getData() {
    if (widget.selectedIndex == 'Available') {
      return SelHomeAblePageData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
    if (widget.selectedIndex == 'Expired') {
      return SelHomeExpiredPageData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
    if (widget.selectedIndex == 'Sold Out') {
      return SelHomeSoldPageData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
    if (widget.selectedIndex == 'Rejected') {
      return SelHomeRejectedPageData(
        selectedIndex: widget.selectedIndex,
        sellerId: widget.sellerId,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // print(' selectIndex ${widget.selectedIndex}');
    return Container(child: getData());
  }
}

/* -------------------------------------------------------------------------- */
/*                          // !Seller Home Page                         */
/* -------------------------------------------------------------------------- */
class SelHomeAblePageData extends StatefulWidget {
  final dynamic selectedIndex;
  final int? sellerId;
  SelHomeAblePageData({Key? key, this.selectedIndex, this.sellerId})
      : super(key: key);

  @override
  State<SelHomeAblePageData> createState() => _SelHomeAblePageDataState();
}

class _SelHomeAblePageDataState extends State<SelHomeAblePageData> {
  SelhomepBloc prodBloc = SelhomepBloc();

  @override
  void initState() {
    prodBloc = BlocProvider.of<SelhomepBloc>(context);
    prodBloc.add(FetchSelHomeEvent(
        sellerId: widget.sellerId, pageNum: 1, dishStatus: [1, 2, 7, 8]));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelhomepBloc, SelhomepState>(
        listener: (context, state) {
      // print('-------------------state $state');
      if (state is SelHomeErrorState) {
        snackBar(context, state.error.toString());
      }

      if (state is SelHomeDelItemState) {
        // snackBar(context, state.delData.toString());
        navigationPush(context, SellerDashboard(sellerId: widget.sellerId));
      }
    }, child:
            BlocBuilder<SelhomepBloc, SelhomepState>(builder: (context, state) {
      // ! Able data in List
      if (state is SelHomeNotSuccessState) {
        return Center(
            child:
                Text('No data for the queried filter', style: labelTextStyle));
      }

      if (state is SelHomeLoadedState) {
        return Container(
            child: Column(
          children: [
            ListView.builder(
                itemCount: state.selDishData['data'].length,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  if ((state.selDishData['data'][index]['DishStatus'] == 1 ||
                          state.selDishData['data'][index]['DishStatus'] ==
                              7) ||
                      (state.selDishData['data'][index]['DishStatus'] == 2 ||
                          state.selDishData['data'][index]['DishStatus'] ==
                              8)) {
                    return SelProdAbleItem(
                        fulldata: state.selDishData,
                        prodNumber: state.selDishData['data'][index],
                        sellerId: widget.sellerId);
                  }
                  return Center(child: Text('No Dish ', style: labelTextStyle));
                }),
            state.selDishData["totalPages"] != null
                ? Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    child: BigButton(
                        color: darkBlueColor,
                        title: "Load More Food",
                        onTap: () {
                          // if (state.selDishData["pageNumber"] <
                          //     state.selDishData["totalPages"]) {
                          BlocProvider.of<SelhomepBloc>(context)
                            ..add(SelDataPageItemEvent(
                                sellerId: widget.sellerId,
                                pageNum: state.selDishData["pageNumber"] <
                                        state.selDishData["totalPages"]
                                    ? state.selDishData["pageNumber"] + 1
                                    : state.selDishData["pageNumber"],
                                dishStatus: [1, 2, 7, 8]));
                        }
                        // },
                        ),
                  )
                : Container(
                    child: null,
                  ),
          ],
        ));
      }

      return Center(child: CircularProgressIndicator());
    }));
  }
}

// ! Expired Data
class SelHomeExpiredPageData extends StatefulWidget {
  dynamic selectedIndex;
  final int? sellerId;
  SelHomeExpiredPageData({Key? key, this.selectedIndex, this.sellerId})
      : super(key: key);

  @override
  _SelHomeExpiredPageDataState createState() => _SelHomeExpiredPageDataState();
}

class _SelHomeExpiredPageDataState extends State<SelHomeExpiredPageData> {
  SelhomepBloc prodBloc = SelhomepBloc();

  @override
  void initState() {
    prodBloc = BlocProvider.of<SelhomepBloc>(context);
    prodBloc.add(FetchSelHomeEvent(
        sellerId: widget.sellerId, pageNum: 1, dishStatus: [5]));
    // prodBloc.add(SelHomeExpiredEvent(
    //     sellerId: widget.sellerId, pageNum: 1, dishStatus: [5]));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SelhomepBloc, SelhomepState>(builder: (context, state) {
      // !Expired Data
      if (state is SelHomeNotSuccessState) {
        return Center(
            child:
                Text('No data for the queried filter', style: labelTextStyle));
      }

      // if (state is SelHomeExpiredState) {
      if (state is SelHomeLoadedState) {
        return Container(
            child: Column(
          children: [
            ListView.builder(
                itemCount: state.selDishData['data'].length,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  if (state.selDishData['data'][index]['DishStatus'] == 5) {
                    return SelProdExpiredItem(
                      prodNumber: state.selDishData['data'][index],
                    );
                  }
                  return Align(child: Text('No Dish ', style: labelTextStyle));
                }),

            // ! LOAD MORE BUTTON
            state.selDishData["totalPages"] != null
                ? Container(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: BigButton(
                          color: darkBlueColor,
                          title: "Load More Food",
                          onTap: () {
                            BlocProvider.of<SelhomepBloc>(context)
                              ..add(SelDataPageItemEvent(
                                  sellerId: widget.sellerId,
                                  pageNum: state.selDishData["pageNumber"] <
                                          state.selDishData["totalPages"]
                                      ? state.selDishData["pageNumber"] + 1
                                      : state.selDishData["pageNumber"],
                                  dishStatus: [5]));
                          }),
                    ),
                  )
                : Container(
                    child: null,
                  ),
          ],
        ));
      }

      return Center(child: CircularProgressIndicator());
    });
  }
}

// ! Seller SOLOD OUT DATA
class SelHomeSoldPageData extends StatefulWidget {
  dynamic selectedIndex;
  final int? sellerId;
  SelHomeSoldPageData({Key? key, this.selectedIndex, this.sellerId})
      : super(key: key);

  @override
  _SelHomeSoldPageDataState createState() => _SelHomeSoldPageDataState();
}

class _SelHomeSoldPageDataState extends State<SelHomeSoldPageData> {
  SelhomepBloc prodBloc = SelhomepBloc();

  @override
  void initState() {
    prodBloc = BlocProvider.of<SelhomepBloc>(context);
    prodBloc.add(FetchSelHomeEvent(
        sellerId: widget.sellerId, pageNum: 1, dishStatus: [6]));
    // print(widget.selectedIndex);
    // prodBloc.add(SelHomeSoldEvent(sellerId: widget.sellerId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SelhomepBloc, SelhomepState>(builder: (context, state) {
      // !Expired Data
      if (state is SelHomeNotSuccessState) {
        return Center(
            child:
                Text('No data for the queried filter', style: labelTextStyle));
      }

      // if (state is SelHomeSoldState) {
      if (state is SelHomeLoadedState) {
        return Container(
            child: Column(
          children: [
            ListView.builder(
                itemCount: state.selDishData['data'].length,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  if (state.selDishData['data'][index]['DishStatus'] == 6) {
                    return SelProdSoldItem(
                      prodNumber: state.selDishData['data'][index],
                    );
                  }
                  return Align(child: Text('No Dish ', style: labelTextStyle));
                }),

            // ! LOAD MORE BUTTON
            state.selDishData["totalPages"] != null
                ? Container(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: BigButton(
                          color: darkBlueColor,
                          title: "Load More Food",
                          onTap: () {
                            BlocProvider.of<SelhomepBloc>(context)
                              ..add(SelDataPageItemEvent(
                                  sellerId: widget.sellerId,
                                  pageNum: state.selDishData["pageNumber"] <
                                          state.selDishData["totalPages"]
                                      ? state.selDishData["pageNumber"] + 1
                                      : state.selDishData["pageNumber"],
                                  dishStatus: [6]));
                          }),
                    ),
                  )
                : Container(
                    child: null,
                  ),
          ],
        ));
      }

      return Center(child: CircularProgressIndicator());
    });
  }
}

// ! Rejected Data
class SelHomeRejectedPageData extends StatefulWidget {
  dynamic selectedIndex;
  final int? sellerId;
  SelHomeRejectedPageData({Key? key, this.selectedIndex, this.sellerId})
      : super(key: key);

  @override
  _SelHomeRejectedPageDataState createState() =>
      _SelHomeRejectedPageDataState();
}

class _SelHomeRejectedPageDataState extends State<SelHomeRejectedPageData> {
  SelhomepBloc prodBloc = SelhomepBloc();

  @override
  void initState() {
    prodBloc = BlocProvider.of<SelhomepBloc>(context);
    prodBloc.add(FetchSelHomeEvent(
        sellerId: widget.sellerId, pageNum: 1, dishStatus: [3]));
    // print(widget.selectedIndex);
    // prodBloc.add(SelHomeRejectedEvent(sellerId: widget.sellerId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SelhomepBloc, SelhomepState>(builder: (context, state) {
      // !Expired Data
      // print(' this is state $state');
      if (state is SelHomeNotSuccessState) {
        return Center(
            child:
                Text('No data for the queried filter', style: labelTextStyle));
      }

      // if (state is SelHomeRejectedState) {
      if (state is SelHomeLoadedState) {
        // ! data in List

        return Container(
            child: Column(
          children: [
            ListView.builder(
                itemCount: state.selDishData['data'].length,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  if (state.selDishData['data'][index]['DishStatus'] == 3) {
                    return SelProdRejectedItem(
                      prodNumber: state.selDishData['data'][index],
                    );
                  }
                  return Align(child: Text('No Dish ', style: labelTextStyle));
                }),

            // ! LOAD MORE BUTTON
            state.selDishData["totalPages"] != null
                ? Container(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: BigButton(
                          color: darkBlueColor,
                          title: "Load More Food",
                          onTap: () {
                            BlocProvider.of<SelhomepBloc>(context)
                              ..add(SelDataPageItemEvent(
                                  sellerId: widget.sellerId,
                                  pageNum: state.selDishData["pageNumber"] <
                                          state.selDishData["totalPages"]
                                      ? state.selDishData["pageNumber"] + 1
                                      : state.selDishData["pageNumber"],
                                  dishStatus: [3]));
                          }),
                    ),
                  )
                : Container(
                    child: null,
                  ),
          ],
        ));
      }

      return Center(child: CircularProgressIndicator());
    });
  }
}
