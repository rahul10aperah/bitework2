import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Screens/profile.dart';
import 'package:comp1/Seller/Backend/BlocPattern/Reg/sellerregconf_bloc.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';

import 'package:comp1/Seller/Fortend/SellerWidget/AlertDigBox.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/SellerFormField.dart';
import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/http_services.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import '/util/style.dart';
import 'SelVerify.dart';

class SellerRegisterScr extends StatefulWidget {
  static const routeName = '/sellersignupscreens';
  SellerRegisterScr({Key? key}) : super(key: key);

  @override
  State<SellerRegisterScr> createState() => _SellerRegisterScrState();
}

class _SellerRegisterScrState extends State<SellerRegisterScr> {
  SellerregconfBloc prodBloc = SellerregconfBloc();

  // @override
  // void initState() {
  //   prodBloc = BlocProvider.of<SellerregconfBloc>(context);
  //   prodBloc.add(());
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(title: 'Seller'),
      body: SingleChildScrollView(
        child: Container(
          child: BlocConsumer<SellerregconfBloc, SellerregconfState>(
              listener: (context, state) {
            print('state $state');
            if (state is SellerRegErrorState) {
              snackBar(context, state.error.toString());
            }
            if (state is SellerRegSuccessState) {
              navigationPush(
                  context,
                  SelVerifyCard(
                      txtData:
                          'Registration is Success. You Will be Contacted Shortly',
                      onTap: () => navigationPush(context, Profile())));
            }

            if (state is SelOtpSendState) {
              snackBar(context, state.message.toString());
            }

            if (state is SelOtpResendState) {
              snackBar(context, state.message.toString());
            }
          },
              // child: BlocBuilder<SellerregconfBloc, SellerregconfState>(
              builder: (context, state) {
            print('state builder $state');
            return NewSellRegForm2();
          }),
        ),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                     // ! 2. SIGNUP FORM WITHOUT BLOC LOGIC                    */
/* -------------------------------------------------------------------------- */

// ! New inputFormatterData
class NewSellRegForm2 extends StatefulWidget {
  NewSellRegForm2({Key? key}) : super(key: key);

  @override
  _NewSellRegForm2State createState() => _NewSellRegForm2State();
}

class _NewSellRegForm2State extends State<NewSellRegForm2> {
  dynamic gValue = '1';
  dynamic modDelValue = 'Delivery';

  final formKey = GlobalKey<FormState>();
  final fnameController = TextEditingController();
  final lnameController = TextEditingController();
  final phoneController = TextEditingController();
  final dateController = TextEditingController();
  final streetController = TextEditingController();
  final cityController = TextEditingController();
  final stateController = TextEditingController();
  final zipCodeController = TextEditingController();
  final countryController = TextEditingController();

  dynamic fodperCon;
  dynamic govIdCon;
  // PickedFile? fodperCon;
  // PickedFile? govIdCon;

  final expController = TextEditingController();
  final refferalController = TextEditingController();
  final hearController = TextEditingController();
  final intersetController = TextEditingController();
  final specialController = TextEditingController();
  final cookTimeController = TextEditingController();
  final cookDayController = TextEditingController();
  dynamic otpValue;
  String? filename1;
  String? filename2;
  dynamic mimeTypeData1;
  List<dynamic> countryList = ['India'];
  List hearList = [];
  List<dynamic> intersetList = [];
  List<dynamic> cookDayList = [];
  List<dynamic> cookTimeList = [];
  List<dynamic> prepartionTimeList = [];
  List<dynamic> stateList = [];
  List<dynamic> cityList = [];

  List<Map<dynamic, dynamic>> productMap = [];

  Future apiData() async {
    final response = await getDioRequest(
      URLConstants.selRegFieldData,
    );

    var datar = response;

    if (response != null && response['success'] == 1) {
      for (var v in datar['getOftenCookType']) {
        setState(() {
          cookDayList.add(v['Name']);
        });
      }

      for (var v in datar['getTimeCookType']) {
        setState(() {
          cookTimeList.add(v['Name']);
        });
      }

      for (var v in datar['hearAboutUsType']) {
        setState(() {
          hearList.add(v['Name']);
        });
      }

      for (var v in datar['interestType']) {
        setState(() {
          intersetList.add(v['Name']);
        });
      }

      for (var v in datar['places']) {
        setState(() {
          cityList.add(v['City']);
          stateList.add(v['State']);
          // automaticData.addAll({'City': v['City'], 'State': v['State']});
          productMap.addAll({
            {'City': v['City'], 'State': v['State']}
          });

          // print('stateList ==========$stateList');
        });
        // print('cityList ==========$automaticData');
        // print('cityList ==========$productMap');
        // print('cityList ==========${productMap.length}');
      }

      return true;
    } else {
      // if (response['message'] != null) return response;
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    apiData();
  }

  @override
  Widget build(BuildContext context) {
    final formvalid = Provider.of<SelValidation>(context);

    // ! rister method
    _registerNow() async {
      var isvalid = formKey.currentState!.validate();
      if (!isvalid) {
        return 'Please Enter Valid Data';
      }
      formKey.currentState!.save();

      // ! Save Data in models
      FormData regMod = new FormData.fromMap({
        "firstName": fnameController.text,
        "lastName": lnameController.text,
        "phone": "91" + phoneController.text,
        "timeToContact": dateController.text,
        "street": streetController.text,
        "city": cityController.text,
        "state": stateController.text,
        "zipCode": zipCodeController.text,
        "country": countryController.text,
        "foodPermit": gValue,

        "foodPermitImage": fodperCon != null
            ? await MultipartFile.fromFile(fodperCon,
                filename: filename1,
                contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
            : '',
        "govtIdImage": govIdCon != null
            ? await MultipartFile.fromFile(govIdCon,
                filename: filename2,
                contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
            : '',
        "cuisineSpecialize": specialController.text,
        "cookDay": cookDayController.text,
        "cookTime": cookTimeController.text,
        "modeOfDelivery": modDelValue,
        "experience": expController.text,
        "hearAboutUs": hearController.text,
        "interests": intersetController.text,

        "sellerReference":
            refferalController.text.isNotEmpty ? refferalController.text : '',
        "phoneVerify": otpValue != null ? otpValue.toString() : 0.toString(),
        // "phoneVerify": '1'
      });
      var isregis = BlocProvider.of<SellerregconfBloc>(context)
        ..add(SelRegSaveBtnEvent(regModel: regMod));
    }

    // ! end register method

    return Padding(
      padding: EdgeInsets.only(right: 5.0, left: 5.0, top: 5.0),
      child: Form(
        // autovalidateMode: AutovalidateMode.onUserInteraction,
        key: formKey,
        child: ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: [
              SelFieldCard(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //! NameEdit text
                    Row(
                      children: [
                        SelTwoInOneField(
                          child: SellerEditFormFields(
                            controller: fnameController,
                            inputType: TextInputType.name,
                            labelText: 'FirstName',
                            placeholder: 'pawan',
                            formValidator: (v) => formvalid.reqField(v),
                          ),
                        ),
                        SelTwoInOneField(
                          child: SellerEditFormFields(
                            controller: lnameController,
                            inputType: TextInputType.name,
                            labelText: 'LastName',
                            placeholder: 'kumar',
                            formValidator: (v) => formvalid.reqField(v),
                          ),
                        ),
                      ],
                    ),

                    //  ! Phone
                    heightSizedBox(10.0),

                    // SelOneField(
                    Padding(
                      padding: const EdgeInsets.fromLTRB(9, 0, 12, 0),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Phone (To sechedule interview)',
                                style: labelTextStyle),
                            Container(
                              // constraints: BoxConstraints.tightFor(height: 20),
                              width: double.maxFinite,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          // top: 4,
                                          bottom: 9.0,
                                        ),
                                        child: Text('+91',
                                            softWrap: true,
                                            style: labelTextStyle),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            // top: 2,
                                            // bottom: 2.0,
                                            left: .3,
                                            right: .1),
                                        child: Container(
                                          // height: 30,
                                          width: getWidth(context) / 1.7,
                                          child: TextFormField(
                                              autovalidateMode: AutovalidateMode
                                                  .onUserInteraction,
                                              controller: phoneController,
                                              keyboardType: otpValue != 1
                                                  ? TextInputType.number
                                                  : null,
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    10),
                                              ],
                                              onChanged: (val) {
                                                if (phoneController
                                                        .text.length !=
                                                    10) {
                                                  setState(() {
                                                    otpValue = 0;
                                                  });
                                                }
                                              },
                                              validator: (val) {
                                                if (val == null || val.isEmpty)
                                                  return 'Mobile Number is Required.';
                                                else if (val.length < 10)
                                                  return 'Mobile number should be 10 digits';
                                                else if (otpValue != 1) {
                                                  return 'Please verify phone first';
                                                } else
                                                  return null;
                                              },
                                              decoration: InputDecoration(
                                                counter: Offstage(),
                                                labelStyle: TextStyle(
                                                    fontSize: 10,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black),
                                                // labelText: widget.labelText,
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        vertical: 4.5,
                                                        horizontal: 1),
                                                isDense: true,
                                                focusedBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: redColor,
                                                      width: 1.0),
                                                ),
                                                enabledBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.grey,
                                                      width: 1.0),
                                                ),
                                                border: UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Colors.grey,
                                                        width: 1.0)),
                                                focusColor: Colors.black,
                                                hoverColor: Colors.black,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        // top: 4,
                                        bottom: 3.0,
                                        left: .2,
                                        right: .1),
                                    child: Container(
                                      width: getWidth(context) / 6,
                                      height: 20,
                                      decoration: BoxDecoration(
                                        color: darkBlueColor,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: MaterialButton(
                                        elevation: 1,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        color: otpValue == 1
                                            ? offWhiteColor
                                            : darkBlueColor,
                                        onPressed: () async {
                                          if (phoneController.text.isEmpty ||
                                              phoneController.text.length <
                                                  10) {
                                            flutterToast(
                                              phoneController.text.isEmpty
                                                  ? 'Please Enter Mobile Number'
                                                  : 'Mobile number should be 10 digits',
                                            );
                                          }
                                          if (phoneController.text.length ==
                                              10) {
                                            dynamic shouldUpdate =
                                                await showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (BuildContext context) {
                                                var mat = BlocProvider.of<
                                                    SellerregconfBloc>(context)
                                                  ..add(SelRegSendOtpEvent(
                                                    phone: phoneController.text,
                                                  ));
                                                return AlrtBox(
                                                    phone:
                                                        phoneController.text);
                                              },
                                            );
                                            if (shouldUpdate == 1) {
                                              setState(() {
                                                otpValue = shouldUpdate;
                                              });
                                            }
                                            // print('OtpValue $otpValue');
                                          }
                                        },
                                        child: Text(
                                            otpValue == 1
                                                ? 'Verfied'
                                                : 'Verfiy',
                                            softWrap: true,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 10,

                                              color: otpValue == 1
                                                  ? blackColor
                                                  : offWhiteColor,
                                              // color: redColor
                                            )),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ]),
                    ),

                    //  ! Time
                    heightSizedBox(10.0),

                    SelOneField(
                      child: SellerEditFormFields(
                        controller: dateController,
                        inputType: TextInputType.datetime,
                        labelText: 'Best Time for Contact',
                        timeName: 'DateField',
                        readOnly: true,
                        // placeholder: '02:4:2021',
                        fieldAble: false,
                        sufIcon: Icon(Icons.alarm, color: redColor),
                        formValidator: (v) => formvalid.reqField(v),
                      ),
                    ),

                    // ! address
                    heightSizedBox(10.0),

                    SelOneField(
                      child: SellerEditFormFields(
                        controller: streetController,
                        inputType: TextInputType.streetAddress,
                        labelText: 'Home Address (where you cook)',
                        placeholder: 'Street',
                        formValidator: (v) => formvalid.reqField(v),
                      ),
                    ),

                    // heightSizedBox(8.0),

                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(9, 0, 12, 0),
                          child: Container(
                            width: getWidth(context) / 2.15,
                            // width: 10,
                            child: DropDownBtn(
                              formValidator: (dynamic val) =>
                                  formvalid.reqField(val),
                              pageName: 'City',
                              listData: cityList,
                              listController: cityController,
                              onChanged: (dynamic newValue) {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                for (var v in productMap) {
                                  if (newValue == v['City']) {
                                    // print('its working ');
                                    setState(() {
                                      cityController.text = v['City'];
                                      stateController.text = v['State'];
                                      countryController.text = 'India';
                                    });
                                  }
                                }
                              },
                              // selectedItemBuilder: (BuildContext context) {
                              //   return cityList.map<Widget>((dynamic item) {
                              //     if (item.toString() == 'Balasore') {
                              //       print('mango');
                              //     }
                              //     print('============= $item');
                              //     return Text(item);
                              //   }).toList();
                              // },
                            ),
                            // child: SellerEditFormFields(
                            //     controller: cityController,
                            //     inputType: TextInputType.text,
                            //     placeholder: 'City',
                            //     formValidator: (v) => formvalid.reqField(v),
                            //     prefixIcon:
                            //         Icon(Icons.arrow_drop_down_circle_sharp),
                            //     onTap: () {}),
                          ),
                        ),
                        SelTwoInOneField(
                          child: SellerEditFormFields(
                            controller: zipCodeController,
                            inputType: TextInputType.number,
                            placeholder: 'ZipCode',
                            formValidator: (v) => formvalid.validateZipCode(v),
                            inputFormatterData: [
                              LengthLimitingTextInputFormatter(6),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(9, 0, 12, 0),
                          child: Container(
                            width: getWidth(context) / 2.15,
                            child: DropDownBtn(
                              formValidator: (dynamic val) =>
                                  formvalid.reqField(val),
                              pageName: 'State',
                              listData: stateList,
                              listController: stateController,
                              // onChanged: (dynamic newValue) {
                              //   FocusScope.of(context)
                              //       .requestFocus(new FocusNode());
                              //   print(cityController.text);
                              //   for (var v in productMap) {
                              //     if (newValue == v['State']) {
                              //       setState(() {
                              //         cityController.text = v['City'];
                              //         stateController.text = v['State'];
                              //         countryController.text = 'India';
                              //       });
                              //     }
                              //   }
                              // },
                            ),
                          ),
                        ),
                        SelTwoInOneField(
                            child: DropDownBtn(
                          formValidator: (dynamic val) =>
                              formvalid.reqField(val),
                          pageName: 'Country',
                          listData: countryList,
                          listController: countryController,
                        )),
                      ],
                    ),
                    heightSizedBox(10.0),
                    // Padding(
                    //   padding: const EdgeInsets.fromLTRB(9, 0, 15, 0),
                    //   child: Text('Food Permit', style: labelTextStyle),
                    // ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(9, 0, 15, 0),
                      child: Row(
                        children: [
                          Text('Food Permit', style: labelTextStyle),
                          SellerRadBtn(
                            title: 'Yes',
                            val: '1',
                            gValue: gValue,
                            onChg: (ind) {
                              setState(() {
                                gValue = ind;
                              });
                            },
                          ),
                          SellerRadBtn(
                            title: 'No',
                            val: '2',
                            gValue: gValue,
                            onChg: (dynamic ind) {
                              setState(() {
                                gValue = ind;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    //  ! Food Certificate
                    heightSizedBox(10.0),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(9, 0, 15, 0),
                      child:
                          // SellerUpdBtn
                          imgField(
                              title: 'Food Permit',
                              filename: filename1,
                              cleanImg: () {
                                setState(() {
                                  fodperCon = null;
                                  filename1 = null;
                                });
                              },
                              onPressed: () async {
                                // var image1 = await picker.pickImage(
                                //   source: ImageSource.gallery,
                                //   imageQuality: 25,
                                // );
                                var image1 = await FilePicker.platform
                                    .pickFiles(
                                        type: FileType.custom,
                                        allowedExtensions: ['pdf', 'jpeg']);
                                print(image1);
                                setState(() {
                                  filename1 = image1?.files.first.name;
                                  fodperCon = image1?.files.first.path;
                                  mimeTypeData1 =
                                      lookupMimeType(fodperCon)!.split('/');
                                  // print(mimeTypeData1);
                                  // print(mimeTypeData1[0]);
                                  // print(mimeTypeData2[1]);
                                });
                              }),
                    ),
                    heightSizedBox(10.0),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(9, 0, 15, 0),
                      child:
                          // SellerUpdBtn
                          imgField(
                              title: 'Govt Permit',
                              filename: filename2,
                              cleanImg: () {
                                setState(() {
                                  govIdCon = null;
                                  filename2 = null;
                                });
                              },
                              onPressed: () async {
                                // var image2 = await picker.pickImage(
                                //   source: ImageSource.gallery,
                                //   imageQuality: 25,
                                // );
                                var image2 = await FilePicker.platform
                                    .pickFiles(
                                        type: FileType.custom,
                                        allowedExtensions: ['pdf', 'jpeg']);
                                setState(() {
                                  filename2 = image2?.files.first.name;
                                  govIdCon = image2?.files.first.path;
                                  mimeTypeData1 =
                                      lookupMimeType(govIdCon)!.split('/');
                                });
                              }),
                    )
                  ],
                ),
              ),

              /* -------------------------------------------------------------------------- */
              /*                             // END  FORM FIELD   PART 1                          */
              /* -------------------------------------------------------------------------- */
              SelFieldCard(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(9, 0, 15, 0),
                      child: Text('Other Info ', style: labelTextStyle),
                    ),
                    heightSizedBox(10.0),

                    SelOneField(
                        child: SellerEditFormFields(
                      controller: specialController,
                      formValidator: (v) => formvalid.reqField(v),
                      inputType: TextInputType.text,
                      placeholder: 'Hydarbadi(indian)',
                      labelText: 'Cuisines you specialized in',
                    )),

                    // ! day for work

                    heightSizedBox(10.0),

                    SelOneField(
                      child: DropDownBtn(
                        // pageName: 'Everyday',
                        listData: cookDayList,
                        listController: cookDayController,
                        formValidator: (v) => formvalid.reqField(v),
                        labelText: 'How often do you cook?',
                      ),
                    ),

                    heightSizedBox(10.0),

                    SelOneField(
                      child: DropDownBtn(
                        // pageName: 'Once a week',
                        listData: cookTimeList,
                        listController: cookTimeController,
                        formValidator: (v) => formvalid.reqField(v),
                        labelText: 'What time of day you cook?',
                      ),
                    ),

                    heightSizedBox(10.0),
                    DayWorkRaioBtn(
                        labelText: 'Mode of Delivery',
                        t1: 'Pickup',
                        t2: 'Delivery',
                        t3: 'Both',
                        val1: 'Pickup',
                        val2: 'Delivery',
                        val3: 'Both',
                        gVal: modDelValue,
                        onChg: (dynamic ind) {
                          setState(() {
                            modDelValue = ind;
                          });
                        }),
                    // ! Experince Level
                    heightSizedBox(10.0),

                    SelOneField(
                        child: SellerEditFormFields(
                      controller: expController,
                      inputType: TextInputType.number,
                      placeholder: '05 years',
                      labelText: 'How many years of experince in cooking',
                      inputFormatterData: [
                        LengthLimitingTextInputFormatter(2),
                      ],
                    )),

                    heightSizedBox(10.0),

                    SelOneField(
                      child: DropDownBtn(
                          formValidator: (v) => formvalid.reqField(v),
                          // pageName: 'from friends',
                          listData: hearList,
                          listController: hearController,
                          labelText: 'How did you hear about us ?'),
                    ),

                    heightSizedBox(10.0),

                    SelOneField(
                      child: DropDownBtn(
                        formValidator: (v) => formvalid.reqField(v),
                        // pageName: 'Interset',
                        listData: intersetList,
                        listController: intersetController,
                        labelText: 'What interest brings you here ?',
                      ),
                    ),

                    heightSizedBox(10.0),

                    SelOneField(
                        child: SellerEditFormFields(
                      controller: refferalController,
                      inputType: TextInputType.text,
                      // placeholder: 'CA987SNMY',
                      labelText: 'Referral Code',
                    )),
                  ],
                ),
              ),

              // ! SUBMIT BUTTON
              Padding(
                // padding: EdgeInsets.only(left: 25.0, right: 25.0),
                padding: EdgeInsets.only(left: 11.0, right: 11.0),
                child: BigButton(
                  title: 'Submit',
                  onTap: _registerNow,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 1, right: 1, top: 2, bottom: 2),
                child: Container(
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SelTCSTxt('By submitting you agree to our', fSize: 10.0),
                      SelTCUTxt('Terms & Condtions', fSize: 10.0),
                      SelTCSTxt('and', fSize: 10.0),
                      SelTCUTxt('Privacy Statment', fSize: 10.0)
                    ],
                  ),
                ),
              )
            ]),
      ),
    );
  }

  Widget imgField(
      {required String title,
      Function()? onPressed,
      String? filename,
      Function()? cleanImg}) {
    return Row(
      children: [
        Text(title, style: labelTextStyle, textAlign: TextAlign.center),
        Padding(
          padding: const EdgeInsets.only(left: 9.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                children: [
                  MaterialButton(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      color: darkBlueColor,
                      child: Text("Upload",
                          style: TextStyle(
                              color: offWhiteColor,
                              fontFamily: montserratMedium)),
                      onPressed: onPressed),
                  Padding(
                    padding: const EdgeInsets.only(top: 18.0),
                    child: Container(
                        child: filename == null || filename.isEmpty
                            ? null
                            : InkWell(
                                onTap: cleanImg,
                                child: SelIcon(Icons.cancel, color: redColor))),
                  )
                ],
                // ),
              ),
              Text(
                filename == null || filename.isEmpty ? 'No Files' : filename,
                softWrap: true,
                style: TextStyle(
                  fontFamily: montserratMedium,
                  fontSize: 8,
                ),
                maxLines: 1,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
