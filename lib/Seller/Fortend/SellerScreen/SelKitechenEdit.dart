import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Screens/seller_dashboard.dart';

import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/util/http_services.dart';

import 'package:dio/dio.dart';

import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Seller/Backend/BlocPattern/DishAdd/dishadd_bloc.dart';

import 'package:comp1/Seller/Fortend/SellerWidget/SellerFormField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/Selleruplod.dart';
import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:comp1/util/style.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';

class SelKitchenEditScr extends StatelessWidget {
  final dynamic prodNumber;
  final dynamic sellerId;
  static const routeName = '/SelKitchenEditScr';
  SelKitchenEditScr({Key? key, this.prodNumber, this.sellerId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Edit Dish',
      ),
      body: SingleChildScrollView(
        child: Container(
          child: BlocConsumer<DishaddBloc, DishaddState>(
              listener: (context, state) {
            if (state is DishAddedSuccessState) {
              // snackBar(context, state.message.toString());
              // flutterToast(state.message.toString());
              navigationPush(context, SellerDashboard(sellerId: sellerId));
            }
          }, builder: (context, state) {
            if (state is DishAddedErrorState) {
              // snackBar(context, state.error.toString());
            }

            return SelKitchenEditScrForm(
              prodNumber: prodNumber,
            );
          }),
        ),
      ),
    );
  }
}

class SelKitchenEditScrForm extends StatefulWidget {
  dynamic prodNumber;
  SelKitchenEditScrForm({Key? key, this.prodNumber}) : super(key: key);

  @override
  _SelKitchenEditScrFormState createState() => _SelKitchenEditScrFormState();
}

class _SelKitchenEditScrFormState extends State<SelKitchenEditScrForm> {
  final _form = GlobalKey<FormState>();
  final dishNameCon = TextEditingController();
  final descriptionCon = TextEditingController();
  final priceCon = TextEditingController();
  final cuisinesCon = TextEditingController();

  final allergensCon = TextEditingController();
  final markingCon = TextEditingController();
  final categoryCon = TextEditingController();
  final preptimeCon = TextEditingController(text: '');
  final highLightCon = TextEditingController();
  final dietaryCon = TextEditingController();
  final spicyCon = TextEditingController();

  dynamic img1;
  dynamic img2;
  dynamic img3;
  dynamic img4;

  // ! check imgSrc
  dynamic imgSrc1;
  dynamic imgSrc2;
  dynamic imgSrc3;
  dynamic imgSrc4;

  // ! valid Day
  DateTime? validDay;
  dynamic yr1;
  dynamic mn;
  dynamic dy;
  // ! Exrata setDate con
  final setDateNew = TextEditingController();

  // ! List Data
  List<String> allergensList = [];

  List<String> markingList = [];

  List<String> cuisinesList = [];

  List<String> spicyList = [];
  List<String> categoryList = [];
  List<String> dietaryList = [];

  Future apiData() async {
    final response = await getDioRequest(
      URLConstants.selDishFliterField,
    );

    var datar = response;

    if (response != null && response['success'] == 1) {
      for (var v in datar['allergenType']) {
        setState(() {
          allergensList.add(v['Name']);
        });
        // print('allergn $allergensList');
      }

      for (var v in datar['markingType']) {
        setState(() {
          markingList.add(v['Name']);
        });
        // print('marking $markingList');
      }

      for (var v in datar['cuisineType']) {
        setState(() {
          cuisinesList.add(v['Name']);
        });
        // print('cusines $cuisinesList');
      }

      for (var v in datar['spicyType']) {
        setState(() {
          spicyList.add(v['Name']);
        });
        // print('spicy $spicyList');
      }

      for (var v in datar['dieteryType']) {
        setState(() {
          dietaryList.add(v['Name']);
        });
        // print('dietary  $dietaryList');
      }

      for (var v in datar['categoryType']) {
        setState(() {
          categoryList.add(v['Name']);
        });
        // print('category $categoryList');
      }
      return true;
    } else {
      // if (response['message'] != null) return response;
      return false;
    }
  }

  final ImagePicker _picker = ImagePicker();
  dynamic detailsCon = false;
  dynamic mimeTypeData1;
  dynamic clBtn1 = false;
  dynamic clBtn2 = false;
  dynamic clBtn3 = false;
  dynamic clBtn4 = false;
  // ignore: unused_element
  _sellerItemMetod() async {
    var isvalid = _form.currentState!.validate();
    if (!isvalid) {
      return 'Please Enter Valid Data';
    }
    _form.currentState!.save();

    // ! new

    FormData moreItemM = new FormData.fromMap({
      "dishId": widget.prodNumber['DishId'].toString(),
      "servingDateFrom": widget.prodNumber['servingDateFrom'].toString(),
      "servingDateTo": widget.prodNumber['servingDateTo'].toString(),
      "servingTimeFrom": widget.prodNumber['servingTimeFrom'].toString(),
      "servingTimeTo": widget.prodNumber['servingTimeTo'].toString(),
      "servingsAvailable": widget.prodNumber['servingsAvailable'].toString(),
      "orderLimit": widget.prodNumber['orderLimit'].toString(),
      "dishName": dishNameCon.text.toString().isNotEmpty
          ? dishNameCon.text.toString()
          : widget.prodNumber['DishName'].toString(),
      "description": descriptionCon.text.isNotEmpty
          ? descriptionCon.text.toString()
          : widget.prodNumber['Description'].toString(),
      "price": priceCon.text.isNotEmpty
          ? priceCon.text.toString()
          : widget.prodNumber['Price'].toString(),
      "discountPercentage": '',
      "discountPrice": '',
      "cuisine": cuisinesCon.text.isNotEmpty
          ? cuisinesCon.text.toString()
          : widget.prodNumber['Cuisine'].toString(),
      "dietary": dietaryCon.text.isNotEmpty
          ? dietaryCon.text.toString()
          : widget.prodNumber['Dietary'].toString(),
      "spicy": spicyCon.text.isNotEmpty
          ? spicyCon.text.toString()
          : widget.prodNumber['Spicy'].toString(),
      "allergen": allergensCon.text.isNotEmpty
          ? allergensCon.text.toString()
          : widget.prodNumber['Allergen'].toString(),
      "marking": markingCon.text.isNotEmpty
          ? markingCon.text.toString()
          : widget.prodNumber['Marking'].toString(),
      "category": categoryCon.text.isNotEmpty
          ? categoryCon.text.toString()
          : widget.prodNumber['Category'].toString(),
      "preperationTime": preptimeCon.text.isNotEmpty
          ? preptimeCon.text.toString()
          : widget.prodNumber['PreperationTime'].toString(),
      "otherHighlight": highLightCon.text.isNotEmpty
          ? highLightCon.text.toString()
          : widget.prodNumber['OtherHighlight'].toString(),
      "dishImageUrl1": img1 != null
          ? await MultipartFile.fromFile(img1.path,
              filename: img1.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
      "dishImageUrl2": img2 != null
          ? await MultipartFile.fromFile(img2.path,
              filename: img2.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
      "dishImageUrl3": img3 != null
          ? await MultipartFile.fromFile(img3.path,
              filename: img3.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          // : widget.prodNumber['DishImageUrl3'].toString(),
          : null,
      "dishImageUrl4": img4 != null
          ? await MultipartFile.fromFile(img4.path,
              filename: img4.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          // : widget.prodNumber['DishImageUrl4'].toString(),
          : null,
    });
    var isregis = await BlocProvider.of<DishaddBloc>(context)
      ..add(EditKitechenDataEvent(editItemModel: moreItemM));
  }

  // ! INIT STATE                                   */
  @override
  void initState() {
    super.initState();
    preptimeCon.text = preptimeCon.text.toString().isNotEmpty
        ? preptimeCon.text
        : widget.prodNumber['PreperationTime'] != null
            ? widget.prodNumber['PreperationTime'].toString()
            : '';

    // imgSrc1 = widget.prodNumber['DishImageUrl1'].toString().isNotEmpty
    //     ? widget.prodNumber['DishImageUrl1'].toString()
    //     : '';
    // imgSrc2 = widget.prodNumber['DishImageUrl2'].toString().isNotEmpty
    //     ? widget.prodNumber['DishImageUrl2'].toString()
    //     : '';
    // imgSrc3 = widget.prodNumber['DishImageUrl3'].toString().isNotEmpty
    //     ? widget.prodNumber['DishImageUrl3'].toString()
    //     : '';
    // imgSrc4 = widget.prodNumber['DishImageUrl4'].toString().isNotEmpty
    //     ? widget.prodNumber['DishImageUrl4'].toString()
    //     : '';

    apiData();
  }

  @override
  Widget build(BuildContext context) {
    // print('dishId  : - ' + widget.prodNumber['DishId'].toString());
    // print(
    //     'dishId date1 : - ' + widget.prodNumber['ServingDateFrom'].toString());
    // print(
    //     'dishId  dtate2 : - ' + widget.prodNumber['ServingDateTo'].toString());
    // print(
    //     'dishId time1 : - ' + widget.prodNumber['ServingTimeFrom'].toString());
    // print('dishId  time2: - ' + widget.prodNumber['ServingTimeTo'].toString());
    // print('dishId avalible : - ' +
    //     widget.prodNumber['ServingsAvailable'].toString());
    // print('dishId limit : - ' + widget.prodNumber['OrderLimit'].toString());
    // print('---------------------------------');
    // print(
    //     'dishId name : - ' + dishNameCon.text + widget.prodNumber['DishName']);
    // print('dishId description : - ' +
    //     descriptionCon.text +
    //     widget.prodNumber['Description']);
    // print('dishId price : - ' +
    //     priceCon.text +
    //     widget.prodNumber['Price'].toString());
    // print('dishId cisunse : - ' +
    //     cuisinesCon.text +
    //     widget.prodNumber['Cuisine']);
    // print(
    //     'dishId dietary : - ' + dietaryCon.text + widget.prodNumber['Dietary']);
    // print('dishId  spicy : - ' + spicyCon.text + widget.prodNumber['Spicy']);
    // print('dishId alleg : - ' +
    //     allergensCon.text +
    //     widget.prodNumber['Allergen']);
    // print(
    //     'dishId marking : - ' + markingCon.text + widget.prodNumber['Marking']);
    // print('dishId categoru : - ' +
    //     categoryCon.text +
    //     widget.prodNumber['Category']);
    // print('dishId  pretime: - ' +
    //     preptimeCon.text +
    //     widget.prodNumber['PreperationTime'].toString());
    // print('dishId higjlithj : - ' +
    //     highLightCon.text +
    //     widget.prodNumber['OtherHighlight']);
    print('img1 ${widget.prodNumber['DishImageUrl1'].runtimeType} ');
    print('img2 ${widget.prodNumber['DishImageUrl2'].runtimeType} ');
    print('img3 ${widget.prodNumber['DishImageUrl3'].toString().length} ');
    print('img4 ${widget.prodNumber['DishImageUrl4']} ');
    if (widget.prodNumber['DishImageUrl1'].toString().isNotEmpty ||
        imgSrc1 != null) {
      setState(() {
        clBtn1 = true;
      });
    }
    if (widget.prodNumber['DishImageUrl2'].toString().isNotEmpty ||
        imgSrc2 != null) {
      setState(() {
        clBtn2 = true;
      });
    }
    if (widget.prodNumber['DishImageUrl3'].toString().isNotEmpty ||
        imgSrc3 != null) {
      setState(() {
        clBtn3 = true;
      });
    }
    if (widget.prodNumber['DishImageUrl4'].toString().isNotEmpty ||
        imgSrc4 != null) {
      setState(() {
        clBtn4 = true;
      });
    }
    print('clBtn1 $clBtn1');
    print('clBtn 2$clBtn2');
    print('clBtn3 $clBtn3');
    print('clBtn4 $clBtn4');
    return Padding(
      padding: EdgeInsets.only(right: 5.0, left: 5.0, top: 5.0),
      child: Form(
        key: _form,
        // autovalidateMode: AutovalidateMode.always,
        child: ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: [
              SelFieldCard(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Add Dish Images', style: labelTextStyle),
                    SizedBox(
                      height: 85,
                      width: 500,
                      child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: [
                          SelUpdMultiFileNet(
                            networkImage: widget.prodNumber['DishImageUrl1'],
                            imgSrc: imgSrc1,
                            backColor: clBtn1 == false ? offWhiteColor : null,
                            upDFile: () async {
                              var image = await _picker.pickImage(
                                source: ImageSource.gallery,
                                imageQuality: 25,
                              );

                              setState(() {
                                img1 = image;
                                imgSrc1 = image!.path.toString();
                                mimeTypeData1 =
                                    lookupMimeType(imgSrc1)!.split('/');
                              });
                            },
                            cleanImg: () {
                              setState(() {
                                img1 = null;
                                imgSrc1 = null;
                                clBtn1 = false;
                              });
                            },
                          ),
                          SelUpdMultiFileNet(
                            networkImage: widget.prodNumber['DishImageUrl2'],
                            imgSrc: imgSrc2,
                            backColor: clBtn2 == false
                                // backColor: imgSrc2.isEmpty
                                ? offWhiteColor
                                : null,
                            upDFile: () async {
                              var image = await _picker.pickImage(
                                source: ImageSource.gallery,
                                imageQuality: 25,
                              );

                              setState(() {
                                img2 = image;
                                imgSrc2 = image!.path.toString();
                                mimeTypeData1 =
                                    lookupMimeType(imgSrc2)!.split('/');
                              });
                            },
                            cleanImg: () {
                              setState(() {
                                img2 = null;
                                imgSrc2 = null;
                                clBtn2 = false;
                              });
                            },
                          ),
                          SelUpdMultiFileNet(
                            networkImage: widget.prodNumber['DishImageUrl3'],
                            imgSrc: imgSrc3,
                            backColor: clBtn3 == false ? offWhiteColor : null,
                            upDFile: () async {
                              var image = await _picker.pickImage(
                                source: ImageSource.gallery,
                                imageQuality: 25,
                              );

                              setState(() {
                                img3 = image;
                                imgSrc3 = image!.path.toString();
                                mimeTypeData1 =
                                    lookupMimeType(imgSrc3)!.split('/');
                              });
                            },
                            cleanImg: () {
                              setState(() {
                                img3 = null;
                                imgSrc3 = null;
                                clBtn3 = false;
                              });
                            },
                          ),
                          SelUpdMultiFileNet(
                            backColor: clBtn4 == false ? offWhiteColor : null,
                            networkImage: widget.prodNumber['DishImageUrl4'],
                            imgSrc: imgSrc4,
                            // networkImage:
                            //     imgSrc4.toString().isNotEmpty ? 'image' : null,
                            upDFile: () async {
                              var image = await _picker.pickImage(
                                source: ImageSource.gallery,
                                imageQuality: 25,
                              );

                              setState(() {
                                img4 = image;
                                imgSrc4 = image!.path.toString();
                                mimeTypeData1 =
                                    lookupMimeType(imgSrc4)!.split('/');
                              });
                            },
                            cleanImg: () {
                              setState(() {
                                img4 = null;
                                imgSrc4 = null;
                                clBtn4 = false;
                              });
                            },
                          ),
                        ],
                      ),
                    ),

                    // ! Field food

                    heightSizedBox(10.0),
                    SelOneField(
                      left: 0,
                      child: SellerEditFormFields(
                          placeholder: 'Hydarbadi(indian)',
                          inputType: TextInputType.name,
                          // controller: dishCon,
                          labelText: 'Dish Name',
                          initValue: widget.prodNumber['DishName']
                                  .toString()
                                  .isNotEmpty
                              ? widget.prodNumber['DishName'].toString()
                              : '',
                          onSaved: (String? val) {
                            setState(() {
                              dishNameCon.text = val!;
                            });
                          }),
                    ),

                    heightSizedBox(10.0),
                    SelOneField(
                      left: 0,
                      child: SellerEditFormFields(
                          placeholder: 'this discription is',
                          inputType: TextInputType.text,
                          // controller: desCon,
                          labelText: 'Dish Description',
                          initValue: widget.prodNumber['Description']
                                  .toString()
                                  .isNotEmpty
                              ? widget.prodNumber['Description'].toString()
                              : '',
                          inputFormatterData: [
                            LengthLimitingTextInputFormatter(180),
                          ],
                          onSaved: (String? val) {
                            setState(() {
                              descriptionCon.text = val!;
                            });
                          }),
                    ),

                    // ! Row Filed for Price and Cusines
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            left: 0,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: SellerEditFormFields(
                                  // controller: priceCon,
                                  inputType: TextInputType.number,
                                  // formValidator: (v) => formvalid.reqField(v),
                                  // placeholder: '80',
                                  labelText: 'Price',
                                  inputFormatterData: [
                                    LengthLimitingTextInputFormatter(3),
                                  ],
                                  initValue: widget.prodNumber['Price']
                                          .toString()
                                          .isNotEmpty
                                      ? widget.prodNumber['Price'].toString()
                                      : '',
                                  onSaved: (String? val) {
                                    setState(() {
                                      priceCon.text = val!;
                                    });
                                  }),
                            )),
                        SelTwoInOneField(
                          left: 3,
                          child: DropDownBtn(
                            labelText: 'Cuisines',
                            // pageName: 'Chicken Briyani',
                            currentItem: widget.prodNumber['Cuisine']
                                    .toString()
                                    .isNotEmpty
                                ? widget.prodNumber['Cuisine'].toString()
                                : '',
                            dName: widget.prodNumber['Cuisine']
                                    .toString()
                                    .isNotEmpty
                                ? widget.prodNumber['Cuisine'].toString()
                                : 'Chicken Briyani',
                            listData: cuisinesList,
                            listController: cuisinesCon,
                          ),
                        )
                      ],
                    ),

                    // ! radio btn Dietary and spicy Level
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            left: 0,
                            child: DropDownBtn(
                              labelText: 'Dietary',
                              // pageName: 'Veg',
                              listData: dietaryList,
                              listController: dietaryCon,
                              currentItem: widget.prodNumber['Dietary']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Dietary'].toString()
                                  : '',
                              dName: widget.prodNumber['Dietary']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Dietary'].toString()
                                  : 'Veg',
                            )),
                        SelTwoInOneField(
                            left: 3,
                            child: DropDownBtn(
                              labelText: 'Spicy',
                              // pageName: 'Mild',
                              listData: spicyList,
                              listController: spicyCon,
                              currentItem: widget.prodNumber['Spicy']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Spicy'].toString()
                                  : '',
                              dName: widget.prodNumber['Spicy']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Spicy'].toString()
                                  : 'Mild',
                            )),
                      ],
                    ),

                    // ! allery and Making
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            left: 0,
                            child: DropDownBtn(
                              labelText: 'Allergens',
                              // pageName: 'Nuts',
                              listData: allergensList,
                              listController: allergensCon,
                              currentItem: widget.prodNumber['Allergen']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Allergen'].toString()
                                  : '',
                              dName: widget.prodNumber['Allergen']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Allergen'].toString()
                                  : 'Nuts',
                            )),
                        SelTwoInOneField(
                            left: 3,
                            child: DropDownBtn(
                              // pageName: 'Halaal',
                              labelText: 'Marking',
                              listData: markingList,
                              listController: markingCon,
                              dName: widget.prodNumber['Marking']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Marking'].toString()
                                  : 'Halal',
                              currentItem: widget.prodNumber['Marking']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Marking'].toString()
                                  : '',
                            )),
                      ],
                    ),

                    // ! Category and Perpartion Time
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            left: 0,
                            child: DropDownBtn(
                              // controller: catCon,
                              // inputType: TextInputType.text,
                              // formValidator: (v) => formvalid.reqField(v),
                              // pageName: 'Lunch',
                              labelText: 'Category',
                              listData: categoryList,
                              listController: categoryCon,
                              dName: widget.prodNumber['Category']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Category'].toString()
                                  : 'Breakfast',
                              currentItem: widget.prodNumber['Category']
                                      .toString()
                                      .isNotEmpty
                                  ? widget.prodNumber['Category'].toString()
                                  : '',
                            )),
                        SelTwoInOneField(
                          left: 0,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: SellerEditFormFields(
                                labelText: 'Perparation Time',
                                readOnly: true,
                                fieldAble: false,
                                timeName: 'DurField',
                                controller: preptimeCon,
                                onSaved: (String? val) {
                                  setState(() {
                                    preptimeCon.text = val!;
                                  });
                                },
                                sufIcon: Icon(Icons.alarm, color: redColor)),
                          ),
                        )
                      ],
                    ),

                    // ! HighLiht THings

                    SelOneField(
                      left: 0,
                      child: SellerEditFormFields(
                          placeholder: 'Any other things to highlight',
                          inputType: TextInputType.text,
                          // controller: highLightCon,
                          labelText: 'Any other things to highlight',
                          initValue: widget.prodNumber['OtherHighlight']
                                  .toString()
                                  .isNotEmpty
                              ? widget.prodNumber['OtherHighlight'].toString()
                              : '',
                          onSaved: (String? val) {
                            setState(() {
                              highLightCon.text = val!;
                            });
                          }),
                    ),
                  ],
                ),
              ),
              Padding(
                // padding: EdgeInsets.only(left: 25.0, right: 25.0),
                padding: EdgeInsets.only(left: 11.0, right: 11.0),
                child: BigButton(
                  title: 'Submit',
                  onTap: _sellerItemMetod,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 1, right: 1, top: 1.5, bottom: 1.5),
                child: Container(
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SelTCSTxt('By submitting you agree to our', fSize: 10.0),
                      SelTCUTxt('Terms & Condtions', fSize: 10.0),
                      SelTCSTxt('and', fSize: 10.0),
                      SelTCUTxt('Privacy Statment', fSize: 10.0)
                    ],
                  ),
                ),
              )
            ]),
      ),
    );
  }
}
