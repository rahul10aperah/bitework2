import 'dart:convert';
import 'package:comp1/Screens/seller_dashboard.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/DataList.dart';
import 'package:comp1/util/http_services.dart';
import 'package:dio/dio.dart';

import 'package:comp1/Elements/buttons.dart';

import 'package:comp1/Seller/Backend/BlocPattern/MoreItem/moreadditem_bloc.dart';

import 'package:comp1/Seller/Fortend/SellerWidget/SellerFormField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/Selleruplod.dart';
import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';

import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';

class SellerAddMoreDetails extends StatelessWidget {
  final dynamic prodNumber;
  final dynamic sellerId;
  static const routeName = '/sellerAddMoreDetails';
  SellerAddMoreDetails({Key? key, this.prodNumber, this.sellerId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('-------------------$sellerId');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: redColor,
        title: Text('Add More Dish Info', style: appBarTS),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //  ! REGISTER PAGE
                MoreFoodOne(prodNumber: prodNumber, sellerId: sellerId)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                          // ! REGISTER PAGE LOGIC                          */
/* -------------------------------------------------------------------------- */
class MoreFoodOne extends StatelessWidget {
  final dynamic prodNumber;
  dynamic sellerId;
  MoreFoodOne({Key? key, this.prodNumber, this.sellerId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<MoreadditemBloc, MoreadditemState>(
        listener: (context, state) {
      if (state is SelMoreItemErrorState) {
        // ScaffoldMessenger.of(context).showSnackBar(
        //   SnackBar(
        //     content: const Text("Added Dish."),
        //     backgroundColor: redColor,
        //   ),
        // );
        // snackBar(context, state.error.toString());
      }
      if (state is SelMoreItemSuccessState) {
        // print('sellerId ------------------------- $sellerId');
        navigationPush(
            context,
            SellerDashboard(
              sellerId: sellerId,
            ));
      }
    }, child: BlocBuilder<MoreadditemBloc, MoreadditemState>(
            builder: (context, state) {
      return SellerItemDetForm(prodNumber: prodNumber);
    }));
  }
}

/* -------------------------------------------------------------------------- */
/*                             // ! Food Item form                            */
/* -------------------------------------------------------------------------- */
class SellerItemDetForm extends StatefulWidget {
  final dynamic prodNumber;
  SellerItemDetForm({Key? key, this.prodNumber}) : super(key: key);

  @override
  _SellerItemDetFormState createState() => _SellerItemDetFormState();
}

class _SellerItemDetFormState extends State<SellerItemDetForm> {
  final _form = GlobalKey<FormState>();

  final avaibleCon = TextEditingController();
  final limitCon = TextEditingController();

  final dishNameCon = TextEditingController();
  final descriptionCon = TextEditingController();
  final priceCon = TextEditingController();
  final cuisinesCon = TextEditingController();
  final allergensCon = TextEditingController();
  final markingCon = TextEditingController();
  final categoryCon = TextEditingController();
  final preptimeCon = TextEditingController(text: '');
  final highLightCon = TextEditingController();
  final dietaryCon = TextEditingController();
  final spicyCon = TextEditingController();
  // final disPertCon = TextEditingController();
  // final disPriceCon = TextEditingController();
  dynamic img1;
  dynamic img2;
  dynamic img3;
  dynamic img4;

  // ! check imgSrc
  dynamic imgSrc1;
  dynamic imgSrc2;
  dynamic imgSrc3;
  dynamic imgSrc4;

  // ! valid Day
  DateTime? validDay;
  dynamic yr1;
  dynamic mn;
  dynamic dy;
  // ! Exrata setDate con
  final setDateNew = TextEditingController();

  // ! List Data
  List<String> allergensList = [];

  List<String> markingList = [];

  List<String> cuisinesList = [];

  List<String> spicyList = [];
  List<String> categoryList = [];
  List<String> dietaryList = [];

  Future apiData() async {
    final response = await getDioRequest(
      URLConstants.selDishFliterField,
    );

    var datar = response;

    if (response != null && response['success'] == 1) {
      for (var v in datar['allergenType']) {
        setState(() {
          allergensList.add(v['Name']);
        });
      }

      for (var v in datar['markingType']) {
        setState(() {
          markingList.add(v['Name']);
        });
      }

      for (var v in datar['cuisineType']) {
        setState(() {
          cuisinesList.add(v['Name']);
        });
      }

      for (var v in datar['spicyType']) {
        setState(() {
          spicyList.add(v['Name']);
        });
      }

      for (var v in datar['dieteryType']) {
        setState(() {
          dietaryList.add(v['Name']);
        });
      }

      for (var v in datar['categoryType']) {
        setState(() {
          categoryList.add(v['Name']);
        });
      }
      return true;
    } else {
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    avaibleCon.text = avaibleCon.text.toString().isNotEmpty
        ? avaibleCon.text.toString()
        : widget.prodNumber['ServingsAvailable'] != null
            ? widget.prodNumber['ServingsAvailable'].toString()
            : '';
    limitCon.text = limitCon.text.toString().isNotEmpty
        ? limitCon.text.toString()
        : widget.prodNumber['OrderLimit'] != null
            ? widget.prodNumber['OrderLimit'].toString()
            : '';

    preptimeCon.text = preptimeCon.text.toString().isNotEmpty
        ? preptimeCon.text
        : widget.prodNumber['PreperationTime'] != null
            ? widget.prodNumber['PreperationTime'].toString()
            : '';

    apiData();
  }

  final ImagePicker _picker = ImagePicker();
  dynamic detailsCon = false;
  dynamic mimeTypeData1;
  dynamic clBtn1 = false;
  dynamic clBtn2 = false;
  dynamic clBtn3 = false;
  dynamic clBtn4 = false;
  // ignore: unused_element
  _sellerDetMet() async {
    var isvalid = _form.currentState!.validate();
    if (!isvalid) {
      return 'Please Enter Valid Data';
    }
    _form.currentState!.save();

    // ! new

    FormData moreItemM = new FormData.fromMap({
      "dishId": widget.prodNumber['DishId'].toString(),
      "servingsAvailable": avaibleCon.text.toString().isNotEmpty
          ? avaibleCon.text.toString()
          : widget.prodNumber['servingsAvailable'].toString(),
      "orderLimit": limitCon.text.toString().isNotEmpty
          ? limitCon.text.toString()
          : widget.prodNumber['orderLimit'].toString(),
      "dishName": dishNameCon.text.toString().isNotEmpty
          ? dishNameCon.text.toString()
          : widget.prodNumber['DishName'].toString(),
      "description": descriptionCon.text.isNotEmpty
          ? descriptionCon.text.toString()
          : widget.prodNumber['Description'].toString(),
      "price": priceCon.text.isNotEmpty
          ? priceCon.text.toString()
          : widget.prodNumber['Price'].toString(),
      "discountPercentage": '',
      "discountPrice": '',
      "cuisine": cuisinesCon.text.isNotEmpty
          ? cuisinesCon.text.toString()
          : widget.prodNumber['Cuisine'].toString(),
      "dietary": dietaryCon.text.isNotEmpty
          ? dietaryCon.text.toString()
          : widget.prodNumber['Dietary'].toString(),
      "spicy": spicyCon.text.isNotEmpty
          ? spicyCon.text.toString()
          : widget.prodNumber['Spicy'].toString(),
      "allergen": allergensCon.text.isNotEmpty
          ? allergensCon.text.toString()
          : widget.prodNumber['Allergen'].toString(),
      "marking": markingCon.text.isNotEmpty
          ? markingCon.text.toString()
          : widget.prodNumber['Marking'].toString(),
      "category": categoryCon.text.isNotEmpty
          ? categoryCon.text.toString()
          : widget.prodNumber['Category'].toString(),
      "preperationTime": preptimeCon.text.isNotEmpty
          ? preptimeCon.text.toString()
          : widget.prodNumber['PreperationTime'].toString(),
      "otherHighlight": highLightCon.text.isNotEmpty
          ? highLightCon.text.toString()
          : widget.prodNumber['OtherHighlight'].toString(),
      "dishImageUrl1": img1 != null
          ? await MultipartFile.fromFile(img1.path,
              filename: img1.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
      "dishImageUrl2": img2 != null
          ? await MultipartFile.fromFile(img2.path,
              filename: img2.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
      "dishImageUrl3": img3 != null
          ? await MultipartFile.fromFile(img3.path,
              filename: img3.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          // : widget.prodNumber['DishImageUrl3'].toString(),
          : null,
      "dishImageUrl4": img4 != null
          ? await MultipartFile.fromFile(img4.path,
              filename: img4.name,
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          // : widget.prodNumber['DishImageUrl4'].toString(),
          : null,
    });
    var isregis = await BlocProvider.of<MoreadditemBloc>(context)
      ..add(SelMoreItemBtnEvent(moreItemModel: moreItemM));
  }

  @override
  Widget build(BuildContext context) {
    final formvalid = Provider.of<SelValidation>(context);
    return Padding(
      padding: EdgeInsets.only(right: 5.0, left: 5.0, top: 8.0),
      child: Form(
        key: _form,
        child: Column(children: [
          /* -------------------------------------------------------------------------- */
          /*                                 FORM FIELD                                   */
          /* -------------------------------------------------------------------------- */

          SelFieldCard(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text('Serving available', style: labelTextStyle),
                    Padding(
                      padding: const EdgeInsets.only(left: 5, right: 3),
                      child: Container(
                        width: 90,
                        child: SellerEditFormFields(
                          placeholder: ' 25',
                          inputType: TextInputType.number,
                          controller: avaibleCon,
                          formValidator: (v) => formvalid.reqField(v),
                        ),
                      ),
                    )
                  ],
                ),

                // ! Order limit
                heightSizedBox(10.0),
                Row(
                  children: [
                    Text('Order Limit', style: labelTextStyle),
                    Padding(
                      padding: const EdgeInsets.only(left: 43, right: 3),
                      child: Container(
                        width: 90,
                        child: SellerEditFormFields(
                          placeholder: '   5',
                          inputType: TextInputType.number,
                          controller: limitCon,
                          formValidator: (v) => formvalid.reqField(v),
                        ),
                      ),
                    ),
                  ],
                ),

                // ! HighLiht THings
                heightSizedBox(10.0),
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: detailsCon == false
                      ? InkWell(
                          onTap: () {
                            setState(() {
                              detailsCon = true;
                            });
                          },
                          child: Row(children: [
                            Text('Edit more details   ', style: labelTextStyle),
                            Icon(Icons.arrow_drop_down_rounded,
                                color: redColor),
                          ]))
                      : selHideField(),
                ),
              ],
            ),
          ),
          Padding(
            // padding: EdgeInsets.only(left: 25.0, right: 25.0),
            padding: EdgeInsets.only(left: 11.0, right: 11.0),
            child: BigButton(
              title: 'Save',
              onTap: _sellerDetMet,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SelTCSTxt('By submitting you agree to our', fSize: 10.0),
              SelTCUTxt('Terms & Condtions', fSize: 10.0),
              SelTCSTxt('and', fSize: 10.0),
              SelTCUTxt('Privacy Statment', fSize: 10.0)
            ],
          )
        ]),
      ),
    );
  }

  Widget? selHideField() {
    if (widget.prodNumber['DishImageUrl1'].toString().isNotEmpty ||
        imgSrc1 != null) {
      setState(() {
        clBtn1 = true;
      });
    }
    if (widget.prodNumber['DishImageUrl2'].toString().isNotEmpty ||
        imgSrc2 != null) {
      setState(() {
        clBtn2 = true;
      });
    }
    if (widget.prodNumber['DishImageUrl3'].toString().isNotEmpty ||
        imgSrc3 != null) {
      setState(() {
        clBtn3 = true;
      });
    }
    if (widget.prodNumber['DishImageUrl4'].toString().isNotEmpty ||
        imgSrc4 != null) {
      setState(() {
        clBtn4 = true;
      });
    }
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Add Dish Images', style: labelTextStyle),
          heightSizedBox(3.0),
          SizedBox(
            height: 85,
            width: 500,
            child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: [
                SelUpdMultiFileNet(
                  networkImage: widget.prodNumber['DishImageUrl1'],
                  imgSrc: imgSrc1,
                  backColor: clBtn1 == false ? offWhiteColor : null,
                  upDFile: () async {
                    var image = await _picker.pickImage(
                      source: ImageSource.gallery,
                      imageQuality: 25,
                    );

                    setState(() {
                      img1 = image;
                      imgSrc1 = image!.path.toString();
                      mimeTypeData1 = lookupMimeType(imgSrc1)!.split('/');
                    });
                  },
                  cleanImg: () {
                    setState(() {
                      img1 = null;
                      imgSrc1 = null;
                      clBtn1 = false;
                    });
                  },
                ),
                SelUpdMultiFileNet(
                  networkImage: widget.prodNumber['DishImageUrl2'],
                  imgSrc: imgSrc2,
                  backColor: clBtn2 == false ? offWhiteColor : null,
                  upDFile: () async {
                    var image = await _picker.pickImage(
                      source: ImageSource.gallery,
                      imageQuality: 25,
                    );

                    setState(() {
                      img2 = image;
                      imgSrc2 = image!.path.toString();
                      mimeTypeData1 = lookupMimeType(imgSrc2)!.split('/');
                    });
                  },
                  cleanImg: () {
                    setState(() {
                      img2 = null;
                      imgSrc2 = null;
                      clBtn2 = false;
                    });
                  },
                ),
                SelUpdMultiFileNet(
                  networkImage: widget.prodNumber['DishImageUrl3'],
                  imgSrc: imgSrc3,
                  backColor: clBtn3 == false ? offWhiteColor : null,
                  upDFile: () async {
                    var image = await _picker.pickImage(
                      source: ImageSource.gallery,
                      imageQuality: 25,
                    );

                    setState(() {
                      img3 = image;
                      imgSrc3 = image!.path.toString();
                      mimeTypeData1 = lookupMimeType(imgSrc3)!.split('/');
                    });
                  },
                  cleanImg: () {
                    setState(() {
                      img3 = null;
                      imgSrc3 = null;
                      clBtn3 = false;
                    });
                  },
                ),
                SelUpdMultiFileNet(
                  backColor: clBtn4 == false ? offWhiteColor : null,
                  networkImage: widget.prodNumber['DishImageUrl4'],
                  imgSrc: imgSrc4,
                  upDFile: () async {
                    var image = await _picker.pickImage(
                      source: ImageSource.gallery,
                      imageQuality: 25,
                    );

                    setState(() {
                      img4 = image;
                      imgSrc4 = image!.path.toString();
                      mimeTypeData1 = lookupMimeType(imgSrc4)!.split('/');
                    });
                  },
                  cleanImg: () {
                    setState(() {
                      img4 = null;
                      imgSrc4 = null;
                      clBtn4 = false;
                    });
                  },
                ),
              ],
            ),
          ),

          // ! Field food

          heightSizedBox(10.0),
          SelOneField(
            left: 0,
            child: SellerEditFormFields(
                placeholder: 'Hydarbadi(indian)',
                inputType: TextInputType.name,
                labelText: 'Dish Name',
                initValue: widget.prodNumber['DishName'].toString().isNotEmpty
                    ? widget.prodNumber['DishName'].toString()
                    : '',
                onSaved: (String? val) {
                  setState(() {
                    dishNameCon.text = val!;
                  });
                }),
          ),

          heightSizedBox(10.0),
          SelOneField(
            left: 0,
            child: SellerEditFormFields(
                placeholder: 'this discription is',
                inputType: TextInputType.text,
                labelText: 'Dish Description',
                initValue:
                    widget.prodNumber['Description'].toString().isNotEmpty
                        ? widget.prodNumber['Description'].toString()
                        : '',
                inputFormatterData: [
                  LengthLimitingTextInputFormatter(180),
                ],
                onSaved: (String? val) {
                  setState(() {
                    descriptionCon.text = val!;
                  });
                }),
          ),

          // ! Row Filed for Price and Cusines
          heightSizedBox(10.0),
          Row(
            children: [
              SelTwoInOneField(
                  left: 0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: SellerEditFormFields(
                        inputType: TextInputType.number,
                        labelText: 'Price',
                        inputFormatterData: [
                          LengthLimitingTextInputFormatter(3),
                        ],
                        initValue: widget.prodNumber['Price']
                                .toString()
                                .isNotEmpty
                            ? widget.prodNumber['Price'].toString().substring(
                                0,
                                widget.prodNumber['Price'].toString().length -
                                    3)
                            : '',
                        onSaved: (String? val) {
                          setState(() {
                            priceCon.text = val!;
                          });
                        }),
                  )),
              SelTwoInOneField(
                left: 3,
                child: DropDownBtn(
                  labelText: 'Cuisines',
                  currentItem:
                      widget.prodNumber['Cuisine'].toString().isNotEmpty
                          ? widget.prodNumber['Cuisine'].toString()
                          : '',
                  dName: widget.prodNumber['Cuisine'].toString().isNotEmpty
                      ? widget.prodNumber['Cuisine'].toString()
                      : 'Chicken Briyani',
                  listData: cuisinesList,
                  listController: cuisinesCon,
                ),
              )
            ],
          ),

          // ! radio btn Dietary and spicy Level
          heightSizedBox(10.0),
          Row(
            children: [
              SelTwoInOneField(
                  left: 0,
                  child: DropDownBtn(
                    labelText: 'Dietary',
                    listData: dietaryList,
                    listController: dietaryCon,
                    currentItem:
                        widget.prodNumber['Dietary'].toString().isNotEmpty
                            ? widget.prodNumber['Dietary'].toString()
                            : '',
                    dName: widget.prodNumber['Dietary'].toString().isNotEmpty
                        ? widget.prodNumber['Dietary'].toString()
                        : 'Veg',
                  )),
              SelTwoInOneField(
                  left: 3,
                  child: DropDownBtn(
                    labelText: 'Spicy',
                    listData: spicyList,
                    listController: spicyCon,
                    currentItem:
                        widget.prodNumber['Spicy'].toString().isNotEmpty
                            ? widget.prodNumber['Spicy'].toString()
                            : '',
                    dName: widget.prodNumber['Spicy'].toString().isNotEmpty
                        ? widget.prodNumber['Spicy'].toString()
                        : 'Mild',
                  )),
            ],
          ),

          // ! allery and Making
          heightSizedBox(10.0),
          Row(
            children: [
              SelTwoInOneField(
                  left: 0,
                  child: DropDownBtn(
                    labelText: 'Allergens',
                    listData: allergensList,
                    listController: allergensCon,
                    currentItem:
                        widget.prodNumber['Allergen'].toString().isNotEmpty
                            ? widget.prodNumber['Allergen'].toString()
                            : '',
                    dName: widget.prodNumber['Allergen'].toString().isNotEmpty
                        ? widget.prodNumber['Allergen'].toString()
                        : 'Nuts',
                  )),
              SelTwoInOneField(
                  left: 3,
                  child: DropDownBtn(
                    labelText: 'Marking',
                    listData: markingList,
                    listController: markingCon,
                    dName: widget.prodNumber['Marking'].toString().isNotEmpty
                        ? widget.prodNumber['Marking'].toString()
                        : 'Halal',
                    currentItem:
                        widget.prodNumber['Marking'].toString().isNotEmpty
                            ? widget.prodNumber['Marking'].toString()
                            : '',
                  )),
            ],
          ),

          // ! Category and Perpartion Time
          heightSizedBox(10.0),
          Row(
            children: [
              SelTwoInOneField(
                  left: 0,
                  child: DropDownBtn(
                    labelText: 'Category',
                    listData: categoryList,
                    listController: categoryCon,
                    dName: widget.prodNumber['Category'].toString().isNotEmpty
                        ? widget.prodNumber['Category'].toString()
                        : 'Breakfast',
                    currentItem:
                        widget.prodNumber['Category'].toString().isNotEmpty
                            ? widget.prodNumber['Category'].toString()
                            : '',
                  )),
              SelTwoInOneField(
                left: 0,
                child: Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: SellerEditFormFields(
                      labelText: 'Perparation Time',
                      readOnly: true,
                      fieldAble: false,
                      timeName: 'DurField',
                      controller: preptimeCon,
                      onSaved: (String? val) {
                        setState(() {
                          preptimeCon.text = val!;
                        });
                      },
                      sufIcon: Icon(Icons.alarm, color: redColor)),
                ),
              )
            ],
          ),

          // ! HighLiht THings

          SelOneField(
            left: 0,
            child: SellerEditFormFields(
                placeholder: 'Any other things to highlight',
                inputType: TextInputType.text,
                labelText: 'Any other things to highlight',
                inputFormatterData: [
                  LengthLimitingTextInputFormatter(50),
                ],
                initValue:
                    widget.prodNumber['OtherHighlight'].toString().isNotEmpty
                        ? widget.prodNumber['OtherHighlight'].toString()
                        : '',
                onSaved: (String? val) {
                  setState(() {
                    highLightCon.text = val!;
                  });
                }),
          ),
        ],
      ),
    );
  }
}
