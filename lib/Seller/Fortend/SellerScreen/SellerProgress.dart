import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

class SellerProgressScr extends StatelessWidget {
  final int? sellerId;
  static const routeName = '/SellerProgressScr';
  SellerProgressScr({Key? key, this.sellerId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        boolLeading: false,
        title: 'Progress',
      ),
      // bottomNavigationBar: BottmNavBar(selectedTab: 2),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SellerCardItem(
                    title: 'Earnings',
                    price1: '\u{20B9}${2999.30}',
                    price2: '\u{20B9}${8000}',
                    time1: 'Febuary',
                    time2: 'AllTime',
                    linkp: 'SEE REVENUE REPORT'),
                SellerCardItem(
                    title: 'Orders',
                    price1: 21,
                    price2: 70,
                    time1: 'Febuary',
                    time2: 'AllTime',
                    linkp: 'SEE ORDERS REPORT'),
                SellerCardItem(
                    title: 'Ratings',
                    price1: 4.3,
                    price2: 4.5,
                    time1: 'Febuary',
                    time2: 'AllTime',
                    icon: true,
                    linkp: 'SEE REVIEWS'),
                SellerCardItem(
                    title: 'Followers',
                    price1: 12,
                    price2: 20,
                    time1: 'Febuary',
                    time2: 'AllTime',
                    linkp: 'SEE FOLLOWERS')
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// ! CARD ITEM FOR ITEM
class SellerCardItem extends StatelessWidget {
  final dynamic title;
  final dynamic price1;
  final dynamic price2;
  final dynamic time1;
  final dynamic time2;
  final dynamic linkp;
  dynamic icon = false;

  SellerCardItem(
      {Key? key,
      this.title,
      this.price1,
      this.price2,
      this.time1,
      this.time2,
      this.linkp,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
          child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            allTxt(title.toString(), siz: 18, col: redColor),
            // ! ROW FOR PRICE
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  icon == true
                      ? rowIcon(
                          txt: allTxt("${price1.toString()}",
                              siz: 15, col: blackColor))
                      : allTxt("${price1.toString()}",
                          siz: 15, col: blackColor),
                  icon == true
                      ? rowIcon(
                          txt: allTxt("${price2.toString()}",
                              siz: 15, col: blackColor))
                      : allTxt("${price2.toString()}",
                          siz: 15, col: blackColor),
                ],
              ),
            ),
            // ! ROW FOR TIME
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  allTxt(time1.toString(), siz: 13, col: Colors.brown),
                  allTxt(time2.toString(), siz: 13, col: Colors.brown)
                ],
              ),
            ),
            // ! LINK headLine
            InkWell(
              onTap: () {},
              child: allTxt(
                linkp != null ? linkp : '',
                siz: 13,
                txtd: TextDecoration.underline,
                col: textBlueColor,
              ),
            )
          ],
        ),
      )),
    );
  }

// ! row with icons
  Widget rowIcon({Widget? txt}) {
    return Row(
      children: [
        Icon(Icons.star, color: yellowColor),
        txt!,
      ],
    );
  }

  // ! all Text
  Widget allTxt(String p, {double? siz, Color? col, TextDecoration? txtd}) {
    return Text(p,
        style: TextStyle(
            fontSize: siz,
            fontWeight: FontWeight.bold,
            color: col,
            decoration: txtd));
  }
}
