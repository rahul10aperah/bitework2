import 'dart:convert';

import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Elements/dialogs.dart';
import 'package:comp1/Screens/seller_dashboard.dart';
import 'package:comp1/Seller/Backend/BlocPattern/PromoCode/selgenpromocode_bloc.dart';

import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/SellerFormField.dart';
import 'package:comp1/util/app_constants.dart';

import 'package:comp1/util/common.dart';
import 'package:comp1/util/http_services.dart';
import 'package:comp1/util/shared_helper.dart';

import 'package:comp1/util/style.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class SelGeneratePromoCodes extends StatefulWidget {
  final int? sellerId;
  SelGeneratePromoCodes({Key? key, this.sellerId}) : super(key: key);

  @override
  State<SelGeneratePromoCodes> createState() => _SelGeneratePromoCodesState();
}

class _SelGeneratePromoCodesState extends State<SelGeneratePromoCodes> {
  SelgenpromocodeBloc prodBloc = SelgenpromocodeBloc();

  @override
  void initState() {
    prodBloc = BlocProvider.of<SelgenpromocodeBloc>(context);
    prodBloc.add(SelPromoDetailsEvent(sellerId: widget.sellerId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: BaseAppBar(
          title: 'Generate Promo Code',
          boolLeading: false,
        ),
        body: BlocListener<SelgenpromocodeBloc, SelgenpromocodeState>(
            listener: (context, state) {
          // if (state is SelGeenreateErrorState) {
          //   snackBar(context, state.error.toString());
          // }
          if (state is SelApplyPromoCodeState) {
            // snackBar(context, state.data["message"].toString());
            navigationPush(context,
                SellerDashboard(sellerId: widget.sellerId, currentTab: 3));
          }
        }, child: BlocBuilder<SelgenpromocodeBloc, SelgenpromocodeState>(
                builder: (context, state) {
          if (state is SelPromoDetailsState) {
            return GeneratePromoCode2(
                sellerId: widget.sellerId, prodNumber: state.data);
          }
          if (state is SelGeneratePromoCodeState) {
            return GeneratePromoCode2(
                sellerId: widget.sellerId, prodNumber: state.data);
          }
          return Center(child: CircularProgressIndicator());
        })));
  }
}

class GeneratePromoCode2 extends StatefulWidget {
  final int? sellerId;
  dynamic prodNumber;
  GeneratePromoCode2({Key? key, this.sellerId, this.prodNumber})
      : super(key: key);

  @override
  _GeneratePromoCode2State createState() => _GeneratePromoCode2State();
}

class _GeneratePromoCode2State extends State<GeneratePromoCode2> {
  dynamic value1 = false;
  dynamic value2 = false;
  dynamic value3 = false;

  // final TextEditingController f1Controller = TextEditingController();

  List<PromoCodeModel> listSuggestedTags = [];

  TextEditingController myTextEditingController = TextEditingController();
  List<PromoCodeModel> listSearchedTags = [];
  String get getSearchedText => myTextEditingController.text.trim();

  @override
  void initState() {
    super.initState();
    if (widget.prodNumber["promoEnabled"] == true) {
      dishPromoResp();

      myTextEditingController.addListener(() => refreshState(() {}));
    }
    // myTextEditingController.addListener(() => refreshState(() {}));
  }

  refreshState(VoidCallback fn) {
    if (mounted) setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
    myTextEditingController.dispose();
  }

  // ! =============================================================
  Future dishPromoResp() async {
    Dio dio = Dio();
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));

    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      // 'Content-Type': 'application/json',
    };

    Map<String, dynamic> data = {"sellerId": widget.sellerId};

    try {
      var response = await dio.post(URLConstants.seldishPromoUrl,
          data: json.encode(data), options: Options(headers: headers));
      print('status code ${response.statusCode}');
      print('status code data ${response.data}');
      print('status code dishid ${response.data['dishes'].toString()}');
      print('status code length  ${response.data['dishes'].length}');
      if (response != null && response.data['success'] == 1) {
        List<dynamic> mat = response.data["dishes"];
        // print('runtype type ${response.data["dishes"].length}');
        // print('runtype type ${response.data["dishes"].runtimeType}');
        // print('runtype type Mat ${mat.length}');
        // print('runtype type Mat ${mat.runtimeType}');
        // for (var v in response.data["dishes"]) {
        for (var v in mat) {
          setState(() {
            listSuggestedTags.add(
              PromoCodeModel(id: v["DishId"], title: v["DishName"]),
            );
          });
        }
        return true;
        // }
      }

      // final response = await postDioRequestWithAllOptional(
      //     URLConstants.seldishPromoUrl,
      //     data: json.encode(data),
      //     newheaders: headers);

      // if (response != null && response['success'] == 1) {
      //   List<dynamic> mat = response["dishes"];
      //   print('runtype type ${response["dishes"].length}');
      //   print('runtype type ${response["dishes"].runtimeType}');
      //   print('runtype type Mat ${mat.length}');
      //   print('runtype type Mat ${mat.runtimeType}');
      //   for (var v in response["dishes"]) {
      //     for (var v in mat) {
      //       setState(() {
      //         listSuggestedTags.add(
      //           PromoCodeModel(id: v["DishId"], title: v["DishName"]),
      //         );
      //       });
      //     }
      //     return true;
      //   }
      // }
    } on DioError catch (e) {
      print('statuscode ${e.response!.statusCode}');
      print('data ${e.response!.data}');
      print('catch ${e.toString()}');

      //   // return false;
    }
  }
  // ! =============================================================

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      width: double.infinity,
      color: redColor,
      child: ListView(
        shrinkWrap: true, physics: ClampingScrollPhysics(),
        // mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: Container(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                RowItem(
                    title: 'Level : ', value: widget.prodNumber['curLevel']),
                RowItem(
                    title: 'Points : ',
                    value: widget.prodNumber['curPoints'] != null
                        ? widget.prodNumber['curPoints'].substring(
                            0,
                            widget.prodNumber['curPoints'].toString().length -
                                3)
                        : widget.prodNumber['curPoints'] == null
                            ? 0.toString()
                            : widget.prodNumber['curPoints'].toString()),
                RowItem(
                    title: 'Discount : ',
                    value: widget.prodNumber['discountPercentage'] == null
                        ? 0.toString() + '%'
                        : widget.prodNumber['discountPercentage'].toString() +
                            '%')
              ],
            )),
          ),
          // ! Text Heading
          Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: Column(children: [
              SelTxtField('Use below button ',
                  fontSize: 15.0, color: offWhiteColor),
              SelTxtField('  to generate Promo Code',
                  fontSize: 15.0, color: offWhiteColor),
            ]),
          ),
          //  ! GeneratePromoCode Sections
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: PromoCodeData(
                sellerId: widget.sellerId, prodNumber: widget.prodNumber),
          ),

          Padding(
            padding:
                const EdgeInsets.only(left: 0.0, right: 15, top: 5, bottom: 0),
            child: Container(
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: double.infinity,
                    height: 30,
                    child: checkBoxItem(
                        title: 'Apply promo code to below items only',
                        checkedValue: value1,
                        selected: value1,
                        func: (newValue) {
                          setState(() {
                            value1 = newValue;
                            value2 = false;
                          });
                        }),
                  ),
                  heightSizedBox(8.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15.0, right: 9),
                        child: Container(
                            alignment: Alignment.topLeft,
                            width: double.infinity,
                            child: listSearchedTags.length > 0
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                        Wrap(
                                          alignment: WrapAlignment.start,
                                          children: listSearchedTags
                                              .map((tagModel) =>
                                                  myTagsChipUIWidget(
                                                    tagModel: tagModel,
                                                    onTap: () =>
                                                        removePlayerTag(
                                                            tagModel),
                                                    action: "Remove",
                                                  ))
                                              .toSet()
                                              .toList(),
                                        ),
                                      ])
                                : Container()),
                      ),
                      heightSizedBox(8.0),
                      Padding(
                        padding: const EdgeInsets.only(left: 18.0, right: 9),
                        child: mySearchEditTextWidget(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15.0, right: 9),
                        child: noTagsTextWidget(),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          heightSizedBox(0.0),
          Padding(
            padding:
                const EdgeInsets.only(left: 0.0, right: 15, top: 5, bottom: 0),
            child: Container(
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: double.infinity,
                    height: 30,
                    child: checkBoxItem(
                        title: 'Apply promo code to all items',
                        checkedValue: value2,
                        func: (newValue) {
                          setState(() {
                            value2 = newValue;
                            value1 = false;
                          });
                        }),
                  ),
                ],
              ),
            ),
          ),

          // ! Apppy Button
          Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 90, right: 90),
            child: InkWell(
                onTap: () {
                  List<dynamic> dishList = [];
                  if (widget.prodNumber["promoEnabled"] == true) {
                    if (listSearchedTags.length > 0) {
                      for (var v in listSearchedTags) {
                        setState(() {
                       
                          dishList.add(v.id);
                        });
                      }
                    }
                    if (
                        dishList.length > 0 &&
                        value1 == true) {
                      BlocProvider.of<SelgenpromocodeBloc>(context)
                        ..add(SelApplyPromoCodeEvent(
                            sellerId: widget.sellerId,
                            dishId: dishList,
                            // dishId: [153, 154, 155],
                            allItems: false));
                    }
                    if (value2 == true && value1 == false) {
                      BlocProvider.of<SelgenpromocodeBloc>(context)
                        ..add(SelApplyPromoCodeEvent(
                            sellerId: widget.sellerId, allItems: true));
                    }
                  }
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: widget.prodNumber["promoEnabled"] == true
                          ? offWhiteColor
                          : Colors.grey,
                    ),
                    alignment: Alignment.center,
                    height: 30,
                    width: getWidth(context) / 5,
                    child: SelTxtField('Apply Promo Code'))),
          ),
          // ! Terms nad  poicy

          Padding(
            padding:
                const EdgeInsets.only(left: 1, right: 1, top: 2, bottom: 2),
            child: Container(
              alignment: Alignment.bottomCenter,
              width: double.infinity,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SelTCSTxt('See ', fSize: 14.0, fCol: offWhiteColor),
                  SelTCUTxt(' Terms & Condtions',
                      fSize: 14.0, fCol: offWhiteColor),
                  SelTCSTxt(' for additional information',
                      fSize: 14.0, fCol: offWhiteColor),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  // ! remove player Event
  removePlayerTag(tagModel) async {
    if (listSearchedTags.contains(tagModel)) {
      setState(() {
        listSearchedTags.remove(tagModel);
      });
    }
  }

  // ! add player Tags screen
  addPlayersTags(tagModel) async {
    if (!listSearchedTags.contains(tagModel))
      setState(() {
        listSearchedTags.add(tagModel);
      });
  }

  // ! Suggested text Uint8List
  Widget suggestionsTextWidget() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      if (listFilteredSearchResults().length != listSearchedTags.length)
        // Text("Suggestions", style: TextStyle(fontSize: 18)),
        Wrap(
          alignment: WrapAlignment.start,
          children: listFilteredSearchResults()
              .where((tagModel) => !listSearchedTags.contains(tagModel))
              .map((tagModel) => myTagsChipUIWidget(
                    tagModel: tagModel,
                    onTap: () => addPlayersTags(tagModel),
                    action: "add",
                  ))
              .toList(),
        ),
    ]);
  }

  // ! tags chip ui
  Widget myTagsChipUIWidget({
    tagModel,
    onTap,
    action,
  }) {
    return InkWell(
        onTap: onTap,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                vertical: 3.0,
                horizontal: 5.0,
              ),
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 5.0,
                  vertical: 3.0,
                ),
                decoration: BoxDecoration(
                  color: darkBlueColor,
                  borderRadius: BorderRadius.circular(100.0),
                ),
                child: Text(
                  "${tagModel.title}",
                  softWrap: true,
                  style: TextStyle(
                    color: offWhiteColor,
                    fontSize: 12,
                    fontFamily: montserratMedium,
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  // ! no tags
  noTagsTextWidget() {
    return Container(
        child: listFilteredSearchResults().isNotEmpty
            ? suggestionsTextWidget()
            : null);
  }

// ! FILTEER TAGS SelverifypEvent
  List<PromoCodeModel> listFilteredSearchResults() {
    if (getSearchedText.isEmpty) return listSuggestedTags;
    List<PromoCodeModel> listTempTags = [];
    for (int index = 0; index < listSuggestedTags.length; index++) {
      PromoCodeModel tagModel = listSuggestedTags[index];
      if (tagModel.title!
          .toLowerCase()
          .trim()
          .contains(getSearchedText.toLowerCase())) {
        listTempTags.add(tagModel);
      }
    }
    // print(' ======= listTempTags $listTempTags');
    return listTempTags;
  }

// ! cREATE SERACH vIEW
  Widget mySearchEditTextWidget() {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              autocorrect: true,
              textInputAction: TextInputAction.search,
              controller: myTextEditingController,
              style: TextStyle(
                fontSize: 15,
              ),
              decoration: InputDecoration(
                  hintText: myTextEditingController.text.isEmpty
                      ? 'Selected Dish Name'
                      : null,
                  counter: Offstage(),
                  hintStyle: TextStyle(
                      fontFamily: montserratMedium,
                      color: offWhiteColor,
                      fontSize: 14),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 5.0, horizontal: 0),
                  isDense: true,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(width: 1, color: offWhiteColor),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(width: 1, color: offWhiteColor),
                  ),
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(width: 1, color: offWhiteColor)),
                  focusColor: offWhiteColor,
                  hoverColor: offWhiteColor,
                  prefixIcon: myTextEditingController.text.isEmpty
                      ? Icon(
                          Icons.search,
                          color: offWhiteColor,
                        )
                      : null,
                  prefixIconConstraints:
                      BoxConstraints(minWidth: 0, minHeight: 0),
                  suffixIconConstraints:
                      BoxConstraints.tightFor(width: 35.0, height: 14.0)),
            ),
          ),
          // getSearchedText.isNotEmpty
          //     ? InkWell(
          //         child: Icon(
          //           Icons.clear,
          //           color: Colors.grey.shade700,
          //         ),
          //         onTap: () => myTextEditingController.clear(),
          //       )
          //     : Icon(
          //         Icons.search,
          //         color: Colors.grey.shade700,
          //       ),
          Container(),
        ],
      ),
    );
  }

  // ! CheckBox Item
  Widget checkBoxItem(
      {bool selected = false,
      String? title,
      dynamic func,
      dynamic onTap,
      bool? checkedValue}) {
    return ListTile(
      onTap: onTap,
      leading: Checkbox(
        value: checkedValue,
        onChanged: func,
      ),
      title: Transform.translate(
        offset: const Offset(-20, 0),
        child: SelTxtField(title,
            fontSize: 13,
            maxLines: 1,
            color: offWhiteColor,
            fontFamily: montserratMedium),
      ),
    );
  }
}

// ! Promo Code Button
class PromoCodeData extends StatelessWidget {
  final int? sellerId;
  dynamic prodNumber;
  PromoCodeData({Key? key, this.sellerId, this.prodNumber}) : super(key: key);
  Widget? dateField() {
    if (prodNumber["expiryDate"] != null) {
      var mat = DateFormat('dd-MM-yyyy').format(DateTime.parse(
        prodNumber["expiryDate"].toString(),
      ));
      return SelTxtField('Expiry : $mat', fontSize: 12.0, color: offWhiteColor);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: cartColor,
        height: getHeight(context) / 3.2,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // ! Text Heading
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  color: prodNumber["promoEnabled"] == true
                      ? bottomSheet
                      : Colors.grey,
                ),
                height: 30,
                width: 200,
                child: InkWell(
                    onTap: prodNumber["promoEnabled"] == true
                        ? () => generatePromoDialog(context, onTap: () {
                              if (prodNumber["promoEnabled"] == true) {
                                BlocProvider.of<SelgenpromocodeBloc>(context)
                                  ..add(SelGeneratePromoCodeEvent(
                                      sellerId: sellerId));
                                Navigator.pop(context);
                              }
                            })
                        : null,
                    child: SelTxtField('Generate Promo Code',
                        fontSize: 15.0, color: offWhiteColor)),
              ),
            ),

            // ! second boxShadow
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                alignment: Alignment.center,
                height: 120,
                width: 200,
                color: darkBlueColor,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SelTxtField(
                        prodNumber["buttonEnabled"] == false
                            ? prodNumber["promoCode"] == null
                                ? "No Promo Generated"
                                : prodNumber["promoCode"].toString()
                            : prodNumber["promoCode"] == null
                                ? "No Promo Generated"
                                : prodNumber["promoCode"].toString(),
                        fontSize: 18.0,
                        color: offWhiteColor),
                    Container(child: dateField())
                  ],
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 3.0, left: 6),
              child: SelTxtField(
                  prodNumber["buttonEnabled"] == false
                      ? prodNumber["message2"].toString()
                      : prodNumber["message1"].toString(),
                  fontSize: 12,
                  color: textBlueColor),
            ),
          ],
        ),
      ),
    );
  }
}

// ! Row Item for
class RowItem extends StatelessWidget {
  final String? title;
  final String? value;
  RowItem({Key? key, this.title, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SelTxtField(title,
            fontSize: 15, color: offWhiteColor, fontFamily: montserratMedium),
        SelTxtField(value, color: offWhiteColor, fontFamily: montserratMedium)
      ],
    );
  }
}

class PromoCodeModel {
  int? id;
  // String? id;
  String? title;

  PromoCodeModel({
    this.id,
    this.title,
  });
}
