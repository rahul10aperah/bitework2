import 'dart:io';

import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Screens/seller_dashboard.dart';
import 'package:comp1/Seller/Backend/BlocPattern/HomeB/selhomep_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/Verify/selverifyp_bloc.dart';
import 'package:comp1/Seller/Backend/SellerRespsitory/SelVerifyResp.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/SellerFormField.dart';

import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:comp1/util/style.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'SellerReg.dart';

// class SellerVerfiyPage extends StatefulWidget {
//   const SellerVerfiyPage({Key? key}) : super(key: key);

//   @override
//   State<SellerVerfiyPage> createState() => _SellerVerfiyPageState();
// }

// class _SellerVerfiyPageState extends State<SellerVerfiyPage> {
//   SelVerifyEntryRespo selVerify = SelVerifyEntryRespo();

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//         future: selVerify.selVerfiySellerEntry(),
//         builder: (context, state) {
//           if (state.hasData && state.data != null) {
//             var dat = (state.data! as dynamic)['sellerStatus'];
//             // print(' selVrify ${state.data}');

//             var sellerId = (state.data! as dynamic)['sellerId'];
//             print(' seller Id $sellerId');
//             if (dat == 1) {
//               return SelVerifyCard(
//                   txtData:
//                       ' Registration is Pending Approval. \n You Will be Contacted Shortly.');
//             }
//             if (dat == 2) {
//               // var mat = BlocProvider.of<SelverifypBloc>(context)
//               //   ..add(SelChangeScreenEvent(screen: 2));

//               return FutureBuilder(
//                   future: selVerify.selCheckKitechenImage(),
//                   builder: (context, state) {
//                     if (state.hasData && state.data != null) {
//                       var datr = (state.data! as dynamic)['success'];
//                       // print('check kitehc image $datr');
//                       if (datr == 1) {
//                         return BlocProvider(
//                           create: (ctx) => SelhomepBloc(sellerId: sellerId),
//                           child: SellerDashboard(
//                               currentTab: 0, sellerId: sellerId),
//                         );
//                       } else {
//                         return SelVerifyDialogBox(sellerId: sellerId);
//                       }
//                     }

//                     return Scaffold(
//                         body: Center(child: CircularProgressIndicator()));
//                   });
//             }
//             if (dat == 3) {
//               return SelVerifyCard(
//                   txtData:
//                       'Your Registration is Rejected. \n Please Contact Customer Support.');
//             }
//             if (dat == 4) {
//               return SellerRegisterScr();
//             }
//           }
//           return Scaffold(body: Center(child: CircularProgressIndicator()));
//         });
//   }
// }

// ! Seller verify fROM Profile page Cusines
class SellerVerifyFromProfile extends StatefulWidget {
  SellerVerifyFromProfile({Key? key}) : super(key: key);

  @override
  _SellerVerifyFromProfileState createState() =>
      _SellerVerifyFromProfileState();
}

class _SellerVerifyFromProfileState extends State<SellerVerifyFromProfile> {
  SelVerifyEntryRespo selVerify = SelVerifyEntryRespo();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: selVerify.selVerfiySellerEntry(),
        builder: (context, state) {
          if (state.hasData && state.data != null) {
            var dat = (state.data! as dynamic)['sellerStatus'];

            var sellerId = (state.data! as dynamic)['sellerId'];
            //  ! SELLLER VERFIY CODE
            if (dat == 1) {
              return SelVerifyCard(
                  txtData:
                      ' Registration is Pending Approval. \n You Will be Contacted Shortly.');
            }
            if (dat == 2) {
              var mat = BlocProvider.of<SelverifypBloc>(context)
                ..add(SelChangeScreenEvent(screen: 2));

              //  ! SELLLER KITCHEN NAME AND KITCHEN IMAGE CHECK CODE
              return FutureBuilder(
                  future: selVerify.selCheckKitechenImage(),
                  builder: (context, state) {
                    if (state.hasData && state.data != null) {
                      var datr = (state.data! as dynamic)['success'];
                      //  ! SELLLER DATE AvailablILTY CHECK CODE
                      if (datr == 1) {
                        return SelCheckAviableData(
                          sellerId: sellerId,
                        );
                      } else {
                        return SelVerifyDialogBox(sellerId: sellerId);
                      }
                    }

                    return Scaffold(
                        body: Center(child: CircularProgressIndicator()));
                  });
            }
            if (dat == 3) {
              return SelVerifyCard(
                  txtData:
                      'Your Registration is Rejected. \n Please Contact Customer Support.');
            }
            if (dat == 4) {
              return SellerRegisterScr();
            }
          }
          return Scaffold(body: Center(child: CircularProgressIndicator()));
        });
  }
}

// ! Home SelVeify alert Model
class SelVerifyDialogBox extends StatefulWidget {
  dynamic sellerId;

  SelVerifyDialogBox({
    Key? key,
    this.sellerId,
  }) : super(key: key);

  @override
  _SelVerifyDialogBoxState createState() => _SelVerifyDialogBoxState();
}

class _SelVerifyDialogBoxState extends State<SelVerifyDialogBox> {
  final fnameController = TextEditingController();
  String? filename1;
  dynamic img1;
  final formKey = GlobalKey<FormState>();
  final ImagePicker picker = ImagePicker();
  // ! rister method
  _registerNow() async {
    var isvalid = formKey.currentState!.validate();
    if (!isvalid) {
      return 'Please Enter Valid Data';
    }
    formKey.currentState!.save();

    var isregis = BlocProvider.of<SelverifypBloc>(
      context,
    )..add(SelAddKitchenImageEvent(
        kitchenName: fnameController.text, kitchenImage: img1));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: offWhiteColor,
      appBar: BaseAppBar(
        title: "Kitchen Details",
        boolLeading: false,
      ),
      body: BlocListener<SelverifypBloc, SelverifypState>(
          listener: (context, state) {
            if (state is SelAddKitchenImageState) {
              navigationPush(
                  context,
                  SelCheckAviableData(
                    sellerId: widget.sellerId,
                  ));
            }
          },
          child: Form(
            key: formKey,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                  child: Container(
                    decoration: boxDecoration,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Kitchen Image ",
                          style: labelTextStyle,
                        ),
                        heightSizedBox(20.0),
                        imgField(
                            cleanImg: () {
                              setState(() {
                                img1 = null;
                                filename1 = null;
                              });
                            },
                            imgSrc: filename1,
                            upDFile: () async {
                              var image = await FilePicker.platform.pickFiles(
                                  type: FileType.custom,
                                  allowedExtensions: ['pdf', 'jpeg', 'png']);

                              setState(() {
                                img1 = image;
                                filename1 = image?.files.first.path.toString();

                                // print(image1!.path);
                              });
                            }),
                        heightSizedBox(10.0),
                        Container(
                          child: Row(
                            children: [
                              Text('Kitchen Name', style: labelTextStyle),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 5.0, right: 5),
                                child: Container(
                                  width: getWidth(context) / 2.15,
                                  child: SellerEditFormFields(
                                    controller: fnameController,
                                    inputType: TextInputType.name,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        heightSizedBox(20.0),
                        BigButton(
                          title: 'Submit',
                          onTap: _registerNow,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }

  // ! Image Field
  Widget imgField({
    Function()? upDFile,
    Function()? cleanImg,
    String? imgSrc,
  }) {
    return Padding(
        padding: const EdgeInsets.all(1.0),
        child: Column(
          children: [
            // ! image conatiner
            Container(
              padding: const EdgeInsets.all(1.0),
              margin: EdgeInsets.all(1.0),
              height: getHeight(context) / 6,
              width: getWidth(context) / 1.2,
              decoration: BoxDecoration(
                image: imgSrc != null
                    ? DecorationImage(
                        image: FileImage(
                            imgSrc != null ? File(imgSrc as String) : File('')),
                        fit: BoxFit.fitWidth,
                      )
                    : null,
                // ! Cricle Border
                borderRadius: BorderRadius.circular(5),
              ),
              child: MaterialButton(
                color: imgSrc == null ? offWhiteColor : null,
                onPressed: upDFile,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Ink(
                  decoration: BoxDecoration(
                    // ! Cricle Border
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Container(
                    constraints: BoxConstraints(
                        maxWidth: 20.0, minHeight: 45.0, maxHeight: 45.0),
                    alignment: Alignment.center,
                    child: Center(
                      child: Container(
                          height: 20,
                          decoration: BoxDecoration(
                            border: imgSrc == null
                                ? Border.all(color: redColor)
                                : null,
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            color: imgSrc == null ? redColor : null,
                          ),
                          child: imgSrc == null || imgSrc.toString().isEmpty
                              ? Center(
                                  child: Icon(
                                    Icons.add,
                                    size: 16,
                                    color: offWhiteColor,
                                    // textDirection: TextDirection.ltr,
                                  ),
                                )
                              : null),
                    ),
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(),
              child: Container(
                  child: imgSrc == null || imgSrc.isEmpty
                      ? null
                      : InkWell(
                          onTap: cleanImg,
                          child: SelIcon(Icons.cancel,
                              size: 15, color: redColor))),
            )
          ],
        ));
  }
}

// ! SELLER CHECK AvailablILTY CODES
class SelCheckAviableData extends StatelessWidget {
  final int? sellerId;
  SelCheckAviableData({Key? key, this.sellerId}) : super(key: key);
  SelVerifyEntryRespo selVerify = SelVerifyEntryRespo();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: selVerify.selCheckAvailability(sellerId: sellerId),
        builder: (context, state) {
          if (state.hasData && state.data != null) {
            var datr = (state.data! as dynamic)['success'];

            if (datr == 1) {
              return SellerDashboard(currentTab: 0, sellerId: sellerId);
            } else {
              return SelCheckAvailabilityScreen(
                  sellerId: sellerId, prodNumber: state.data);
            }
          }

          return Scaffold(body: Center(child: CircularProgressIndicator()));
        });
  }
}

class SelCheckAvailabilityScreen extends StatefulWidget {
  final int? sellerId;
  dynamic prodNumber;

  SelCheckAvailabilityScreen({Key? key, this.sellerId, this.prodNumber})
      : super(key: key);

  @override
  _SelCheckAvailabilityScreenState createState() =>
      _SelCheckAvailabilityScreenState();
}

class _SelCheckAvailabilityScreenState
    extends State<SelCheckAvailabilityScreen> {
  final date1Con = TextEditingController();
  final date2Con = TextEditingController();
  final time1Con = TextEditingController();
  final time2Con = TextEditingController();
  dynamic servingDateFrom;
  dynamic servingDateTo;
  dynamic servingTimeFrom;
  dynamic servingTimeTo;
  final formKey = GlobalKey<FormState>();

  // ! rister method
  _registerNow() async {
    var isvalid = formKey.currentState!.validate();
    if (!isvalid) {
      return 'Please Enter Valid Data';
    }
    formKey.currentState!.save();

    var isregis = BlocProvider.of<SelverifypBloc>(
      context,
    )..add(SelCheckAvailabilityEvent(
        sellerId: widget.sellerId,
        servingDateFrom: servingDateFrom,
        servingDateTo: servingDateTo,
        servingTimeFrom: servingTimeFrom,
        servingTimeTo: servingTimeTo,
      ));

    print('sellerId is ${widget.sellerId}');
    print('dateFrom is $servingDateFrom');
    print('dateTo is $servingDateTo');
    print('TimeFrom is $servingTimeFrom');
    print('TimeTo is $servingTimeTo');
  }

  // ! null data Field
  dynamic res1;

  @override
  Widget build(BuildContext context) {
    final formvalid = Provider.of<SelValidation>(context);
    return Scaffold(
      backgroundColor: offWhiteColor,
      appBar: BaseAppBar(
        title: " Add Availability",
        boolLeading: false,
      ),
      body: BlocConsumer<SelverifypBloc, SelverifypState>(
          listener: (context, state) {
        print('this is state ==== $state');
        if (state is SelCheckAvabilityState) {
          navigationPush(context,
              SellerDashboard(currentTab: 0, sellerId: widget.sellerId));
        }
        if (state is SelVerifyErrorState) {
          snackBar(context, state.error.toString());
        }
      }, builder: (context, state) {
        return Container(
            child: Form(
          key: formKey,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 30),
                child: Container(
                  decoration: boxDecoration,
                  padding: EdgeInsets.symmetric(horizontal: 3, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            'Select Date',
                            style: labelTextStyle,
                          ),
                          SelTwoInOneField(
                            child: SellerEditFormFields(
                              labelText: '   From',
                              placeholder: '4-May',
                              controller: date1Con,
                              readOnly: true,
                              fieldAble: false,
                              formValidator: (v) => formvalid.reqField(v),
                              sufIcon: Icon(Icons.calendar_today,
                                  size: 15, color: redColor),
                              onTap: () => pickDateRange(context),
                            ),
                          ),
                          Text(
                            'To',
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: montserratMedium,
                                fontWeight: FontWeight.bold),
                          ),
                          SelTwoInOneField(
                            child: SellerEditFormFields(
                              labelText: '   To',
                              placeholder: '8-May',
                              // inputType: TextInputType.datetime,
                              controller: date2Con,
                              // timeName: 'CalendarFieldFrom',
                              readOnly: true,
                              fieldAble: false,
                              // onTap: _selectDateFrom,
                              onTap: () => pickDateRange(context),
                              formValidator: (v) => formvalid.reqField(v),
                              sufIcon: Icon(Icons.calendar_today,
                                  size: 15, color: redColor),
                            ),
                          ),
                        ],
                      ),

                      // ! SERVING TIMING
                      heightSizedBox(10.0),
                      Row(
                        children: [
                          Text(
                            'Select Timing',
                            style: labelTextStyle,
                          ),
                          SelTwoInOneField(
                            child: SellerEditFormFields(
                              placeholder: '12 AM',
                              // inputType: TextInputType.number,
                              controller: time1Con,
                              // timeName: 'DateField',
                              onTap: () {
                                _choseTime(timeName: 'servingTimeFrom');
                              },
                              readOnly: true,
                              fieldAble: false,
                              formValidator: (v) => formvalid.reqField(v),
                              sufIcon:
                                  Icon(Icons.alarm, size: 15, color: redColor),
                            ),
                          ),
                          Text(
                            'To',
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: montserratMedium,
                                fontWeight: FontWeight.bold),
                          ),
                          SelTwoInOneField(
                            child: SellerEditFormFields(
                              placeholder: '   2 PM',
                              // inputType: TextInputType.number,
                              controller: time2Con,
                              // timeName: 'DateField',
                              onTap: () {
                                _choseTime(timeName: 'servingTimeTo');
                              },
                              readOnly: true,
                              fieldAble: false,
                              formValidator: (v) => formvalid.reqField(v),
                              sufIcon:
                                  Icon(Icons.alarm, size: 15, color: redColor),
                            ),
                          ),
                        ],
                      ),
                      heightSizedBox(20.0),
                      BigButton(
                        title: 'Submit',
                        onTap: _registerNow,
                      ),

                      Padding(
                        padding: const EdgeInsets.only(
                            left: 1, right: 1, top: 2, bottom: 2),
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          width: double.infinity,
                          height: 40,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SelTCSTxt('See ',
                                  fSize: 14.0, fCol: offWhiteColor),
                              SelTCUTxt(' Terms & Condtions',
                                  fSize: 14.0, fCol: offWhiteColor),
                              SelTCSTxt(' for additional information',
                                  fSize: 14.0, fCol: offWhiteColor),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
      }),
    );
  }

  Future pickDateRange(BuildContext context) async {
    // final initialDateRange = DateTimeRange(
    //   start: DateTime.now(),
    //   end: DateTime.now().add(Duration(hours: 24 * 3)),
    // );
    final newDateRange = await showDateRangePicker(
      context: context,
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(Duration(
          days: widget.prodNumber["servingDateLimit"] != null
              ? widget.prodNumber["servingDateLimit"]
              : 14)),
      // initialDateRange: dateRange
      // ?? initialDateRange,
    );

    if (newDateRange == null) return;

    print('NewdateRange $newDateRange');
    print('--------------------------------');
    if (newDateRange != null) {
      setState(() {
        // dateRange = newDateRange;
        var mon = DateFormat('dd/MMM/yy').format(newDateRange.start);

        date1Con.text = DateFormat('dd-MMM').format(newDateRange.start);
        date2Con.text = DateFormat('dd-MMM').format(newDateRange.end);
        servingDateTo = DateFormat('yyyy-MM-dd').format(newDateRange.end);
        servingDateFrom = DateFormat('yyyy-MM-dd').format(newDateRange.start);
        // print('dateRange $dateRange');

        // print('NewdateRange ${newDateRange.start}, ${newDateRange.end}');
      });
    }
  }

  // ! Choose Timig Field
  Future _choseTime({String? timeName}) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: new TimeOfDay.now(),
      initialEntryMode: TimePickerEntryMode.input,
    );
    if (picked != null)
      setState(() {
        var timeSlot = picked.period.toString().split('.');

        var dat =
            "${picked.hourOfPeriod}: ${picked.minute.toString().length < 2 ? 0.toString() + picked.minute.toString() : picked.minute.toString()} ${timeSlot.last}";

        // widget.controller!.text = dat;
        if (timeName == 'servingTimeFrom') {
          time1Con.text = dat;
          servingTimeFrom = "${picked.hour}:${picked.minute}:00";
        }
        if (timeName == 'servingTimeTo') {
          time2Con.text = dat;
          servingTimeTo = "${picked.hour}:${picked.minute}:00";
          // print('timeTo $servingTimeTo');
          // print('timeFrom $servingTimeFrom');
        }
      });
  }
}

// ! SELLER FIELDS FOR DATA
class SelVerifyCard extends StatelessWidget {
  final String? txtData;
  final String? appTxt;
  dynamic onTap;
  SelVerifyCard({Key? key, this.txtData, this.appTxt, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: offWhiteColor,
        appBar: BaseAppBar(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 100),
              child: Container(
                decoration: boxDecoration,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    heightSizedBox(20.0),
                    Text(
                      txtData.toString(),
                      style: smallTextStyle,
                    ),
                    heightSizedBox(20.0),
                    BigButton(
                      title: 'OK',
                      onTap:
                          onTap != null ? onTap : () => Navigator.pop(context),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
