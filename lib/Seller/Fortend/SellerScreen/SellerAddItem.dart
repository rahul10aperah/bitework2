import 'package:comp1/Elements/base_appbar.dart';
import 'package:comp1/Screens/seller_dashboard.dart';

import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/util/http_services.dart';

import 'package:dio/dio.dart';

import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Seller/Backend/BlocPattern/DishAdd/dishadd_bloc.dart';

import 'package:comp1/Seller/Fortend/SellerWidget/SellerFormField.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/Selleruplod.dart';
import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:comp1/util/style.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';

class SellerAddItem extends StatelessWidget {
  dynamic sellerId;
  static const routeName = '/SellerAddItem';
  SellerAddItem({Key? key, this.sellerId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Add Dish',
      ),
      // AppBar(
      //   backgroundColor: Colors.red,
      //   title: Text('Add Dish', style: appBarTS),
      // ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: BlocConsumer<DishaddBloc, DishaddState>(
                listener: (context, state) {
              if (state is DishAddedSuccessState) {
                navigationPush(
                    context,
                    SellerDashboard(
                      sellerId: sellerId,
                      currentTab: 0,
                    ));
              }
            }, builder: (context, state) {
              if (state is DishAddedErrorState) {
                // snackBar(context, state.error.toString());
              }

              return SellerAddItemForm();
            }),
            // child: Column(
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //     //  ! REGISTER PAGE

            //     DishAddedOne(sellerId: sellerId)
            //   ],
            // ),
          ),
        ),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                             // ! Food Item form                            */
/* -------------------------------------------------------------------------- */
class SellerAddItemForm extends StatefulWidget {
  SellerAddItemForm({
    Key? key,
  }) : super(key: key);

  @override
  _SellerAddItemFormState createState() => _SellerAddItemFormState();
}

class _SellerAddItemFormState extends State<SellerAddItemForm> {
  final _form = GlobalKey<FormState>();
  final dishNameCon = TextEditingController();
  final descriptionCon = TextEditingController();
  final priceCon = TextEditingController();
  final cuisinesCon = TextEditingController();
  final dietaryCon = TextEditingController();
  final spicyCon = TextEditingController();
  final allergensCon = TextEditingController();
  final markingCon = TextEditingController();
  final categoryCon = TextEditingController();
  final preptimeCon = TextEditingController();
  final highLightCon = TextEditingController();
  final disPertCon = TextEditingController();
  final disPriceCon = TextEditingController();
  dynamic img1;
  dynamic img2;
  dynamic img3;
  dynamic img4;
  dynamic mimeTypeData1;
  final ImagePicker _picker = ImagePicker();

  // ! check imgSrc
  dynamic imgSrc1;
  dynamic imgSrc2;
  dynamic imgSrc3;
  dynamic imgSrc4;
  dynamic mimeTypeData;
  List<dynamic> allergensList = [
    // 'Nuts', 'Mat'
  ];

  List<dynamic> markingList = [
    // 'Halaal', 'Kashmiri'
  ];

  List cuisinesList = [];

  List<dynamic> spicyList = [
    // 'Mild',
    // 'Medium',
    // 'Hot',
  ];
  List<dynamic> categoryList = [
    // 'Breakfast', 'Lunch', 'Dinner'
  ];
  List<dynamic> dietaryList = [
    // 'Vegan',
    // 'Veg',
    // 'NonVeg',
  ];
  List<dynamic> prepartionTimeList = [
    // '3', '2', '1'
  ];

  Future apiData() async {
    final response = await getDioRequest(
      URLConstants.selDishFliterField,
    );

    var datar = response;

    if (response != null && response['success'] == 1) {
      for (var v in datar['allergenType']) {
        setState(() {
          allergensList.add(v['Name']);
        });
        // print(allergensList);
      }

      for (var v in datar['markingType']) {
        setState(() {
          markingList.add(v['Name']);
        });
        // print(markingList);
      }

      for (var v in datar['cuisineType']) {
        setState(() {
          cuisinesList.add(v['Name']);
        });
        // print(cuisinesList);
      }

      for (var v in datar['spicyType']) {
        setState(() {
          spicyList.add(v['Name']);
        });
        // print(spicyList);
      }

      for (var v in datar['dieteryType']) {
        setState(() {
          dietaryList.add(v['Name']);
        });
        // print(dietaryList);
      }

      for (var v in datar['categoryType']) {
        setState(() {
          categoryList.add(v['Name']);
        });
        // print(categoryList);
      }
      return true;
    } else {
      // if (response['message'] != null) return response;
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    apiData();
  }
  /* -------------------------------------------------------------------------- */
  /*                      //  !SUBMIT MEHTOD FOR VALIDATION                     */
  /* -------------------------------------------------------------------------- */

  // ignore: unused_element
  _sellerItemMetod() async {
    var isvalid = _form.currentState!.validate();
    if (!isvalid) {
      return 'Please Enter Valid Data';
    }
    _form.currentState!.save();

    FormData selItemM = FormData.fromMap({
      "dishName": dishNameCon.text,
      "description": descriptionCon.text,
      "price": priceCon.text,
      "discountPercentage": '',
      "discountPrice": '',
      "cuisine": cuisinesCon.text,
      "dietary": dietaryCon.text,
      "spicy": spicyCon.text,
      "allergen": allergensCon.text,
      "marking": markingCon.text,
      "category": categoryCon.text,
      "preperationTime": preptimeCon.text,
      "otherHighlight": highLightCon.text,
      "dishImageUrl1": img1 != null
          ? await MultipartFile.fromFile(img1.path,
              filename: img1.name,
              // contentType: new MediaType('image', 'jpeg')
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
      // "type": "application/pdf",
      "dishImageUrl2": img2 != null
          ? await MultipartFile.fromFile(img2.path,
              filename: img2.name,
              // contentType: new MediaType('image', 'jpeg')
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
      "dishImageUrl3": img3 != null
          ? await MultipartFile.fromFile(img3.path,
              filename: img3.name,
              // contentType: new MediaType('image', 'jpeg')
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
      "dishImageUrl4": img4 != null
          ? await MultipartFile.fromFile(img4.path,
              filename: img4.name,
              // contentType: new MediaType('image', 'jpeg')
              contentType: new MediaType(mimeTypeData1[0], mimeTypeData1[1]))
          : null,
    });

    var isregis = await BlocProvider.of<DishaddBloc>(context)
      ..add(DishAddedBtnEvent(addItemModel: selItemM));
  }

  @override
  Widget build(BuildContext context) {
    //  ! FORM VALIDATION PROVIDER

    final formvalid = Provider.of<SelValidation>(context);

    return Padding(
      padding: EdgeInsets.only(right: 5.0, left: 5.0, top: 5.0),
      child: Form(
        key: _form,
        // autovalidateMode: AutovalidateMode.onUserInteraction,
        child: ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: [
              SelFieldCard(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(9, 9, 15, 9),
                      child: Text('Add Dish Images', style: labelTextStyle),
                    ),
                    SizedBox(
                      height: 83,
                      width: 500,
                      child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: [
                          SelUpdMultiFile(
                              cleanImg: () {
                                setState(() {
                                  img1 = null;
                                  imgSrc1 = null;
                                });
                              },
                              imgSrc: imgSrc1,
                              upDFile: () async {
                                var image = await _picker.pickImage(
                                  source: ImageSource.gallery,
                                  imageQuality: 25,
                                );

                                setState(() {
                                  img1 = image;
                                  imgSrc1 = image!.path.toString();
                                  mimeTypeData1 =
                                      lookupMimeType(imgSrc1)!.split('/');
                                });
                              }),
                          SelUpdMultiFile(
                              cleanImg: () {
                                setState(() {
                                  img2 = null;
                                  imgSrc2 = null;
                                });
                              },
                              imgSrc: imgSrc2,
                              upDFile: () async {
                                var image = await _picker.pickImage(
                                  source: ImageSource.gallery,
                                  imageQuality: 25,
                                );

                                setState(() {
                                  img2 = image;
                                  imgSrc2 = image!.path.toString();
                                  mimeTypeData1 =
                                      lookupMimeType(imgSrc2)!.split('/');
                                });
                              }),
                          SelUpdMultiFile(
                              cleanImg: () {
                                setState(() {
                                  img3 = null;
                                  imgSrc3 = null;
                                });
                              },
                              imgSrc: imgSrc3,
                              upDFile: () async {
                                var image = await _picker.pickImage(
                                  source: ImageSource.gallery,
                                  imageQuality: 25,
                                );

                                setState(() {
                                  img3 = image;
                                  imgSrc3 = image!.path.toString();
                                  mimeTypeData1 =
                                      lookupMimeType(imgSrc3)!.split('/');
                                });
                              }),
                          SelUpdMultiFile(
                              cleanImg: () {
                                setState(() {
                                  img4 = null;
                                  imgSrc4 = null;
                                });
                              },
                              imgSrc: imgSrc4,
                              upDFile: () async {
                                var image = await _picker.pickImage(
                                  source: ImageSource.gallery,
                                  imageQuality: 100,
                                );

                                setState(() {
                                  img4 = image;
                                  imgSrc4 = image!.path.toString();
                                  mimeTypeData1 =
                                      lookupMimeType(imgSrc4)!.split('/');
                                });
                              }),
                        ],
                      ),
                    ),
                    // ! Field food

                    heightSizedBox(10.0),

                    SelOneField(
                      child: SellerEditFormFields(
                        placeholder: 'Hydarbadi(indian)',
                        inputType: TextInputType.name,
                        controller: dishNameCon,
                        labelText: 'Dish Name',
                        formValidator: (v) => formvalid.reqField(v),
                      ),
                    ),

                    heightSizedBox(10.0),
                    SelOneField(
                      child: SellerEditFormFields(
                        placeholder: 'this discription is',
                        inputType: TextInputType.multiline,
                        controller: descriptionCon,
                        labelText: 'Dish Description',
                        maxLines: 3,
                        minLines: 1,
                        formValidator: (v) => formvalid.selDescField(v),
                        inputFormatterData: [
                          LengthLimitingTextInputFormatter(85),
                        ],
                      ),
                    ),

                    // ! Row Filed for Price and Cusines
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            child: Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: SellerEditFormFields(
                            controller: priceCon,
                            inputType: TextInputType.number,
                            formValidator: (v) => formvalid.reqField(v),
                            // placeholder: '80',
                            labelText: 'Price',
                            inputFormatterData: [
                              LengthLimitingTextInputFormatter(3),
                            ],
                          ),
                        )),
                        SelTwoInOneField(
                            child: DropDownBtn(
                          labelText: 'Cuisines',
                          formValidator: (val) => formvalid.reqField(val),
                          // pageName: 'Chicken Briyani',
                          listData: cuisinesList,
                          listController: cuisinesCon,
                        )),
                      ],
                    ),

                    // // ! discount price and perctnage
                    // heightSizedBox(10.0),
                    // Row(
                    //   children: [
                    //     SelTwoInOneField(
                    //         child: SellerEditFormFields(
                    //       // validator: validateField,

                    //       controller: disPriceCon,
                    //       inputType: TextInputType.number,
                    //       formValidator: (v) => formvalid.reqField(v),
                    //       placeholder: '500',
                    //       labelText: 'DiscountPrice',
                    //     )),
                    //     SelTwoInOneField(
                    //         child: SellerEditFormFields(
                    //       // validator: validateField,
                    //       controller: disPertCon,
                    //       formValidator: (v) => formvalid.reqField(v),
                    //       inputType: TextInputType.number,
                    //       placeholder: '5',
                    //       labelText: 'DiscountPercentage',
                    //     )),
                    //   ],
                    // ),

                    // ! radio btn Dietary and spicy Level
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            child: DropDownBtn(
                          labelText: 'Dietary',
                          formValidator: (val) => formvalid.reqField(val),
                          // pageName: 'Veg',
                          listData: dietaryList,
                          listController: dietaryCon,
                        )),
                        SelTwoInOneField(
                            child: DropDownBtn(
                          labelText: 'Spicy',
                          formValidator: (val) => formvalid.reqField(val),
                          // pageName: 'Mild',
                          listData: spicyList,
                          listController: spicyCon,
                        )),
                      ],
                    ),

                    // DayWorkRaioBtn(
                    //     labelText: 'Dietary',
                    //     t1: 'Veg',
                    //     t2: 'Non-Veg',
                    //     t3: 'Vegan',
                    //     val1: 'Veg',
                    //     val2: 'Non-Veg',
                    //     val3: 'Vegan',
                    //     gVal: dietValue,
                    //     onChg: (dynamic ind) {
                    //       setState(() {
                    //         dietValue = ind;
                    //       });
                    //     }),

                    //  ! spicy Level

                    // DayWorkRaioBtn(
                    //     labelText: 'Spicy Level',
                    //     t1: 'Mild',
                    //     t2: 'Medium',
                    //     t3: 'Hot',
                    //     val1: 'Mild',
                    //     val2: 'Medium',
                    //     val3: 'Hot',
                    //     gVal: spicyValue,
                    //     onChg: (dynamic ind) {
                    //       setState(() {
                    //         spicyValue = ind;
                    //       });
                    //     }),

                    // ! allery and Making
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            child: DropDownBtn(
                          labelText: 'Allergens',
                          formValidator: (val) => formvalid.reqField(val),
                          // pageName: 'Nuts',
                          listData: allergensList,
                          listController: allergensCon,
                        )),
                        SelTwoInOneField(
                            child: DropDownBtn(
                          labelText: 'Marking',
                          formValidator: (val) => formvalid.reqField(val),
                          // pageName: 'Halaal',
                          listData: markingList,
                          listController: markingCon,
                        )),
                      ],
                    ),

                    // ! Category and Perpartion Time
                    heightSizedBox(10.0),
                    Row(
                      children: [
                        SelTwoInOneField(
                            child: DropDownBtn(
                          labelText: 'Category',
                          formValidator: (val) => formvalid.reqField(val),
                          listData: categoryList,
                          listController: categoryCon,
                        )),
                        SelTwoInOneField(
                            child: Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: SellerEditFormFields(
                              controller: preptimeCon,
                              formValidator: (v) => formvalid.reqField(v),
                              inputType: TextInputType.number,
                              // placeholder: '5',

                              // onSaved: (String? val) {
                              //   setState(() {
                              //     // var t1 = int.parse(preptimeCon.text);
                              //   });
                              // },
                              labelText: 'Perparation Time',
                              fieldAble: false,
                              readOnly: true,
                              timeName: 'DurField',
                              sufIcon: Icon(Icons.alarm, color: redColor)),
                        )),
                      ],
                    ),

                    // ! HighLiht THings
                    heightSizedBox(10.0),
                    SelOneField(
                      child: SellerEditFormFields(
                        minLines: 1,
                        maxLines: 1,
                        placeholder: 'Any other things to highlight',
                        inputType: TextInputType.text,
                        controller: highLightCon,
                        labelText: 'Any other things to highlight',
                        inputFormatterData: [
                          LengthLimitingTextInputFormatter(50),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 11.0, right: 11.0),
                child: BigButton(
                  title: 'Submit',
                  onTap: _sellerItemMetod,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 1, right: 1, top: 1.5, bottom: 1.5),
                child: Container(
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SelTCSTxt('By submitting you agree to our', fSize: 10.0),
                      SelTCUTxt('Terms & Condtions', fSize: 10.0),
                      SelTCSTxt('and', fSize: 10.0),
                      SelTCUTxt('Privacy Statment', fSize: 10.0)
                    ],
                  ),
                ),
              )
            ]),
      ),
    );
  }
}
