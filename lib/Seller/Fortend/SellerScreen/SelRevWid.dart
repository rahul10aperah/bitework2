import 'package:comp1/Elements/buttons.dart';
import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

class FiltReviewPage extends StatefulWidget {
  const FiltReviewPage({Key? key}) : super(key: key);

  @override
  _FiltReviewPageState createState() => _FiltReviewPageState();
}

class _FiltReviewPageState extends State<FiltReviewPage> {
  dynamic filterV1 = false;
  dynamic filterV2 = false;
  dynamic ratingV1 = false;
  dynamic ratingV2 = false;
  dynamic ratingV3 = false;
  dynamic ratingV4 = false;
  dynamic ratingV5 = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Drawer(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SelTxtField('Reviews',
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: blackColor),
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SelTxtField('Filter',
                            fontSize: 15, fontWeight: FontWeight.bold),
                        checkBoxItem(
                            title: 'No comment',
                            checkedValue: filterV1,
                            func: (newValue) {
                              setState(() {
                                filterV1 = newValue;
                              });
                            }),
                        checkBoxItem(
                            title: 'Has comment',
                            checkedValue: filterV2,
                            func: (newValue) {
                              setState(() {
                                filterV2 = newValue;
                              });
                            }),
                      ],
                    ),
                  ),

                  // ! REVIEWS PAGE
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SelTxtField('Rating',
                            fontSize: 15, fontWeight: FontWeight.bold),
                        checkBoxItem(
                            checkedValue: ratingV1,
                            fieldAble: true,
                            func: (newValue) {
                              setState(() {
                                ratingV1 = newValue;
                              });
                            },
                            widget: colStarIcon(c1: yellowColor)),
                        checkBoxItem(
                            checkedValue: ratingV2,
                            fieldAble: true,
                            func: (newValue) {
                              setState(() {
                                ratingV2 = newValue;
                              });
                            },
                            widget: colStarIcon(
                              c1: yellowColor,
                              c2: yellowColor,
                            )),
                        checkBoxItem(
                            checkedValue: ratingV3,
                            fieldAble: true,
                            func: (newValue) {
                              setState(() {
                                ratingV3 = newValue;
                              });
                            },
                            widget: colStarIcon(
                              c1: yellowColor,
                              c2: yellowColor,
                              c3: yellowColor,
                            )),
                        checkBoxItem(
                            checkedValue: ratingV4,
                            fieldAble: true,
                            func: (newValue) {
                              setState(() {
                                ratingV4 = newValue;
                              });
                            },
                            widget: colStarIcon(
                              c1: yellowColor,
                              c2: yellowColor,
                              c3: yellowColor,
                              c4: yellowColor,
                            )),
                        checkBoxItem(
                            checkedValue: ratingV5,
                            fieldAble: true,
                            func: (newValue) {
                              setState(() {
                                ratingV5 = newValue;
                              });
                            },
                            widget: colStarIcon(
                              c1: yellowColor,
                              c2: yellowColor,
                              c3: yellowColor,
                              c4: yellowColor,
                              c5: yellowColor,
                            )),
                      ],
                    ),
                  ),
                ],
              ),

              // ! button PAGE

              // Flex(
              //   direction: Axis.horizontal,
              //   mainAxisAlignment: MainAxisAlignment.end,
              //   crossAxisAlignment: CrossAxisAlignment.baseline,
              //   textBaseline: TextBaseline.alphabetic,
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                          child: Container(
                        height: 40,
                        child: MyTabButton(
                            onTap: () {
                              setState(() {
                                filterV1 = false;
                                filterV2 = false;
                                ratingV1 = false;
                                ratingV2 = false;
                                ratingV3 = false;
                                ratingV4 = false;
                                ratingV5 = false;
                              });
                            },
                            label: 'Reset',
                            color2: darkBlueColor,
                            color: offWhiteColor),
                      )),
                      Expanded(
                          child: Container(
                        height: 40,
                        child: MyTabButton(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            label: 'cancel',
                            color2: darkBlueColor,
                            color: offWhiteColor),
                      ))
                    ],
                  ),
                ],
              ),
              // )
            ]),
      ),
    );
  }

  Widget checkBoxItem(
      {checkedValue = false,
      String? title,
      bool fieldAble = false,
      Widget? widget,
      dynamic func}) {
    return CheckboxListTile(
      title: fieldAble == true ? widget : SelTxtField(title, fontSize: 18),
      value: checkedValue,

      onChanged: func,

      controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
    );
  }

  colStarIcon({
    Color? c1,
    Color? c2,
    Color? c3,
    Color? c4,
    Color? c5,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SelIcon(Icons.star, color: c1),
        SelIcon(Icons.star, color: c2),
        SelIcon(Icons.star, color: c3),
        SelIcon(Icons.star, color: c4),
        SelIcon(Icons.star, color: c5),
      ],
    );
  }
}
