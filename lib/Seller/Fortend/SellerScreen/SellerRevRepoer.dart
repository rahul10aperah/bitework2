import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

import 'SellerProgress.dart';

class SellerRevenuReport extends StatelessWidget {
  static const routeName = '/revenuwreport';
  const SellerRevenuReport({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Revenue Report'),
      ),
      // bottomNavigationBar: BottmNavBar(selectedTab: 1),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SellerCardItem(
                title: 'Earnings',
                price1: 800.20,
                price2: 800.20,
                time1: 'Febuary',
                time2: 'AllTime',
              ),
              SellerTableH5(
                  t1: 'Time Period',
                  t2: 'Earnings',
                  t3: 'Orders',
                  t4: 'Payment Date',
                  t5: 'Download'),
              SellerTableB()
            ],
          ),
        ),
      ),
    );
  }
}

// ! TABLE FOR REPORT

class SellerTableH5 extends StatelessWidget {
  final dynamic t1;
  final dynamic t2;
  final dynamic t3;
  final dynamic t4;
  final dynamic t5;
  final double? fSize;
  SellerTableH5(
      {Key? key, this.t1, this.t2, this.t3, this.t4, this.t5, this.fSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: offWhiteColor,
      decoration: BoxDecoration(
        // border: Border.all(color: offWhiteColor),
        // borderRadius: BorderRadius.circular(10.0),
        borderRadius: BorderRadius.all(
          Radius.circular(6),
        ),
      ),

      padding: EdgeInsets.only(left: 6.0, right: 6),
      child: Table(
        // border: TableBorder(
        //     horizontalInside: BorderSide(color: Colors.blue.shade400),
        //     right: BorderSide(color: Colors.blue.shade400)),
        // border: TableBorder.symmetric(
        //   inside: BorderSide(width: 1),
        // ),
        border: TableBorder.all(
            width: 1,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0))),
        textDirection: TextDirection.ltr,
        children: [
          TableRow(
              // color: offWhiteColor,
              decoration: BoxDecoration(
                  color: offWhiteColor,
                  borderRadius: BorderRadius.all(
                    Radius.circular(6),
                  )
                  // BorderRadius.only(topLeft: Radius.circular(10.0)
                  ),
              children: [
                tableH(t1, fSize: fSize),
                tableH(t2, fSize: fSize),
                tableH(t3, fSize: fSize),
                tableH(t4, fSize: fSize),
                tableH(t5, fSize: fSize),
              ]),
        ],
      ),
    );
  }

  // ! TABLE Header REPORT
  Widget tableH(String t, {double? fSize, Color? color}) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0, bottom: 3),
      child: Text(t,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: fSize != null ? fSize : 15,
              color: color != null ? color : textBlueColor,
              fontFamily: montserratSemiBold)),
    );
  }
}

// ! TABLE row REPORT
class SellerTableB extends StatelessWidget {
  final v = 'yes';
  final String? r;
  SellerTableB({
    Key? key,
    this.r,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 6.0, right: 6),
      child: SizedBox(
        height: 700,
        width: 600,
        child: ListView.builder(
            itemCount: 8,
            itemBuilder: (context, index) {
              return Container(
                child: Table(
                    border: TableBorder.all(width: 1, color: blackColor),
                    children: [
                      TableRow(children: [
                        tableH('Time Period'),
                        tableH(r != null ? 'Time Period' : ''),
                        tableH('Time Period'),
                        tableH('Time Period'),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 5.0, left: 1.5, right: 1.5),
                          child: Container(
                              width: double.infinity,
                              color: Colors.red,
                              child: tableH('Download')),
                        ),
                      ])
                    ]),
              );
            }),
      ),
    );
  }

  // ! TABLE Header REPORT
  Widget tableH(
    String t,
  ) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text(t,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 12, fontFamily: montserratMedium)),
    );
  }
}
