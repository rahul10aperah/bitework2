import 'package:comp1/Seller/Fortend/SellerWidget/AllCommonField.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

import 'SelRevWid.dart';

class SellerReviewScr extends StatelessWidget {
  static const routeName = '/SellerReviewScr';
  const SellerReviewScr({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Review'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SellerReviewHeader(rat1: 4.3, rat2: 4.5, mon: 'March'),
                // ! DIVIDER WITH tEXT
                Row(children: <Widget>[
                  Expanded(child: Divider()),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0, right: 5),
                    child: Text(
                      "15 Reviews",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                  ),
                  Expanded(child: Divider()),
                ]),
                // ! review item
                SellerReviewItem(
                  t3: 'Earnings',
                  t1: 'Febuary',
                  t2: 'AllTime',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// ! Review Header
class SellerReviewHeader extends StatelessWidget {
  final dynamic rat1;
  final dynamic rat2;
  final dynamic mon;
  SellerReviewHeader({Key? key, this.rat1, this.rat2, this.mon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SelIcon(Icons.star, color: yellowColor),
                      SelTxtField(
                        rat1.toString(),
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: blackColor,
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        'Month',
                        style: labelTextStyle,
                      ),
                      btn('filter', onTap: () {
                        // print('filter page');
                        // Widget page() {
                        //   return FiltReviewPage();
                        // }
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FiltReviewPage()));
                      })
                    ],
                  ),

                  // ! all time
                  Row(
                    children: [
                      SelIcon(Icons.star, color: yellowColor),
                      SelTxtField(
                        rat2.toString(),
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: blackColor,
                      )
                    ],
                  )
                ],
              ),

              // ! NEXTLINE
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SelTxtField(
                    mon.toString(),
                    fontSize: 15,
                    color: blackColor,
                  ),
                  // ! all time
                  Row(
                    children: [
                      SelTxtField(
                        mon.toString(),
                        fontSize: 15,
                        color: blackColor,
                      ),
                      btn('View')
                    ],
                  ),

                  SelTxtField(
                    'All Time',
                    fontSize: 15,
                    color: blackColor,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget btn(String btnName, {dynamic onTap}) {
    return Padding(
      padding: const EdgeInsets.only(left: 3.0, bottom: 5),
      child: Container(
        constraints: BoxConstraints(
            maxWidth: 35.0,
            minHeight: 25.0,
            maxHeight: 25.0), // min sizes for Material buttons
        alignment: Alignment.center,
        color: darkBlueColor,
        child: InkWell(
          onTap: onTap,
          child: Text(
            btnName,
            style: TextStyle(fontSize: 12, color: offWhiteColor),
          ),
        ),
      ),
    );
  }
}

// ! Review Item
class SellerReviewItem extends StatelessWidget {
  final dynamic t1;
  final dynamic t2;
  final dynamic t3;
  SellerReviewItem({
    Key? key,
    this.t1,
    this.t2,
    this.t3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // ! Star PAGE
              starc(starNum: 6),

              Padding(
                padding: const EdgeInsets.only(top: 3.0, left: 10, bottom: 2),
                child: SelTxtField(
                  t1,
                  fontSize: 17,
                  color: blackColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 3.0, left: 10, bottom: 2),
                child: SelTxtField(t2,
                    fontSize: 15,
                    color: blackColor,
                    fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 3.0, left: 10, bottom: 2),
                child: SelTxtField(
                  t3,
                  fontSize: 15,
                  color: redColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // ! STAR COUNT
  Widget starc({int starNum = 4}) {
    return SizedBox(
      height: 40,
      width: 600,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 4,
          itemBuilder: (context, index) {
            return SelIcon(Icons.star, color: yellowColor);
          }),
    );
  }
}
