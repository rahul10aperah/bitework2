import 'package:comp1/Seller/Backend/BlocPattern/HomeB/selhomep_bloc.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SelKitechenEdit.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerAddItem.dart';
import 'package:comp1/Seller/Fortend/SellerScreen/SellerAddmoreDet.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ! seller appbar btn
class SelAppbarBtn extends StatelessWidget {
  final int? sellerId;
  SelAppbarBtn({Key? key, this.sellerId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: InkWell(
        onTap: () => navigationPush(
            context,
            SellerAddItem(
              sellerId: sellerId,
            )),
        child: Container(
          padding: const EdgeInsets.all(7),
          // color: darkBlueColor,

          decoration:
              // boxDecoration,
              BoxDecoration(
            // borderRadius: BorderRadius.all(Radius.circular(12)),
            borderRadius: BorderRadius.circular(6),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.8),
                  blurRadius: 2,
                  offset: Offset(-0.1, -0.1)),
            ],
            color: darkBlueColor,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(1.0),
                child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      color: offWhiteColor,
                    ),
                    child: Center(
                      child: Icon(Icons.add,
                          size: 18,
                          textDirection: TextDirection.ltr,
                          color: textBlueColor),
                    )),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                child: Text("Add",
                    style: TextStyle(
                        fontSize: 18,
                        color: offWhiteColor,
                        fontFamily: montserratMedium)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ! seller card txt
class SelCardHTxt extends StatelessWidget {
  final String t1;

  dynamic prodNumber;

  final Color? col;
  SelCardHTxt({Key? key, this.col, this.t1 = '', this.prodNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      // padding: const EdgeInsets.all(0.0),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(6, 3, 6, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(prodNumber['DishName'].toString(),
                    style:
                        TextStyle(fontSize: 16, fontFamily: montserratMedium)),
                Container(
                    decoration: BoxDecoration(
                      // border: Border.all(color: darkBlueColor),
                      color: col,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(3.0, 0, 3.0, 0),
                      // padding: const EdgeInsets.all(3.0),
                      child: Text(t1,
                          style: TextStyle(
                              fontSize: 16,
                              color: offWhiteColor,
                              fontFamily: montserratMedium)),
                    ))
              ],
            ),
          ),
          // ! More data2
          prodNumber['DishStatus'] != 1
              ? Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(6, 0, 6, 3),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              'Availability: ${prodNumber['ServingsAvailable'] == null ? 0.toString() : prodNumber['ServingsAvailable'].toString()}',
                              style: TextStyle(
                                  fontSize: 14, fontFamily: montserratMedium)),
                          Text(
                              'Sold: ${prodNumber['Sold'] == null ? 0.toString() : prodNumber['Sold'].toString()}',
                              style: TextStyle(
                                  fontSize: 14, fontFamily: montserratMedium)),
                        ],
                      ),
                    ),
                    // ! More Data 3
                    Padding(
                      padding: const EdgeInsets.fromLTRB(6, 0, 6, 6),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              'Limit: ${prodNumber['OrderLimit'] == null ? 0.toString() : prodNumber['OrderLimit'].toString()}',
                              style: TextStyle(
                                  fontSize: 14, fontFamily: montserratMedium)),
                          Text(
                              'Expired: ${prodNumber['ExpireIn'] == null ? 0.toString() : prodNumber['ExpireIn'].toString()} Days',
                              style: TextStyle(
                                  fontSize: 14, fontFamily: montserratMedium)),
                        ],
                      ),
                    ),
                  ],
                )
              : Container(child: null),
        ],
      ),
    );
  }
}

// ! SELLER CLICK BoxDecoration
class SelClickBtn extends StatelessWidget {
  dynamic sellerId;
  final dynamic prodNumber;
  SelClickBtn({Key? key, this.prodNumber, this.sellerId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(top: 50.0, bottom: 50, left: 10, right: 10),
      child: Container(
        width: double.infinity,
        height: 50,
        color: redColor,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            eadTxt('Edit',
                onTap: () => navigationPush(
                    context,
                    SelKitchenEditScr(
                      sellerId: sellerId,
                      prodNumber: prodNumber,
                    )
                    // SellerAddItem(
                    //   sellerId: sellerId,
                    // )
                    )),
            eadTxt('Add/Extend Time',
                onTap: () => navigationPush(
                    context,
                    SellerAddMoreDetails(
                        sellerId: sellerId, prodNumber: prodNumber))),
            eadTxt('Delete', onTap: () {
              print(prodNumber['DishId']);
              BlocProvider.of<SelhomepBloc>(context)
                ..add(SelHomeDelItemEvent(dishId: prodNumber['DishId']));
            }),
          ],
        ),
      ),
    );
  }

  // ! Text for EAD BTN
  Widget eadTxt(String t, {dynamic onTap}) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(color: offWhiteColor),
            ),
            child: Text(t,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 15,
                    color: offWhiteColor,
                    fontFamily: montserratMedium))),
      ),
    );
  }
}
