import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';

import 'package:duration_picker/duration_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class SellerEditFormFields extends StatefulWidget {
  final TextInputType? inputType;
  final String? placeholder;
  String? labelText;
  dynamic onSaved;
  dynamic onChanged;
  String? initValue;
  String? errorText;
  final fNumber;
  final Widget? sufIcon;
  final Widget? prefixIcon;
  double? vertical;
  double? horizontal;
  final List<TextInputFormatter>? inputFormatterData;
  final TextEditingController? controller;
  String? Function(String?)? formValidator;
  // final Function? formValidator;
  String? timeName;
  bool? fieldAble;
  FocusNode? focusNode;
  int? maxLen;
  int? minLines;
  int? maxLines;
  bool? readOnly;
  dynamic onTap;
  bool? enabled;
  SellerEditFormFields(
      {Key? key,
      this.onTap,
      this.fNumber,
      this.vertical,
      this.horizontal,
      this.inputType,
      this.errorText,
      this.placeholder,
      this.controller,
      this.formValidator,
      this.labelText,
      this.onSaved,
      this.onChanged,
      this.initValue,
      this.prefixIcon,
      this.sufIcon,
      this.inputFormatterData,
      this.timeName,
      this.maxLines,
      this.fieldAble,
      this.focusNode,
      this.maxLen,
      this.minLines,
      this.readOnly,
      this.enabled})
      : super(key: key);

  @override
  _FieldFState createState() => _FieldFState();
}

class _FieldFState extends State<SellerEditFormFields> {
  // ! choosing Date format
  Future _selectDateFrom() async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(
          Duration(days: 14),
        ));
    if (picked != null)
      setState(() {
        dynamic timeSlot;

        switch (picked.month) {
          case 1:
            timeSlot = 'Jan';
            break;
          case 2:
            timeSlot = 'Feb';
            break;
          case 3:
            timeSlot = 'Mar';
            break;
          case 4:
            timeSlot = 'Apr';
            break;
          case 5:
            timeSlot = 'May';
            break;
          case 6:
            timeSlot = 'Jun';
            break;
          case 7:
            timeSlot = 'Jul';
            break;
          case 8:
            timeSlot = 'Aug';
            break;
          case 9:
            timeSlot = 'Sep';
            break;
          case 10:
            timeSlot = 'Oct';
            break;
          case 11:
            timeSlot = 'Nov';
            break;
          case 12:
            timeSlot = 'Dec';
            break;
        }

        var dat = "${picked.day}-$timeSlot";

        widget.controller!.text = dat;
      });
  }

  Future _choseDate() async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: new TimeOfDay.now(),
      initialEntryMode: TimePickerEntryMode.input,
    );
    if (picked != null)
      setState(() {
        var timeSlot = picked.period.toString().split('.');

        var dat =
            " ${picked.hourOfPeriod} : ${picked.minute.toString().length < 2 ? 0.toString() + picked.minute.toString() : picked.minute.toString()}  ${timeSlot.last}  ";

        widget.controller!.text = dat;
      });
  }

  Future _getduractionTime() async {
    var resDur = await showDurationPicker(
      context: context,
      initialTime: Duration(hours: 1, minutes: 30),
      baseUnit: BaseUnit.minute,
    );

    if (resDur != null)
      setState(() {
        var t1 = resDur.inHours == 0 ? '' : resDur.inHours.toString() + 'hr';
        var m1 = resDur.inMinutes - (resDur.inHours * 60);
        var dat = "$t1 ${m1.toString()} mins    ";

        var totalTime = resDur.inHours * 60 + resDur.inMinutes;
        // print('mins on ${resDur.inMinutes / 60}');
        // print(resDur.inMinutes - (resDur.inHours * 60));
        // print('totalTime $totalTime');
        print('value $dat');
        widget.controller!.text = dat;
      });
  }
  // ! Drop Down Btn

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.labelText != null
            ? Text(widget.labelText!, style: labelTextStyle)
            : heightSizedBox(0.0),
        TextFormField(
            onTap: widget.onTap != null
                ? widget.onTap
                : () {
                    if (widget.timeName == 'DateField') {
                      _choseDate();
                    }
                    if (widget.timeName == 'DurField') {
                      _getduractionTime();
                    }
                    if (widget.timeName == 'CalendarFieldFrom') {
                      _selectDateFrom();
                    }
                    // if (widget.timeName == 'CalendarFieldTo') {
                    //   _selectDateTo();
                    // }
                  },
            autovalidateMode: AutovalidateMode.onUserInteraction,
            enabled: widget.enabled,
            maxLines: widget.maxLines,
            onChanged: widget.onChanged,
            inputFormatters: widget.inputFormatterData,
            initialValue: widget.initValue,
            enableInteractiveSelection:
                widget.fieldAble == true || widget.fieldAble == null
                    ? true
                    : false,
            minLines: widget.minLines,
            maxLength: widget.maxLen,

            // focusNode: FocusNode(),
            focusNode: widget.focusNode,
            controller: widget.controller,
            onSaved: widget.onSaved,
            keyboardType: widget.inputType,
            // validator: (value) => widget.formValidator!(value),
            validator: widget.formValidator,
            autofocus: false,
            readOnly: widget.readOnly ?? false,
            scrollPadding: EdgeInsets.zero,
            decoration: InputDecoration(
                errorText: widget.errorText,
                counter: Offstage(),
                labelStyle: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
                // labelText: widget.labelText,
                contentPadding: EdgeInsets.symmetric(
                    vertical: widget.vertical != null ? widget.vertical! : 5.0,
                    horizontal: widget.horizontal ?? 0),
                isDense: true,
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: redColor, width: 1.0),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                border: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1.0)),
                focusColor: Colors.black,
                hoverColor: Colors.black,
                prefixIconConstraints:
                    BoxConstraints(minWidth: 0, minHeight: 0),
                hintText: widget.placeholder,
                prefixIcon: widget.prefixIcon,
                suffixIcon: widget.sufIcon,
                suffixIconConstraints:
                    BoxConstraints.tightFor(width: 35.0, height: 14.0))),
      ],
    );
  }
}

// ! RADION BUTTON
// ignore: must_be_immutable
class SellerRadBtn extends StatefulWidget {
  dynamic gValue = 0;
  String title;
  dynamic val;
  Function(dynamic)? onChg;
  SellerRadBtn(
      {Key? key, required this.title, this.val, this.gValue, this.onChg})
      : super(key: key);

  @override
  _SellerRadBtnState createState() => _SellerRadBtnState();
}

class _SellerRadBtnState extends State<SellerRadBtn> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: -7,
          children: [
            Transform.scale(
              alignment: Alignment.centerLeft,
              scale: 1,
              child: Radio(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: widget.val,
                  groupValue: widget.gValue,
                  onChanged: widget.onChg),
            ),
            Text(
              widget.title.toString(),
              maxLines: 1,
              style: labelTextStyle,
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.start,
            ),
          ]),
    );
  }
}

class DayWorkRaioBtn extends StatelessWidget {
  final String t1;
  final String t2;
  final String t3;
  dynamic val1;
  dynamic val2;
  dynamic val3;
  dynamic gVal;
  final String? labelText;
  Function(dynamic)? onChg;
  DayWorkRaioBtn(
      {Key? key,
      required this.t1,
      required this.t2,
      required this.t3,
      this.labelText,
      this.val1,
      this.val2,
      this.val3,
      this.gVal,
      this.onChg})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        labelText != null
            ? Padding(
                padding: const EdgeInsets.fromLTRB(9, 0, 15, 0),
                child: Text(labelText!, style: labelTextStyle),
              )
            : heightSizedBox(0.0),
        // SizedBox(
        //   height: 30,
        //   width: 500,
        // child:
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
          child: Row(
            // ListView(
            // scrollDirection: Axis.horizontal,
            // shrinkWrap: true,
            children: [
              SellerRadBtn(
                title: t1,
                val: val1,
                gValue: gVal,
                onChg: onChg,
              ),
              SellerRadBtn(
                title: t2,
                val: val2,
                gValue: gVal,
                onChg: onChg,
              ),
              SellerRadBtn(
                title: t3,
                val: val3,
                gValue: gVal,
                onChg: onChg,
              ),
            ],
          ),
        ),
        // ),
      ],
    );
  }
}

// ! HEADLINE TEXT FOR LABLE

class SelOneField extends StatelessWidget {
  final Widget? child;
  double? left;
  SelOneField({Key? key, this.child, this.left}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(left ?? 9, 0, 15, 0),
        child: Container(
          width: double.maxFinite,
          child: child,
        ));
  }
}

class SelTwoInOneField extends StatelessWidget {
  final Widget? child;
  final double? width;
  double? left;
  SelTwoInOneField({Key? key, this.child, this.width, this.left})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
          padding: EdgeInsets.fromLTRB(left ?? 9, 0, 12, 0),
          child: Container(
            width: double.maxFinite,
            child: child,
          )),
    );
  }
}

// ! Seller Card Field
class SelFieldCard extends StatelessWidget {
  final Widget? child;
  SelFieldCard({Key? key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding:
            const EdgeInsets.only(left: 6.0, right: 6.0, top: 3, bottom: 3),
        child: Card(
            elevation: 1,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(9),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 8.0, top: 6, bottom: 6),
              child: child,
            )));
  }
}

class DropDownBtn extends StatefulWidget {
  String? dName;
  dynamic listData;
  String pageName;
  String? currentItem;
  String? labelText;
  double? vertical;
  double? horizontal;
  TextEditingController? listController = new TextEditingController();
  dynamic onChanged;
  // String? Function(String?)? formValidator;
  // dynamic formValidator;
  String? Function(dynamic)? formValidator;
  dynamic selectedItemBuilder;

  DropDownBtn(
      {Key? key,
      this.selectedItemBuilder,
      this.labelText,
      this.dName,
      this.listData,
      this.vertical,
      this.horizontal,
      this.listController,
      this.pageName = '',
      this.formValidator,
      this.currentItem,
      this.onChanged})
      : super(key: key);

  @override
  _DropDownBtnState createState() => _DropDownBtnState();
}

class _DropDownBtnState extends State<DropDownBtn> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.labelText != null
            ? Text(widget.labelText!, style: labelTextStyle)
            : heightSizedBox(0.0),
        DropdownButtonFormField<dynamic>(
            // ! DROP DOWN MENU dropdownValue
            dropdownColor: offWhiteColor,
            // value: widget.currentItem,
            value: widget.listController!.text.isNotEmpty
                ? widget.listController!.text
                : widget.currentItem,
            validator: widget.formValidator,
            isExpanded: true,
            icon: const Icon(
              Icons.arrow_drop_down,
              size: 18,
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            alignment: AlignmentDirectional.bottomEnd,
            iconSize: 18,
            elevation: 1,
            style: smallTextStyle,
            onChanged: widget.onChanged != null
                ? widget.onChanged
                : (dynamic newValue) {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    setState(() {
                      widget.currentItem = newValue;
                      widget.listController!.text = newValue!;
                    });
                  },
            onTap: () {},
            selectedItemBuilder: widget.selectedItemBuilder,

            // items: widget.listData.toList(),
            items:
                //   widget.listData.map<DropdownMenuItem<String>>((String value) {
                // return DropdownMenuItem<String>(

                widget.listData.map<DropdownMenuItem>((dynamic value) {
              return DropdownMenuItem(
                value: value,
                child: Text(value, style: smallTextStyle),
              );
            }).toList(),

            // ! DROP DOWN MENU Dname

            hint: Text(
              widget.currentItem == null
                  ? widget.pageName.toString()
                  : widget.dName.toString(),
              style: smallTextStyle,
            ),
            decoration: InputDecoration(
                counter: Offstage(),
                labelStyle: labelTextStyle,
                contentPadding: EdgeInsets.symmetric(
                    vertical: widget.vertical != null ? widget.vertical! : 5.0,
                    horizontal: widget.horizontal ?? 0),
                isDense: true,
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: redColor, width: 1.0),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                border: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1.0)),
                focusColor: Colors.black,
                hoverColor: Colors.black,
                suffixIconConstraints:
                    BoxConstraints.tightFor(width: 35.0, height: 12.0))),
      ],
    );
  }
}

// ! DropDownBtn for UpdateGoogleUser
// class DropDownBtnUpdate extends StatefulWidget {
//   String? dName;
//   dynamic listData;
//   String pageName;
//   String? currentItem;
//   String? labelText;
//   double? vertical;
//   double? horizontal;
//   TextEditingController? listController = new TextEditingController();

//   // String? Function(String?)? formValidator;
//   // dynamic formValidator;
//   String? Function(dynamic)? formValidator;

//   DropDownBtnUpdate(
//       {Key? key,
//       this.labelText,
//       this.dName,
//       this.listData,
//       this.vertical,
//       this.horizontal,
//       this.listController,
//       this.pageName = '',
//       this.formValidator,
//       this.currentItem})
//       : super(key: key);

//   @override
//   _DropDownBtnUpdateState createState() => _DropDownBtnUpdateState();
// }

// class _DropDownBtnUpdateState extends State<DropDownBtnUpdate> {
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.start,
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         widget.labelText != null
//             ? Text(widget.labelText!, style: labelTextStyle)
//             : heightSizedBox(0.0),
//         DropdownButtonFormField<dynamic>(
//             // ! DROP DOWN MENU dropdownValue

//             dropdownColor: offWhiteColor,
//             value: widget.currentItem,
//             validator: widget.formValidator,
//             isExpanded: true,
//             autofocus: true,
//             iconSize: 18,
//             elevation: 1,
//             icon: const Icon(
//               Icons.arrow_drop_down,
//               size: 18,
//             ),
//             alignment: AlignmentDirectional.bottomEnd,
//             style: smallTextStyle,
//             onChanged: (dynamic newValue) {
//               FocusScope.of(context).requestFocus(new FocusNode());
//               setState(() {
//                 widget.currentItem = newValue;
//                 widget.listController!.text = newValue!;
//               });
//             },
//             items:
//                 //   widget.listData.map<DropdownMenuItem<String>>((String value) {
//                 // return DropdownMenuItem<String>(

//                 widget.listData.map<DropdownMenuItem>((dynamic value) {
//               return DropdownMenuItem(
//                 value: value,
//                 child: Text(value, style: smallTextStyle),
//               );
//             }).toList(),

//             // ! DROP DOWN MENU Dname

//             hint: Text(
//               widget.currentItem != null
//                   ? widget.currentItem.toString()
//                   : widget.dName.toString(),
//               style: smallTextStyle,
//             ),
//             decoration: InputDecoration(
//                 counter: Offstage(),
//                 labelStyle: labelTextStyle,
//                 contentPadding: EdgeInsets.symmetric(
//                     vertical: widget.vertical != null ? widget.vertical! : 5.0,
//                     horizontal: widget.horizontal ?? 0),
//                 isDense: true,
//                 focusedBorder: UnderlineInputBorder(
//                   borderSide: BorderSide(color: redColor, width: 1.0),
//                 ),
//                 enabledBorder: UnderlineInputBorder(
//                   borderSide: BorderSide(color: Colors.grey, width: 1.0),
//                 ),
//                 border: UnderlineInputBorder(
//                     borderSide: BorderSide(color: Colors.grey, width: 1.0)),
//                 focusColor: Colors.black,
//                 hoverColor: Colors.black,
//                 suffixIconConstraints:
//                     BoxConstraints.tightFor(width: 35.0, height: 12.0))),
//       ],
//     );
//   }
// }
