import 'dart:io';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/style.dart';

import 'package:flutter/material.dart';

import 'AllCommonField.dart';

class SellerUpdBtn extends StatefulWidget {
  final String? title;
  Function()? onPressed;
  String? filename;
  Function()? cleanImg;
  SellerUpdBtn(
      {Key? key, this.title, this.onPressed, this.filename, this.cleanImg})
      : super(key: key);

  @override
  _SellerUpdBtnState createState() => _SellerUpdBtnState();
}

class _SellerUpdBtnState extends State<SellerUpdBtn> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // Text(widget.title!, style: labelTextStyle, textAlign: TextAlign.center),
        widget.title != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 9.0),
                child: Text(widget.title!,
                    style: labelTextStyle, textAlign: TextAlign.center),
              )
            : heightSizedBox(0.0),
        Padding(
          padding: const EdgeInsets.only(left: 9.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  MaterialButton(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      color: darkBlueColor,
                      child: Text("Upload",
                          style: TextStyle(
                              color: offWhiteColor,
                              fontFamily: montserratMedium)),
                      onPressed: widget.onPressed),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Container(
                        child: widget.filename == null ||
                                widget.filename!.isEmpty
                            ? null
                            : InkWell(
                                onTap: widget.cleanImg,
                                child: SelIcon(Icons.cancel, color: redColor))),
                  )
                ],
                // ),
              ),
              Text(
                widget.filename == null || widget.filename!.isEmpty
                    ? 'No Files'
                    : widget.filename.toString(),
                softWrap: true,
                style: TextStyle(
                  fontFamily: montserratMedium,
                  fontSize: 8,
                ),
                maxLines: 1,
              ),
            ],
          ),
        ),
      ],
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                         // ! 2 UPLOAD MULTIPLE PIC                         */
/* -------------------------------------------------------------------------- */
class SelUpdMultiFile extends StatefulWidget {
  final Function()? upDFile;
  final Function()? cleanImg;
  final String? imgSrc;
  final String? networkImage;
  SelUpdMultiFile(
      {Key? key, this.imgSrc, this.upDFile, this.cleanImg, this.networkImage})
      : super(key: key);

  @override
  _SelUpdMultiFileState createState() => _SelUpdMultiFileState();
}

class _SelUpdMultiFileState extends State<SelUpdMultiFile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(1.0),
        child: Column(
          children: [
            // ! UPLOAD NEW FILE
            Container(
              padding: const EdgeInsets.all(1.0),
              margin: EdgeInsets.all(1.0),
              height: 62,
              decoration: BoxDecoration(
                image:
                    //  widget.imgSrc != null?
                    DecorationImage(
                  image: FileImage(widget.imgSrc != null
                      ? File(widget.imgSrc as String)
                      : File('')),
                  // fit: BoxFit.fitWidth,
                )
                // : null
                ,
                // ! Cricle Border
                borderRadius: BorderRadius.circular(5),
              ),
              child: MaterialButton(
                // color: Color.fromRGBO(215, 219, 221, .5),
                color: widget.imgSrc == null ? offWhiteColor : null,
                // height: 50,
                onPressed: widget.upDFile,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Ink(
                  // height: 45,
                  // width: widget.imgSrc == null ? 50 : null,
                  decoration: BoxDecoration(
                    // image: widget.imgSrc != null
                    //     ? DecorationImage(
                    //         image: FileImage(widget.imgSrc != null
                    //             ? File(widget.imgSrc as String)
                    //             : File('')),
                    //         fit: BoxFit.cover,
                    //       )
                    //     : null,
                    // ! Cricle Border
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Container(
                    constraints: BoxConstraints(
                        maxWidth: 20.0,
                        minHeight: 45.0,
                        maxHeight: 45.0), // min sizes for Material buttons
                    alignment: Alignment.center,
                    child: Center(
                      child: Container(
                          height: 20,
                          decoration: BoxDecoration(
                            border: widget.imgSrc == null
                                ? Border.all(color: redColor)
                                : null,
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            color: widget.imgSrc == null ? redColor : null,
                          ),
                          child: widget.imgSrc == null ||
                                  widget.imgSrc.toString().isEmpty
                              ? Center(
                                  child: Icon(
                                    Icons.add,
                                    size: 16,
                                    color: offWhiteColor,
                                    textDirection: TextDirection.ltr,
                                  ),
                                )
                              : null),
                    ),
                  ),
                ),
              ),
            ),
            // ! Remove Button
            Padding(
              padding: const EdgeInsets.only(),
              child: Container(
                  child: widget.imgSrc == null || widget.imgSrc!.isEmpty
                      ? null
                      : InkWell(
                          onTap: widget.cleanImg,
                          child: SelIcon(Icons.cancel,
                              size: 15, color: redColor))),
            )
          ],
        ));
  }
}

// ! SelUpd NetworkImage
class SelUpdMultiFileNet extends StatefulWidget {
  final Function()? upDFile;
  final Function()? cleanImg;
  final String? imgSrc;
  final String? networkImage;
  Color? backColor;
  SelUpdMultiFileNet(
      {Key? key,
      this.imgSrc,
      this.backColor,
      this.upDFile,
      this.cleanImg,
      this.networkImage})
      : super(key: key);

  @override
  _SelUpdMultiFileNetState createState() => _SelUpdMultiFileNetState();
}

class _SelUpdMultiFileNetState extends State<SelUpdMultiFileNet> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(1.0),
        child: Column(
          children: [
            // ! UPLOAD NEW FILE
            Container(
              padding: const EdgeInsets.all(1.0),
              margin: EdgeInsets.all(1.0),
              height: 62,
              decoration: BoxDecoration(
                image:
                    // widget.networkImage != null ||
                    //         widget.networkImage.toString().length > 0
                    widget.imgSrc == null || widget.imgSrc.toString().isEmpty
                        ? DecorationImage(
                            image: NetworkImage(widget.networkImage as String),
                            // image: NetworkImage(widget.imgSrc as String),
                          )
                        : DecorationImage(
                            image: FileImage(widget.imgSrc != null
                                ? File(widget.imgSrc as String)
                                : File('')),
                            // fit: BoxFit.fitWidth,
                          ),
                // ! Cricle Border
                borderRadius: BorderRadius.circular(5),
              ),
              child: MaterialButton(
                // color:
                //  widget.imgSrc == null || widget.imgSrc.toString().isEmpty
                //     ? offWhiteColor
                //     : null,
                color: widget.backColor != null ? widget.backColor : null,
                // height: 50,
                onPressed: widget.upDFile,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Ink(
                  decoration: BoxDecoration(
                    // ! Cricle Border
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Container(
                    constraints: BoxConstraints(
                        maxWidth: 20.0,
                        minHeight: 45.0,
                        maxHeight: 45.0), // min sizes for Material buttons
                    alignment: Alignment.center,
                    child: Center(
                      child: Container(
                          height: 20,
                          decoration: BoxDecoration(
                            border: widget.imgSrc == null
                                ? Border.all(color: redColor)
                                : null,
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            color: widget.imgSrc == null ? redColor : null,
                          ),
                          child: widget.imgSrc == null ||
                                  widget.imgSrc.toString().isEmpty
                              ? Center(
                                  child: Icon(
                                    Icons.add,
                                    size: 16,
                                    color: offWhiteColor,
                                    textDirection: TextDirection.ltr,
                                  ),
                                )
                              : null),
                    ),
                  ),
                ),
              ),
            ),
            // ! Clean Imge Button
            Padding(
              padding: const EdgeInsets.only(),
              child: Container(
                  child: widget.imgSrc == null || widget.imgSrc!.isEmpty
                      ? null
                      : InkWell(
                          onTap: widget.cleanImg,
                          child: SelIcon(Icons.cancel,
                              size: 15, color: redColor))),
            )
          ],
        ));
  }
}
