import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';

class SelTxtField extends StatelessWidget {
  final double? fontSize;
  final dynamic title;
  final FontWeight? fontWeight;
  final TextDecoration? decoration;
  final Color? color;
  final TextDirection? direction;
  final int? maxLines;
  final TextAlign? textAlign;
  final String? fontFamily;
  SelTxtField(this.title,
      {Key? key,
      this.color,
      this.decoration,
      this.textAlign,
      this.maxLines,
      this.fontSize,
      this.fontFamily,
      this.fontWeight,
      this.direction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(title,
        textDirection: direction,
        maxLines: maxLines,
        textAlign: textAlign,
        style: TextStyle(
            fontSize: fontSize,
            fontWeight: fontWeight,
            decoration: decoration,
            color: color,
            fontFamily: fontFamily ?? montserratMedium));
  }
}

// ! star icon
class SelIcon extends StatelessWidget {
  final IconData? icon;
  final double? size;
  final Color? color;
  final TextDirection? textDirection;
  const SelIcon(this.icon,
      {Key? key, this.color, this.size, this.textDirection})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Icon(
      icon,
      // color: color != null ? color : yellowColor,
      color: color != null ? color : primaryColor,
      size: size,
      textDirection: textDirection,
    );
  }
}

// ! Seller Menu Text
class SellerMenuBtn extends StatefulWidget {
  dynamic selectedIndex = 'Pending';
  final List? titList;

  SellerMenuBtn({
    Key? key,
    this.selectedIndex,
    this.titList,
  }) : super(key: key);

  @override
  _SellerMenuBtnState createState() => _SellerMenuBtnState();
}

class _SellerMenuBtnState extends State<SellerMenuBtn> {
  void onItemTapped(String? index) {
    setState(() {
      widget.selectedIndex = index;
    });
  }

  txtNum() {
    for (var i in widget.titList!) {
      return SellerMenuTxt(
        widget.titList![i].toString(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(children: txtNum()),
    );
    // SizedBox(
    //   height: 30,
    //   width: 400,
    //   child: ListView.builder(
    //     shrinkWrap: true,
    //     scrollDirection: Axis.horizontal,
    //     itemCount: widget.titList!.length,
    //     itemBuilder: (context, index) {
    //       return SellerMenuTxt(widget.titList![index].toString(),
    //           selectedIndex: widget.selectedIndex, onItemTapped: onItemTapped);
    //     },
    //   ),
    // );
  }
}

// ! TEXTBTN  FOR MENU
// ignore: must_be_immutable
class SellerMenuTxt extends StatefulWidget {
  final String title;
  dynamic selectedIndex;
  dynamic onItemTapped;
  final dynamic txtColor;
  BorderRadius? borderRadius;
  double? height;
  SellerMenuTxt(this.title,
      {Key? key,
      this.height,
      this.selectedIndex,
      this.onItemTapped,
      this.txtColor,
      this.borderRadius})
      : super(key: key);

  @override
  _SellerMenuTxtState createState() => _SellerMenuTxtState();
}

class _SellerMenuTxtState extends State<SellerMenuTxt> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: widget.height != null ? widget.height : 30,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(color: darkBlueColor),
          color: widget.selectedIndex == widget.title ? darkBlueColor : null,
          // borderRadius: BorderRadius,
          // borderRadius: BorderRadius.circular(5),
          borderRadius: widget.borderRadius,
        ),
        child: InkWell(
          onTap: () {
            widget.onItemTapped(widget.title);
          },
          child: Text(widget.title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 14,
                  color: widget.txtColor != null ? offWhiteColor : blackColor,
                  fontFamily: montserratMedium)),
        ),
      ),
    );
  }
}

// ! Terms and Condtion Txt
// !1  Simple Text
class SelTCSTxt extends StatelessWidget {
  final String t;
  final double? fSize;
  final Color? fCol;
  final TextDecoration? fDec;
  SelTCSTxt(this.t, {Key? key, this.fCol, this.fDec, this.fSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(t,
        style: TextStyle(
            fontFamily: montserratMedium,
            fontSize: fSize != null ? fSize : 12,
            color: fCol != null ? fCol : Colors.black));
  }
}

// ! Link Txt
class SelTCUTxt extends StatelessWidget {
  final String t;
  final dynamic fSize;
  final Color? fCol;
  final TextDecoration? fDec;
  SelTCUTxt(this.t, {Key? key, this.fCol, this.fDec, this.fSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Text(t,
          style: TextStyle(
            fontSize: fSize != null ? fSize : 12,
            decoration: fDec != null ? fDec : TextDecoration.underline,
            color: fCol != null ? fCol : Colors.blue,
          )),
    );
  }
}
