import 'package:comp1/Seller/Backend/BlocPattern/DishAdd/dishadd_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/HomeB/selhomep_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/Progress/selprogress_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/PromoCode/selgenpromocode_bloc.dart';

import 'package:comp1/Seller/Backend/BlocPattern/Reg/sellerregconf_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/MoreItem/moreadditem_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/Revenue/selrevenue_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/Review/selreview_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/TrackOrder/seltrackorder_bloc.dart';
import 'package:comp1/Seller/Backend/BlocPattern/Verify/selverifyp_bloc.dart';
import 'package:comp1/util/input_validation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class MainBloc {
  static List<SingleChildWidget> allBlocs() {
    return [
      ChangeNotifierProvider(create: (ctx) => SelValidation()),
      BlocProvider(create: (ctx) => SellerregconfBloc()),
      BlocProvider(create: (ctx) => DishaddBloc()),
      BlocProvider(create: (ctx) => SelhomepBloc()),
      BlocProvider(create: (ctx) => MoreadditemBloc()),
      BlocProvider(create: (ctx) => SelreviewBloc()),
      BlocProvider(create: (ctx) => SeltrackorderBloc()),
      BlocProvider(create: (ctx) => SelrevenueBloc()),
      BlocProvider(create: (ctx) => SelprogressBloc()),
      BlocProvider(create: (ctx) => SelverifypBloc()),
      BlocProvider(create: (ctx) => SelgenpromocodeBloc()),
    ];
  }
}
