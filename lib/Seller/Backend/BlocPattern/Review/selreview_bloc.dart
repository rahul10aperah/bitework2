import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'selreview_event.dart';
part 'selreview_state.dart';

class SelreviewBloc extends Bloc<SelreviewEvent, SelreviewState> {
  //  orc Adding Repo for data logic

  // SelreviewedRespo addRespo = SelreviewedRespo();

  SelreviewBloc() : super(SelreviewInitial());

  @override
  Stream<SelreviewState> mapEventToState(
    SelreviewEvent event,
  ) async* {}
}
