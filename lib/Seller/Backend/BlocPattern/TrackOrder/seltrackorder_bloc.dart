import 'package:bloc/bloc.dart';
import 'package:comp1/Seller/Backend/SellerRespsitory/SelTrackOrderResp.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'seltrackorder_event.dart';
part 'seltrackorder_state.dart';

class SeltrackorderBloc extends Bloc<SeltrackorderEvent, SeltrackorderState> {
  //  orc Adding Repo for data logic

  SelTrackOrderResp addRespo = SelTrackOrderResp();

  SeltrackorderBloc() : super(SeltrackorderInitial());

  @override
  Stream<SeltrackorderState> mapEventToState(
    SeltrackorderEvent event,
  ) async* {
    if (event is FetchTrackOrderEvent) {
      yield SelTrackLoadingState();
      dynamic dishData = await addRespo.getTrackOrderData(
          context: event.context,
          sellerId: event.sellerId,
          orderStatus: event.orderStatus);

      if (dishData['success'] == 1) {
        yield SelTrackLoadedState(
          selDishData: dishData['Bags'],
        );
      } else {
        yield SelTrackErrorState(error: dishData);
      }
    }

    // ! TrackOrder Status Code
    if (event is RejectedStatsEvent) {
      yield SelTrackLoadingState();
      dynamic dishData = await addRespo.trackOrderStatusResp(
          context: event.context,
          orderId: event.orderId,
          orderStatus: event.orderStatus);
      print('============ $dishData');
      try {
        if (dishData == true) {
          yield SelTrackSuccessState();
        }
      } catch (e) {
        yield SelTrackErrorState(error: e.toString());
      }
    }
  }
}
