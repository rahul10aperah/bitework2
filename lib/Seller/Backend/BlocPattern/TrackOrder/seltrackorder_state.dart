part of 'seltrackorder_bloc.dart';

@immutable
abstract class SeltrackorderState {}

class SeltrackorderInitial extends SeltrackorderState {}

/* -------------------------------------------------------------------------- */
/*                     // !  ADD  STATE                                   */
/* -------------------------------------------------------------------------- */

class SelTrackLoadingState extends SeltrackorderState {}

// !  Address LOADED GET REQUEST METHOD
class SelTrackLoadedState extends SeltrackorderState {
  final dynamic selDishData;

  SelTrackLoadedState({
    this.selDishData,
  });

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class SelTrackSuccessState extends SeltrackorderState {
  SelTrackSuccessState();
}

class SelTrackErrorState extends SeltrackorderState {
  dynamic error;
  SelTrackErrorState({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddressFailure(error: $error)';
}
