part of 'seltrackorder_bloc.dart';

abstract class SeltrackorderEvent extends Equatable {
  SeltrackorderEvent();

  @override
  List<Object> get props => [];
}

class FetchTrackOrderEvent extends SeltrackorderEvent {
  final int? orderStatus;
  final int? sellerId;
  dynamic context;

  FetchTrackOrderEvent({this.orderStatus, this.context, this.sellerId});
  @override
  List<Object> get props => [];
}

// ! ACCEPTED EVENT FOR
class RejectedStatsEvent extends SeltrackorderEvent {
  final int? orderStatus;
  final int? orderId;
  dynamic context;
  RejectedStatsEvent({this.orderStatus, this.context, this.orderId});
}
