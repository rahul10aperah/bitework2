import 'package:bloc/bloc.dart';

import 'package:comp1/Seller/Backend/SellerRespsitory/SelDishMoreResp.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

part 'moreadditem_event.dart';
part 'moreadditem_state.dart';

class MoreadditemBloc extends Bloc<MoreadditemEvent, MoreadditemState> {
  //  orc Adding Repo for data logic

  SelDishItemMoreRespo addRespo = SelDishItemMoreRespo();

  MoreadditemBloc() : super(MoreadditemInitial());

  @override
  Stream<MoreadditemState> mapEventToState(
    MoreadditemEvent event,
  ) async* {
    /* -------------------------------------------------------------------------- */
    /*                  // ! Address ADDED , ADDING  , DELETING BLOC                 */
    /* -------------------------------------------------------------------------- */
    if (event is SelMoreItemBtnEvent) {
      dynamic regDat = await addRespo.selDishItemMoreRespo(
          addItemModel: event.moreItemModel);
      if (regDat['success'] == 1) {
        yield SelMoreItemSuccessState();
      }
      yield SelMoreItemErrorState(error: regDat['message'].toString());
    }
  }
}
