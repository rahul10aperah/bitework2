part of 'moreadditem_bloc.dart';

@immutable
abstract class MoreadditemEvent {}

// !  Address FETCH Event
class FetchSelMoreItemEvent extends MoreadditemEvent {
  @override
  List<Object> get props => [];
}

/* -------------------------------------------------------------------------- */
/*                          // ! Address SAVE BUTTON                          */
/* -------------------------------------------------------------------------- */

// ignore: must_be_immutable
class SelMoreItemBtnEvent extends MoreadditemEvent {
  FormData? moreItemModel;
  SelMoreItemBtnEvent({this.moreItemModel});

  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}
