part of 'moreadditem_bloc.dart';

@immutable
abstract class MoreadditemState {}

class MoreadditemInitial extends MoreadditemState {}

/* -------------------------------------------------------------------------- */
/*                     // !  ADD  STATE                                   */
/* -------------------------------------------------------------------------- */

class SelMoreItemLoadingState extends MoreadditemState {}

// !  Address LOADED GET REQUEST METHOD
class SelMoreItemState extends MoreadditemState {
  SelMoreItemState();

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class SelMoreItemSuccessState extends MoreadditemState {}

class SelMoreItemErrorState extends MoreadditemState {
  final String error;
  SelMoreItemErrorState({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddressFailure(error: $error)';
}
