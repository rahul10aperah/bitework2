part of 'selgenpromocode_bloc.dart';

abstract class SelgenpromocodeState extends Equatable {
  const SelgenpromocodeState();

  @override
  List<Object> get props => [];
}

class SelgenpromocodeInitial extends SelgenpromocodeState {}

// ! state
class SelGenerateLoadingeState extends SelgenpromocodeState {}

class SelGeneratePromoCodeState extends SelgenpromocodeState {
  dynamic data;
  SelGeneratePromoCodeState({this.data});
}

class SelDishPromoState extends SelgenpromocodeState {
  dynamic data;
  SelDishPromoState({this.data});
}

class SelPromoDetailsState extends SelgenpromocodeState {
  dynamic data;
  SelPromoDetailsState({this.data});
}

class SelApplyPromoCodeState extends SelgenpromocodeState {
  dynamic data;
  SelApplyPromoCodeState({this.data});
}

class SelGeenreateErrorState extends SelgenpromocodeState {
  dynamic error;
  SelGeenreateErrorState({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddressFailure(error: $error)';
}
