import 'package:bloc/bloc.dart';
import 'package:comp1/Seller/Backend/SellerRespsitory/SelPromoCodeResp.dart';
import 'package:equatable/equatable.dart';

part 'selgenpromocode_event.dart';
part 'selgenpromocode_state.dart';

class SelgenpromocodeBloc
    extends Bloc<SelgenpromocodeEvent, SelgenpromocodeState> {
  //  orc Adding Repo for data logic
  int? sellerId;
  SelPromoCodeResp addRespo = SelPromoCodeResp();

  SelgenpromocodeBloc({this.sellerId}) : super(SelgenpromocodeInitial());

  @override
  Stream<SelgenpromocodeState> mapEventToState(
    SelgenpromocodeEvent event,
  ) async* {
    // ! PromoDetails
    if (event is SelPromoDetailsEvent) {
      // yield SelGenerateLoadingeState();

      dynamic genData =
          await addRespo.promoDetailsResp(sellerId: event.sellerId);
      // print('Bloc data ------------ promo Deaiols${genData['success']}');
      if (genData['success'] == 1) {
        yield SelPromoDetailsState(data: genData);
      } else {
        yield SelGeenreateErrorState(error: genData);
        // yield SelGeenreateErrorState(error: 'No Data');
      }
    }
    // ! Genereate Promo Code
    if (event is SelGeneratePromoCodeEvent) {
      yield SelGenerateLoadingeState();
      dynamic genData =
          await addRespo.generatePromoResp(sellerId: event.sellerId);
      print('bloc genData ===== $genData');
      if (genData['success'] == 1) {
        yield SelGeneratePromoCodeState(data: genData);
      } else {
        yield SelGeenreateErrorState(error: genData);
      }
    }

    // ! ApplyPromoCode
    if (event is SelApplyPromoCodeEvent) {
      yield SelGenerateLoadingeState();

      dynamic genData = await addRespo.applyPromoResp(
          sellerId: event.sellerId,
          dishId: event.dishId,
          allItems: event.allItems);

      if (genData['success'] == 1) {
        yield SelApplyPromoCodeState(data: genData);
      } else {
        yield SelGeenreateErrorState(error: genData);
      }
    }

    // ! Dish Promo Code','
    // if (event is SelDishPromoCodeEvent) {
    //   yield SelGenerateLoadingeState();

    //   dynamic genData = await addRespo.dishPromoResp(sellerId: event.sellerId);

    //   if (genData['success'] == 1) {
    //     yield SelDishPromoState(data: genData);
    //   } else {
    //     yield SelGeenreateErrorState(error: genData);
    //   }
    // }
  }
}
