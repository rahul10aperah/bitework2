part of 'selgenpromocode_bloc.dart';

abstract class SelgenpromocodeEvent extends Equatable {
  const SelgenpromocodeEvent();

  @override
  List<Object> get props => [];
}

class SelGeneratePromoCodeEvent extends SelgenpromocodeEvent {
  int? sellerId;
  SelGeneratePromoCodeEvent({this.sellerId});
}

class SelDishPromoCodeEvent extends SelgenpromocodeEvent {
  int? sellerId;
  SelDishPromoCodeEvent({this.sellerId});
}

class SelPromoDetailsEvent extends SelgenpromocodeEvent {
  final int? sellerId;
  SelPromoDetailsEvent({this.sellerId});
}

class SelApplyPromoCodeEvent extends SelgenpromocodeEvent {
  final int? sellerId;
  final List? dishId;
  final bool? allItems;
  SelApplyPromoCodeEvent({this.sellerId, this.dishId, this.allItems});
}
