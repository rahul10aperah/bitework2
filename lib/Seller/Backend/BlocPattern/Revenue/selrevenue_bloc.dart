import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'selrevenue_event.dart';
part 'selrevenue_state.dart';

class SelrevenueBloc extends Bloc<SelrevenueEvent, SelrevenueState> {
  //  orc Adding Repo for data logic

  // SelrevenueedRespo addRespo = SelrevenueedRespo();

  SelrevenueBloc() : super(SelrevenueInitial());

  @override
  Stream<SelrevenueState> mapEventToState(
    SelrevenueEvent event,
  ) async* {}
}
