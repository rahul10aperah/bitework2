import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'selprogress_event.dart';
part 'selprogress_state.dart';

class SelprogressBloc extends Bloc<SelprogressEvent, SelprogressState> {
  //  orc Adding Repo for data logic

  // SelprogressedRespo addRespo = SelprogressedRespo();

  SelprogressBloc() : super(SelprogressInitial());

  @override
  Stream<SelprogressState> mapEventToState(
    SelprogressEvent event,
  ) async* {}
}
