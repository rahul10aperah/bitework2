part of 'selhomep_bloc.dart';

abstract class SelhomepEvent extends Equatable {
  SelhomepEvent();

  @override
  List<Object> get props => [];
}

// ! FIRST EVENT FOR FETCHING / INITIALIZE THE EVENT
class FetchSelHomeEvent extends SelhomepEvent {
  final int? sellerId;
  final List? dishStatus;
  final int? pageNum;

  FetchSelHomeEvent({
    this.sellerId,
    this.pageNum,
    this.dishStatus,
  });
  @override
  List<Object> get props => [];
}

class SelHomeDelItemEvent extends SelhomepEvent {
  final int? dishId;
  SelHomeDelItemEvent({this.dishId});
  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}

class SelHomeUpdateItemEvent extends SelhomepEvent {
  final String? dishId;
  final String? onoff;
  final int? sellerId;
  final int? pageNum;
  final List? dishStatus;
  dynamic context;
  SelHomeUpdateItemEvent(
      {this.pageNum,
      this.dishStatus,
      this.dishId,
      this.context,
      this.sellerId,
      this.onoff});
  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}

// ! data with pagination
class SelDataPageItemEvent extends SelhomepEvent {
  int? pageNum;
  final int? sellerId;
  List? dishStatus;
  dynamic context;
  SelDataPageItemEvent(
      {this.pageNum, this.context, this.sellerId, this.dishStatus});
  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}
