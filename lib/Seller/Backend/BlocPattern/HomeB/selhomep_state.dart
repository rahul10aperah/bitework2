part of 'selhomep_bloc.dart';

// @immutable
abstract class SelhomepState extends Equatable {
  const SelhomepState();

  @override
  List<Object> get props => [];
}

class SelhomepInitial extends SelhomepState {}

/* -------------------------------------------------------------------------- */
/*                     // !  ADD  STATE                                   */
/* -------------------------------------------------------------------------- */

class SelHomeLoadingState extends SelhomepState {}

// !  Address LOADED GET REQUEST METHOD
class SelHomeLoadedState extends SelhomepState {
  final dynamic selDishData;
  final dynamic onoffData;
  final dynamic delData;

  SelHomeLoadedState({this.selDishData, this.onoffData, this.delData});

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class SelHomeSuccessState extends SelhomepState {
  SelHomeSuccessState();
}

class SelHomeUpdateItemState extends SelhomepState {
  final dynamic onoffData;
  SelHomeUpdateItemState({this.onoffData});
}

class SelHomeDelItemState extends SelhomepState {
  final String? delData;
  SelHomeDelItemState({this.delData});

  // @override
  // List<Object> get props => [delData];
}

class SelHomeNotSuccessState extends SelhomepState {
  final String? message;
  SelHomeNotSuccessState({this.message});
}

class SelHomeErrorState extends SelhomepState {
  final dynamic error;
  SelHomeErrorState({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddressFailure(error: $error)';
}
