import 'package:bloc/bloc.dart';

import 'package:comp1/Seller/Backend/SellerRespsitory/SelHomeResp.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'selhomep_event.dart';
part 'selhomep_state.dart';

class SelhomepBloc extends Bloc<SelhomepEvent, SelhomepState> {
  //  orc Adding Repo for data logic
  int? sellerId;

  SellerHomeRespo addRespo = SellerHomeRespo();

  SelhomepBloc({this.sellerId}) : super(SelhomepInitial());

  @override
  Stream<SelhomepState> mapEventToState(
    SelhomepEvent event,
  ) async* {
    // ! added data
    if (event is FetchSelHomeEvent) {
      yield SelHomeLoadingState();
      dynamic dishData = await addRespo.getDishDataPage(
          sellerId: event.sellerId,
          pageNum: event.pageNum,
          dishStatus: event.dishStatus);

      if (dishData != false) {
        yield SelHomeLoadedState(
          selDishData: dishData,
        );
      } else {
        yield SelHomeNotSuccessState(message: dishData.toString());
      }
    }

    if (event is SelDataPageItemEvent) {
      yield SelHomeLoadingState();
      dynamic dishData = await addRespo.getDishDataPage(
          sellerId: event.sellerId,
          pageNum: event.pageNum,
          dishStatus: event.dishStatus);

      if (dishData != false) {
        yield SelHomeLoadedState(
          selDishData: dishData,
        );
      } else {
        yield SelHomeNotSuccessState(message: dishData.toString());
      }
    }

    // ! on or off
    if (event is SelHomeUpdateItemEvent) {
      dynamic dishData = await addRespo.getDishDataPage(
          sellerId: event.sellerId,
          pageNum: event.pageNum,
          dishStatus: event.dishStatus);
      dynamic onoffdata = await addRespo.dishOnOffMet(
        dishId: event.dishId,
        onOff: event.onoff,
        context: event.context,
      );

      if (onoffdata == true) {
        yield SelHomeLoadedState(
          onoffData: onoffdata,
          selDishData: dishData,
        );
      } else {
        yield SelHomeErrorState(error: onoffdata);
      }
    }

    // ! delete Item
    if (event is SelHomeDelItemEvent) {
      dynamic delData = await addRespo.dishDeleteMet(
        dishId: event.dishId,
      );
      if (delData != false) {
        yield SelHomeDelItemState(delData: delData['data']);
      } else {
        yield SelHomeErrorState(error: delData['message'].toString());
      }
    }
  }
}
