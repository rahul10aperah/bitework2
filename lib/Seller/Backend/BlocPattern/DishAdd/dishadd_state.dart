part of 'dishadd_bloc.dart';

@immutable
abstract class DishaddState {}

class DishaddInitial extends DishaddState {
  dynamic data;
  DishaddInitial({this.data});
  @override
  List<Object> get props => [];
}

/* -------------------------------------------------------------------------- */
/*                     // !  Address STATE                                   */
/* -------------------------------------------------------------------------- */

class DishAddedLoadingState extends DishaddState {}

// ! get field data
class DishAddFieldState extends DishaddState {
  dynamic data;
  DishAddFieldState({this.data});
}

// !  Address LOADED GET REQUEST METHOD
class DishAddedState extends DishaddState {
  // List<Address> addressData;

  DishAddedState();

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class DishAddedSuccessState extends DishaddState {
  final String? message;
  DishAddedSuccessState({this.message});
}

class EditKitechenDataState extends DishaddState {
  EditKitechenDataState();

  @override
  List<Object> get props => [];
}

class DishAddedErrorState extends DishaddState {
  final String? error;
  DishAddedErrorState({this.error});
}
