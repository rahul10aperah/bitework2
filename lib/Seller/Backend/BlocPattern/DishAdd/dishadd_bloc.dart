import 'package:comp1/Seller/Backend/SellerRespsitory/SelDishAddedResp.dart';
import 'package:bloc/bloc.dart';
import 'package:comp1/Seller/Backend/SellerRespsitory/SelDishMoreResp.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

part 'dishadd_event.dart';
part 'dishadd_state.dart';

class DishaddBloc extends Bloc<DishaddEvent, DishaddState> {
  //  orc Adding Repo for data logic

  SelDishAddedRespo addRespo = SelDishAddedRespo();
  SelDishItemMoreRespo addResp = SelDishItemMoreRespo();
  // dynamic fieldData;

  DishaddBloc() : super(DishaddInitial());

  @override
  Stream<DishaddState> mapEventToState(
    DishaddEvent event,
  ) async* {
    // ! save btn Logic
    if (event is DishAddedBtnEvent) {
      dynamic regDat =
          await addRespo.selDishAddResp(addItemModel: event.addItemModel);
      if (regDat['success'] == 1) {
        yield DishAddedSuccessState(message: regDat['message'].toString());
      } else {
        yield DishAddedErrorState(error: regDat['message'].toString());
      }
    }

    if (event is EditKitechenDataEvent) {
      dynamic regDat =
          await addResp.selDishItemMoreRespo(addItemModel: event.editItemModel);
      if (regDat['success'] == 1) {
        yield DishAddedSuccessState();
      }
      yield DishAddedErrorState(error: regDat['message'].toString());
    }
  }
}
