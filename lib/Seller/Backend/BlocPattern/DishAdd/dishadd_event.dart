part of 'dishadd_bloc.dart';

@immutable
abstract class DishaddEvent {}

/* -------------------------------------------------------------------------- */
/*                          // ! Address SAVE BUTTON                          */
/* -------------------------------------------------------------------------- */

// ! FIRST EVENT FOR FETCHING / INITIALIZE THE EVENT
class FetchItemFieldEvent extends DishaddEvent {
  @override
  List<Object> get props => [];
}

class DishAddedBtnEvent extends DishaddEvent {
  // SelAddItemModel? addItemModel;
  dynamic addItemModel;
  DishAddedBtnEvent({this.addItemModel});

  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}

// ! ADD UPDATE
class EditKitechenDataEvent extends DishaddEvent {
  FormData? editItemModel;
  EditKitechenDataEvent({this.editItemModel});

  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}
