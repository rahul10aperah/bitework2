part of 'sellerregconf_bloc.dart';

abstract class SellerregconfEvent extends Equatable {
  const SellerregconfEvent();

  @override
  List<Object> get props => [];
}

// !  Address FETCH Event

/* -------------------------------------------------------------------------- */
/*                          // ! Address SAVE BUTTON                          */
/* -------------------------------------------------------------------------- */
class SelreviewInitial extends SellerregconfEvent {}

// ignore: must_be_immutable
class SelRegSaveBtnEvent extends SellerregconfEvent {
  // SellerRegModel regModel;
  FormData? regModel;
  SelRegSaveBtnEvent({this.regModel});

  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}

// ! OTP SEND Event
class SelRegSendOtpEvent extends SellerregconfEvent {
  final String? phone;

  SelRegSendOtpEvent({this.phone});

  @override
  String toString() => 'FnameSaveBtn { phone: $phone }';
}

// ! Seller Verify Otp
class SelRegOtpVerifyEvent extends SellerregconfEvent {
  final String? phone;
  final String? otpN;

  SelRegOtpVerifyEvent({this.phone, this.otpN});

  @override
  String toString() => 'FnameSaveBtn { phone: $phone }';
}

// !  RESEND OTP  Event
class SelRegReOtpEvent extends SellerregconfEvent {
  final String? phone;

  SelRegReOtpEvent({this.phone});

  @override
  String toString() => 'FnameSaveBtn { phone: $phone }';
}
