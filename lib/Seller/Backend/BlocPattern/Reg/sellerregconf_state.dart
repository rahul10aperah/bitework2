part of 'sellerregconf_bloc.dart';

abstract class SellerregconfState extends Equatable {
  const SellerregconfState();

  @override
  List<Object> get props => [];
}

class SellerregconfInitial extends SellerregconfState {}

/* -------------------------------------------------------------------------- */
/*                     // !  ADD  STATE                                   */
/* -------------------------------------------------------------------------- */

class SellerRegLoadingState extends SellerregconfState {}

// !  Address LOADED GET REQUEST METHOD
class SellerRegState extends SellerregconfState {
  // List<Address> addressData;

  SellerRegState();

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class SellerRegSuccessState extends SellerregconfState {
  final String? message;
  SellerRegSuccessState({this.message});
}

class SellerRegErrorState extends SellerregconfState {
  final String? error;
  SellerRegErrorState({this.error});
}

// ! otp send State
class SelOtpSendState extends SellerregconfState {
  final String? message;
  SelOtpSendState({this.message});
}

class SelOtpResendState extends SellerregconfState {
  final String? message;
  SelOtpResendState({this.message});
}

class SelOtpVerfiyState extends SellerregconfState {
  final String? message;
  SelOtpVerfiyState({this.message});
}
