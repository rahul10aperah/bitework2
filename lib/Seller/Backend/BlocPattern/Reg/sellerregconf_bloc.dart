import 'package:comp1/Seller/Backend/SellerRespsitory/SelRegResp.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'dart:async';
part 'sellerregconf_event.dart';
part 'sellerregconf_state.dart';

class SellerregconfBloc extends Bloc<SellerregconfEvent, SellerregconfState> {
  //  orc Adding Repo for data logic

  SelRegDataRespo addRespo = SelRegDataRespo();

  SellerregconfBloc() : super(SellerregconfInitial());

  @override
  Stream<SellerregconfState> mapEventToState(
    SellerregconfEvent event,
  ) async* {
    /* -------------------------------------------------------------------------- */
    /*                  // ! Address ADDED , ADDING  , DELETING BLOC                 */
    /* -------------------------------------------------------------------------- */
    if (event is SelRegSaveBtnEvent) {
      yield SellerregconfInitial();
      try {
        dynamic regData =
            await addRespo.selRegisterResp(regModel: event.regModel);
        print(' bloc $regData');
        if (regData == true) {
          yield SellerRegSuccessState();
        }
      } catch (e) {
        yield SellerRegErrorState(error: e.toString());
      }
    }

    // ! otp send Event
    if (event is SelRegSendOtpEvent) {
      dynamic sedMOtp = await addRespo.selSendOtp(phone: event.phone);
      if (sedMOtp['success'] == 1) {
        yield SelOtpSendState(message: sedMOtp['info']);
      }
      // yield SellerRegErrorState(error:sedMOtp['data']);
    }
    // ! Verify otp Event
    if (event is SelRegOtpVerifyEvent) {
      dynamic verifyOtp =
          await addRespo.selVerfityOtp(phone: event.phone, otpN: event.otpN);

      if (verifyOtp['success'] == 1) {
        yield SelOtpVerfiyState(message: verifyOtp['message']);
      }
      yield SellerRegErrorState(error: verifyOtp['message']);
    }

    // ! RESEND  otp Event
    if (event is SelRegReOtpEvent) {
      dynamic resedMOtp = await addRespo.selResendOtp(phone: event.phone);

      if (resedMOtp['success'] == 1) {
        yield SelOtpResendState(message: resedMOtp['message']);
      }
    }
  }
}
