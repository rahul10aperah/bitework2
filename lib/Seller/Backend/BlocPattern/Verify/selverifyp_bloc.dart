import 'package:bloc/bloc.dart';
import 'package:comp1/Seller/Backend/SellerRespsitory/SelVerifyResp.dart';
import 'package:equatable/equatable.dart';

part 'selverifyp_event.dart';
part 'selverifyp_state.dart';

// class SelverifypBloc extends Bloc<SelverifypEvent, SelverifypState> {
//   SelverifypBloc() : super(SelverifypInitial()) {
//     on<SelverifypEvent>((event, emit) {
//       // TODO: implement event handler
//     });
//   }
// }

class SelverifypBloc extends Bloc<SelverifypEvent, SelverifypState> {
  SelVerifyEntryRespo addRespo = SelVerifyEntryRespo();

  SelverifypBloc() : super(SelverifypInitial());

  @override
  Stream<SelverifypState> mapEventToState(
    SelverifypEvent event,
  ) async* {
    if (event is SelChangeScreenEvent) {
      yield SelverifypInitial();
      try {
        bool delData = await addRespo.selChangeScreen(screen: event.screen);
        if (delData == true) {
          yield SelChangeScreenState();
        }
      } catch (e) {
        yield SelVerifyErrorState(error: e.toString());
      }
    }

    // ! add image in screen
    if (event is SelAddKitchenImageEvent) {
      yield SelverifypInitial();
      try {
        bool delData = await addRespo.selAddKitechenImage(
            kitchenImage: event.kitchenImage, kitchenName: event.kitchenName);
        if (delData == true) {
          yield SelAddKitchenImageState();
        }
      } catch (e) {
        yield SelVerifyErrorState(error: e.toString());
      }
    }

    // ! Addd Check Availablity
    if (event is SelCheckAvailabilityEvent) {
      yield SelverifypInitial();
      dynamic delData = await addRespo.selAddAvailability(
        sellerId: event.sellerId,
        servingDateFrom: event.servingDateFrom,
        servingDateTo: event.servingDateTo,
        servingTimeTo: event.servingTimeTo,
        servingTimeFrom: event.servingTimeFrom,
      );
      try {
        print('this is delData $delData');
        if (delData == true) {
          yield SelCheckAvabilityState();
        }
      } catch (e) {
        yield SelVerifyErrorState(error: e.toString());
      }
    }
  }
}
