part of 'selverifyp_bloc.dart';

abstract class SelverifypState extends Equatable {
  const SelverifypState();

  @override
  List<Object> get props => [];
}

class SelverifypInitial extends SelverifypState {}

class SelAddKitchenImageState extends SelverifypState {}

class SelChangeScreenState extends SelverifypState {}

class SelCheckAvabilityState extends SelverifypState {}

class SelVerifyErrorState extends SelverifypState {
  final String error;
  SelVerifyErrorState({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => '(error: $error)';
}
