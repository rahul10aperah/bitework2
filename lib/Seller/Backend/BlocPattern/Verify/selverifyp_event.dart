part of 'selverifyp_bloc.dart';

abstract class SelverifypEvent extends Equatable {
  const SelverifypEvent();

  @override
  List<Object> get props => [];
}

class SelAddKitchenImageEvent extends SelverifypEvent {
  dynamic kitchenImage;
  final String? kitchenName;
  SelAddKitchenImageEvent({this.kitchenImage, this.kitchenName});
  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}

class SelChangeScreenEvent extends SelverifypEvent {
  final int? screen;
  SelChangeScreenEvent({
    this.screen,
  });
  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}

// ! CHECK CODE EVENT screen
class SelCheckAvailabilityEvent extends SelverifypEvent {
  final int? sellerId;
  final String? servingDateFrom;
  final String? servingDateTo;
  final String? servingTimeFrom;
  final String? servingTimeTo;
  SelCheckAvailabilityEvent(
      {this.sellerId,
      this.servingDateFrom,
      this.servingTimeTo,
      this.servingDateTo,
      this.servingTimeFrom});
}
