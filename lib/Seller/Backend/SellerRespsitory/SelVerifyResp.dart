import 'dart:convert';

import 'package:comp1/util/app_constants.dart';

import 'package:comp1/util/http_services.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';

class SelVerifyEntryRespo {
  Dio dio = Dio();

  Future<dynamic> selVerfiySellerEntry() async {
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    // ! Header fILE
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'application/json; charset=utf-8',
    };
    Response res = await dio.get(
        "http://bitesvilla-test.us-east-2.elasticbeanstalk.com/api/users/sellerstatus/${tokn['id']}",
        options: Options(headers: headers));

    var verifyData = res.data as dynamic;
    if (res.statusCode == 200) {
      // print('data sellerverify entry ======================$verifyData');
      return verifyData;
    } else {
      return 'No data';
    }
  }

  // ! 2. CHANGE SCREEN
  Future selChangeScreen({int? screen}) async {
    FormData data = FormData.fromMap({"screen": screen});
    final response = await postDioRequestWithAllOptional(
      URLConstants.selSwitchScreen,
      data: data,
    );

    if (response != null && response['success'] == 1) {
      // print('Change Screeen ======================$response');
      return true;
    } else {
      if (response['message'] != null) return false;
    }
  }

  // ! 3. Seller kitchen Image
  Future selCheckKitechenImage() async {
    final response = await postDioRequestWithAllOptional(
      URLConstants.selKitchenCheckImage,
    );

    var verifyData = response;

    if (response != null && response['success'] == 1) {
      // print('veridy kitchen Image  ======================$verifyData');
      return verifyData;
    } else {
      if (response['message'] != null) return verifyData;
    }
  }

  Future selAddKitechenImage(
      {dynamic kitchenImage, String? kitchenName}) async {
    // ! TAKING TOKEN DATA

    var mimeTypeData =
        lookupMimeType(kitchenImage.files.first.name)!.split('/');

    FormData data = FormData.fromMap({
      "kitchenImage": await MultipartFile.fromFile(
          kitchenImage.files.first.path,
          filename: kitchenImage.files.first.name,
          contentType: new MediaType(mimeTypeData[0], mimeTypeData[1])),
      "kitchenName": kitchenName
    });

    final response = await postDioRequestWithAllOptional(
        URLConstants.selKitchenAddImage,
        data: data,
        contentType: 'multipart/form-data');

    if (response != null && response['success'] == 1) {
      return true;
    } else {
      return false;
    }
  }

// ! Seller Add Availabllity
  Future selAddAvailability(
      {int? sellerId,
      String? servingDateFrom,
      String? servingDateTo,
      String? servingTimeFrom,
      String? servingTimeTo}) async {
    Map<String, dynamic> data = {
      "sellerId": sellerId,
      "servingDateFrom": servingDateFrom,
      "servingDateTo": servingDateTo,
      "servingTimeFrom": servingTimeFrom,
      "servingTimeTo": servingTimeTo,
    };
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'application/json',
    };
    try {
      var response = await dio.put(URLConstants.selCheckAvailability,
          data: json.encode(data), options: Options(headers: headers));
      print('normal data $data');
      print('json data ${json.encode(data)}');
      print('statu Code ${response.statusCode}');
      print('data${response.data}');
      return true;
    } on DioError catch (e) {
      print('statu Code ${e.response!.statusCode}');
      print('data ${e.response!.data}');
      return false;
    }
    // final response =
    //     await putDioWithHeadersRequest(URLConstants.selCheckAvailability, data);
    // print(response);
    // if (response != null && response['success'] == 1) {
    //   return true;
    // } else {
    //   return response;
    // }
  }

  // ! seller check code
  Future selCheckAvailability({
    int? sellerId,
  }) async {
    // ! TAKING TOKEN DATA
    Map<String, dynamic> data = {
      "sellerId": sellerId,
    };

    final response = await postDioRequestWithAllOptional(
        URLConstants.selCheckAvailability,
        data: data);
    // print(response);
    if (response != null && response['success'] == 1) {
      return response;
      // return true;
    } else {
      return response;
    }
  }
}
