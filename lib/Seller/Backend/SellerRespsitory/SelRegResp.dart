import 'dart:convert';

import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/http_services.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:dio/dio.dart';

// ValueNotifier<User> currentUser = ValueNotifier(User());

class SelRegDataRespo {
  // ! SEND OTP
  Future<dynamic> selSendOtp({String? phone}) async {
    // ! TAKING TOKEN DATA

    FormData selOtpM = FormData.fromMap({"phone": "91" + phone.toString()});

    final response = await postDioRequestWithAllOptional(
      URLConstants.selMobileOtp,
      data: selOtpM,
    );
    print('resendsendOtp $response');
    if (response != null && response['success'] == 1) {
      return response;
    } else {
      return response;
    }
  }

// ! RESEND OTP
  Future<dynamic> selVerfityOtp({String? phone, String? otpN}) async {
    FormData selOtpM =
        FormData.fromMap({"phone": "91" + phone.toString(), "otpCode": otpN});

    final response = await postDioRequestWithAllOptional(
      URLConstants.selverifyOtp,
      data: selOtpM,
    );
    // print('verfiy' + response.toString());
    // print()
    if (response != null && response['success'] == 1) {
      //  print('matchOtp $response');
      return response;
    } else {
      return response;
    }
  }

// ! RESEND OTP
  Future<dynamic> selResendOtp({String? phone}) async {
    FormData selOtpM = FormData.fromMap({"phone": "91" + phone.toString()});
    final response = await postDioRequestWithAllOptional(
      URLConstants.selresendOtp,
      data: selOtpM,
    );
    print('resendsendOtp $response');
    if (response != null && response['success'] == 1) {
      return response;
    } else {
      return response;
    }
  }

  Future<dynamic> selRegisterResp({FormData? regModel}) async {
    // Dio dio = Dio();
    // ! headers
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    Map<String, String> headers = {
      "accepts": "*/*",
      "Authorization": 'Bearer ' + tokn['token']
    };
    final response = await postDioRequestWithAllOptional(URLConstants.sellerReg,
        data: regModel, newheaders: headers);

    if (response != null && response['success'] == 1) {
      print(response);
      // return response;
      return true;
    } else {
      return response;
    }
  }
}
