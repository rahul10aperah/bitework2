import 'dart:convert';
import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:dio/dio.dart';

class SelPromoCodeResp {
  Dio dio = Dio();
  // ! Prmo Deatisl
  Future<dynamic> promoDetailsResp({int? sellerId}) async {
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));

    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
    };

    Map<String, dynamic> data = {"sellerId": sellerId};

    try {
      Response response = await dio.post(URLConstants.selpromoDetailsUrl,
          data: json.encode(data), options: Options(headers: headers));
      // print('promoDetailsResp $response');
      if (response.data['success'] == 1) {
        return response.data;
      }
    } on DioError catch (e) {
      // print('catch ------------------- ${e.toString()}');
      // print(' this is error --------------- ${e.response!.headers}');
      // print(' this is error --------------- ${e.response!.data['success']}');

      return e.response!.data;
      // return false;
    }
  }

  // ! GENERATE PROMO CODE
  Future<dynamic> generatePromoResp({int? sellerId}) async {
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));

    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
    };
    // print('sellerId prmocCode $sellerId');
    Map<String, dynamic> data = {"sellerId": sellerId};

    try {
      Response res = await dio.patch(URLConstants.selgeneratePromoUrl,
          data: json.encode(data), options: Options(headers: headers));
      // print('generatePromoResp $res');
      if (res.data['success'] == 1) {
        return res.data;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  // !Dish Promo code
  Future<dynamic> applyPromoResp(
      {int? sellerId, List? dishId, bool? allItems}) async {
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));

    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'application/json',
    };

    Map<String, dynamic> data = {
      "sellerId": sellerId,
      "dishId": dishId,
      "allItems": allItems
    };
    Map<String, dynamic> data2 = {"sellerId": sellerId, "allItems": allItems};

    try {
      dynamic mainData = allItems == true ? data2 : data;
      Response res = await dio.put(URLConstants.selapplyPromoUrl,
          data: json.encode(mainData), options: Options(headers: headers));
      print('this resquestbody ${json.encode(data)}');
      print('gentea promo code ${res.headers}');
      print('gentea promo code ${res.data}');
      print('======= Status Code ${res.statusCode}');
      if (res.data['success'] == 1) {
        return res.data;
      }
    } on DioError catch (e) {
      print('statuscode ${e.response!.statusCode}');
      print('data ${e.response!.data}');
      // print('catch mathod ${e.toString()}');

      // return e.response!.data;
      // return false;
    }
  }
}
