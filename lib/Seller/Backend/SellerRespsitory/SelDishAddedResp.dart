import 'dart:convert';
import 'dart:io';

import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/http_services.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

class SelDishAddedRespo {
// ! Add New Dish
  Future<dynamic> selDishAddResp({FormData? addItemModel}) async {
    // ! headers
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    Map<String, String> headers = {
      "accepts": "*/*",
      "Authorization": 'Bearer ' + tokn['token']
    };
    final response = await postDioRequestWithAllOptional(
        URLConstants.selDishAdd,
        data: addItemModel,
        newheaders: headers);
    print('-------------- ${response}');
    if (response != null && response['success'] == 1) {
      return response;
    } else {
      return response;
    }
  }

  // // ! Kitchen Edit DATAR
  // Future<dynamic> selKitechenEditResp({FormData? editItemModel}) async {
  //   // ! headers
  //   SharedHelper shared = SharedHelper();
  //   var tokn = json.decode(await shared.getString('current_user'));
  //   Map<String, String> headers = {
  //     "accepts": "*/*",
  //     "Authorization": 'Bearer ' + tokn['token']
  //   };
  //   final response = await postDioRequestWithAllOptional(
  //       URLConstants.selDishAdd,
  //       data: editItemModel,
  //       newheaders: headers);
  //   print('-------------- ${response}');
  //   if (response != null && response['success'] == 1) {
  //     return response;
  //   } else {
  //     return response;
  //   }
  // }
}
