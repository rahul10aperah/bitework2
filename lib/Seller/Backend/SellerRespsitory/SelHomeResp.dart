import 'dart:convert';
import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

// ! Dish on Off

class SellerHomeRespo {
  Dio dio = Dio();
  Future<bool> dishOnOffMet({
    dynamic context,
    String? dishId,
    String? onOff,
  }) async {
    // ! TAKING TOKEN DATA
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));

    // ! Header fILE
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'application/json',
    };
    Map<String, dynamic> data = {
      "dishId": dishId,
      "onOff": onOff,
    };
    try {
      Response res = await dio.put(
        URLConstants.selDishOnOff,
        data: json.encode(data),
        options: Options(headers: headers),
      );
      // print('======================status Code ${res.statusCode}');
      // print('====================== ${res}');

      if (res.data['success'] == 1 || res.statusCode == 409) {
        snackBar(context, res.data['message'] ?? '');

        return true;
      }
      return false;
    } on DioError catch (e) {
      return false;
    }
  }

  // ! delete DISH METHOD
  Future<dynamic> dishDeleteMet({int? dishId}) async {
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));

    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'application/json',
    };

    Map<String, dynamic> data = {"dishId": dishId};

    // print(json.encode(data));
    try {
      Response response = await dio.delete(
        URLConstants.selDishDelete,
        data: data,
        options: Options(headers: headers),
      );
      // print('--------------------------------------Home Data ${response}');
      // print(
      //     '--------------------------------------Home Data ${response.runtimeType}');
      if (response.statusCode == 200 || response.data['success'] == 1) {
        return response.data;
        // return true;
      }
    } catch (e) {
      // print('catech ${e.toString()}');
      return false;
    }
  }

  // ! GET KITECHN SCREEN DATA IN
  Future<dynamic> getDishDataPage(
      {int? sellerId, int? pageNum, List? dishStatus}) async {
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'application/json',
    };

    // print('page number $pageNum');
    // print('dishStats $dishStatus');
    Map<String, dynamic> data = {
      "sellerId": [sellerId],
      "dishStatus": dishStatus,
    };

    try {
      Response res = await dio.post(
          URLConstants.baseUrl + 'api/dish/sellerId?page=$pageNum',
          data: json.encode(data),
          options: Options(headers: headers));
      // print('================= ${res.data}');
      // print('================= ${res.statusCode}');
      if (res.data['success'] == 1) {
        return res.data;
      }
    } on DioError catch (e) {
      return false;
    }
  }
}
