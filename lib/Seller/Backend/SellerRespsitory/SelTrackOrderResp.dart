import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/common.dart';
import 'package:comp1/util/http_services.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class SelTrackOrderResp {
  Dio dio = Dio();
  Future getTrackOrderData(
      {dynamic context, int? sellerId, int? orderStatus}) async {
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'application/json',
    };
    Map<String, dynamic> data = {
      "sellerId": sellerId,
      "orderStatus": orderStatus,
    };

    final response =
        await postDioRequestWithHeaders(URLConstants.selTrackOrderUrl, data);

    if (response != null && response['success'] == 1) {
      return response;
    } else {
      if (response['message'] != null)
        // snackBar(context, response['message'] ?? 'No Dat');
        return null;
    }
  }

  // ! CHange order Status Code
  Future trackOrderStatusResp(
      {dynamic context, int? orderId, int? orderStatus}) async {
    Map<String, dynamic> data = {
      "orderStatus": orderStatus,
      "orderId": orderId
    };

    final response = await putDioWithHeadersRequest(
        URLConstants.selTrackOrderStatusUrl, json.encode(data));
    print(response);
    if (response != null && response['success'] == 1) {
      // snackBar(context, response['data'] ?? 'No Data');
      return true;
    } else {
      // snackBar(context, response['message'] ?? 'No Dat');
      return response;
    }
  }
}
