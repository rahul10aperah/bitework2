import 'dart:convert';
import 'dart:io';

import 'package:comp1/util/app_constants.dart';
import 'package:comp1/util/http_services.dart';
import 'package:comp1/util/shared_helper.dart';
import 'package:dio/dio.dart';
// import 'package:http/http.dart' as http;

class SelDishItemMoreRespo {
  Dio dio = Dio();
  // ! DISH ADD METHOD
  Future<dynamic> selDishItemMoreRespo({FormData? addItemModel}) async {
    // ! headers
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    Map<String, String> headers = {
      "accepts": "*/*",
      // "Access-Control_Allow_Origin": "*",
      "Authorization": 'Bearer ' + tokn['token']
    };
    final response = await postDioRequestWithAllOptional(
        URLConstants.selDishAdd,
        data: addItemModel,
        newheaders: headers);
    // Response response = await dio.post(URLConstants.selDishAdd,
    //     data: addItemModel, options: Options(headers: headers));
    // print('response ' + response.toString());

    // print(response['data']);

    if (response != null && response['success'] == 1) {
      return response;
    } else {
      return response;
    }
  }
}
