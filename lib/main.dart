import 'dart:io';
import 'package:comp1/Screens/splash.dart';
import 'package:comp1/util/style.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'Seller/SExtFile/AllproviderList.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: MainBloc.allBlocs(),
      child: MaterialApp(
        theme: ThemeData(
            primaryColor: redColor,
            appBarTheme: const AppBarTheme(color: redColor)),
        debugShowCheckedModeBanner: false,
        home: Splash(),
      ),
    );
  }
}
